/***** Application.cpp *************************************************************************************************

    Application: Rescue3D application class

***********************************************************************************************************************/

#include <cmath>
#include <ctime>
#include <iostream>

#ifdef __EMSCRIPTEN__
#include <emscripten/emscripten.h>
#endif

#include "Application.h"
#include "Models/Models.h"

using namespace Rescue3D;

#ifndef M_PI
#define M_PI            3.14159265358979323846264
#endif

#define WINDOW_WIDTH	800
#define WINDOW_HEIGHT   800

#define PREFERRED_MAXIMUM_ROTATION_SPEED    1.0f

/**********************************************************************************************************************/
/*****  Application  **************************************************************************************************/
/**********************************************************************************************************************/

bool Application::run()
{
    if (!init())
    {
        cleanup();
        return false;
    }

#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop_arg(emscriptenMainLoop, (void *)this, 0, 1);
#else
    while (!glfwWindowShouldClose(main_window)) mainLoop();
#endif

    cleanup();
    return true;
}

//######################################################################################################################

void Application::emscriptenMainLoop(void * app_context)
{
    Application * app = (Application *)app_context;
    app->mainLoop();
}

//######################################################################################################################

void Application::glfwError(int error_code, const char * description)
{
    std::cout << "glfw error (" << error_code << "): " << description << std::endl;
}

//######################################################################################################################

void Application::glfwMouseCursorPositionCallback(GLFWwindow * window, double x, double y)
{
    Application * app = (Application *)glfwGetWindowUserPointer(window);
    app->mouseMove(x, y);
}

//######################################################################################################################

void Application::glfwMouseCursorEnterCallback(GLFWwindow * window, int entered)
{
    Application * app = (Application *)glfwGetWindowUserPointer(window);
    if (entered) app->mouseEnter();
    else         app->mouseLeave();
}

//######################################################################################################################

void Application::glfwMouseButtonCallback(GLFWwindow * window, int button, int action, int mods)
{
    Application * app = (Application *)glfwGetWindowUserPointer(window);
    app->mouseClick(button, action, mods);
}

//######################################################################################################################

bool Application::init()
{
    /* Initialize GLFW library */
    glfwSetErrorCallback(glfwError);
    if (!glfwInit()) return false;

    /* Create a windowed mode window and its OpenGL context */
    main_window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Rescue 3D", NULL, NULL);
    if (!main_window)
    {
        glfwTerminate();
        return false;
    }

    glfwSetWindowUserPointer(main_window, this);
    glfwMakeContextCurrent(main_window);

#ifndef __EMSCRIPTEN__
    glfwSwapInterval(1);
#endif

    glfwSetCursorPosCallback(main_window, glfwMouseCursorPositionCallback);
    glfwSetCursorEnterCallback(main_window, glfwMouseCursorEnterCallback);
    glfwSetMouseButtonCallback(main_window, glfwMouseButtonCallback);

    /* Initialize OpenGL extensions and set defaults */
    if (!setupOpenGL()) return false;

    /* Initialize random number generation */
    std::srand((unsigned int)std::time(NULL));

    if (!initScene()) return false;

    last_time = glfwGetTime();

    return true;
}

//######################################################################################################################

void Application::cleanup()
{
    if (vertex_buffer) glDeleteBuffers(1, &vertex_buffer);
    if (normal_buffer) glDeleteBuffers(1, &normal_buffer);
    if (index_buffer) glDeleteBuffers(1, &index_buffer);
    if (shader_program) glDeleteProgram(shader_program);

    glfwDestroyWindow(main_window);
    glfwTerminate();
}

//######################################################################################################################

void Application::mainLoop()
{
    double elapsed = glfwGetTime() - last_time;
    last_time += elapsed;

    if (elapsed == 0.0) return;

    /* Limit maximum rotation speed */
    GLfloat rotation_speed_square = rotation_speed_x * rotation_speed_x + rotation_speed_y * rotation_speed_y;
    GLfloat rotation_speed = std::sqrt(rotation_speed_square);
    if (rotation_speed > PREFERRED_MAXIMUM_ROTATION_SPEED)
    {
        const GLfloat preferred_speed_square = PREFERRED_MAXIMUM_ROTATION_SPEED * PREFERRED_MAXIMUM_ROTATION_SPEED;
        const GLfloat max_divisor = std::sqrt(rotation_speed_square / preferred_speed_square);
        const GLfloat divisor = std::min(1.0f + (GLfloat)elapsed, max_divisor); // both are greater than zero
        
        rotation_speed_x /= divisor;
        rotation_speed_y /= divisor;

        rotation_speed = std::sqrt(rotation_speed_x * rotation_speed_x + rotation_speed_y * rotation_speed_y);
    }

    /* Calculate rotation */
    rotation_angle += rotation_speed * (GLfloat)elapsed;
    if (rotation_angle > 2 * (GLfloat)M_PI) rotation_angle -= 2 * (GLfloat)M_PI;
    if (rotation_angle < -2 * (GLfloat)M_PI) rotation_angle += 2 * (GLfloat)M_PI;

    rotation_matrix = multiplyMatrix(last_rotation_matrix,
                                     createRotationMatrix(rotation_speed_y, rotation_speed_x, rotation_angle));

    render();
    glfwPollEvents();
}

//######################################################################################################################

bool Application::setupOpenGL()
{
    /* Initialize GLEW */
    GLenum error_code = glewInit();
    if (error_code != GLEW_OK)
    {
        const GLubyte * error_message = glewGetErrorString(error_code);
        return false;
    }
    //const GLubyte * glew_version = glewGetString(GLEW_VERSION);

    /* Set defaults */
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);

    glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    return true;
}

//######################################################################################################################

void Application::setPerspectiveProjection(GLfloat field_of_view_y,
                                           GLfloat aspect_ratio, 
                                           GLfloat near_clipping_plane,
                                           GLfloat far_clipping_plane)
{
    GLfloat t = (GLfloat)std::tan(field_of_view_y * 0.5f * M_PI / 180.0f);
    projection_matrix.m22 = 1.0f / t;
    projection_matrix.m11 = projection_matrix.m22 / aspect_ratio;
    projection_matrix.m33 = -(near_clipping_plane + far_clipping_plane) / (far_clipping_plane - near_clipping_plane);
    projection_matrix.m43 =
        -2.0f * near_clipping_plane * far_clipping_plane / (far_clipping_plane - near_clipping_plane);
    projection_matrix.m12 = projection_matrix.m13 = projection_matrix.m14 = 0.0f;
    projection_matrix.m21 = projection_matrix.m23 = projection_matrix.m24 = 0.0f;
    projection_matrix.m31 = projection_matrix.m32 = 0.0f; projection_matrix.m34 = -1.0f;
    projection_matrix.m41 = projection_matrix.m42 = projection_matrix.m44 = 0.0f;
}

//######################################################################################################################

Application::Matrix4x4 Application::multiplyMatrix(const Matrix4x4& a, const Matrix4x4& b)
{
    Matrix4x4 m;
    
    m.m11 = a.m11 * b.m11 + a.m12 * b.m21 + a.m13 * b.m31 + a.m14 * b.m41;
    m.m12 = a.m11 * b.m12 + a.m12 * b.m22 + a.m13 * b.m32 + a.m14 * b.m42;
    m.m13 = a.m11 * b.m13 + a.m12 * b.m23 + a.m13 * b.m33 + a.m14 * b.m43;
    m.m14 = a.m11 * b.m14 + a.m12 * b.m24 + a.m13 * b.m34 + a.m14 * b.m44;

    m.m21 = a.m21 * b.m11 + a.m22 * b.m21 + a.m23 * b.m31 + a.m24 * b.m41;
    m.m22 = a.m21 * b.m12 + a.m22 * b.m22 + a.m23 * b.m32 + a.m24 * b.m42;
    m.m23 = a.m21 * b.m13 + a.m22 * b.m23 + a.m23 * b.m33 + a.m24 * b.m43;
    m.m24 = a.m21 * b.m14 + a.m22 * b.m24 + a.m23 * b.m34 + a.m24 * b.m44;

    m.m31 = a.m31 * b.m11 + a.m32 * b.m21 + a.m33 * b.m31 + a.m34 * b.m41;
    m.m32 = a.m31 * b.m12 + a.m32 * b.m22 + a.m33 * b.m32 + a.m34 * b.m42;
    m.m33 = a.m31 * b.m13 + a.m32 * b.m23 + a.m33 * b.m33 + a.m34 * b.m43;
    m.m34 = a.m31 * b.m14 + a.m32 * b.m24 + a.m33 * b.m34 + a.m34 * b.m44;

    m.m41 = a.m41 * b.m11 + a.m42 * b.m21 + a.m43 * b.m31 + a.m44 * b.m41;
    m.m42 = a.m41 * b.m12 + a.m42 * b.m22 + a.m43 * b.m32 + a.m44 * b.m42;
    m.m43 = a.m41 * b.m13 + a.m42 * b.m23 + a.m43 * b.m33 + a.m44 * b.m43;
    m.m44 = a.m41 * b.m14 + a.m42 * b.m24 + a.m43 * b.m34 + a.m44 * b.m44;
    
    return m;
}

//######################################################################################################################

Application::Matrix4x4 Application::createIdentityMatrix()
{
    Matrix4x4 m;
    m.m11 = m.m22 = m.m33 = m.m44 = 1.0;
    m.m12 = m.m13 = m.m14 = m.m21 = m.m23 = m.m24 = m.m31 = m.m32 = m.m34 = m.m41 = m.m42 = m.m43 = 0.0;
    return m;
}

//######################################################################################################################

Application::Matrix4x4 Application::createTranslationMatrix(GLfloat x, GLfloat y, GLfloat z)
{
    Matrix4x4 m = createIdentityMatrix();
    m.m41 = x;
    m.m42 = y;
    m.m43 = z;
    return m;
}

//######################################################################################################################

Application::Matrix4x4 Application::createRotationMatrix(GLfloat x, GLfloat y, GLfloat angle)
{
    Matrix4x4 m;

    /* Normalize (X, Y) vector */
    GLfloat length = std::sqrt(x * x + y * y);
    x = x / length;
    y = y / length;

    GLfloat sin_angle = std::sin(angle);
    GLfloat cos_angle = std::cos(angle);

    m.m11 = cos_angle * (1 - x * x) + x * x;
    m.m12 = x * y * (1 - cos_angle);
    m.m13 = -sin_angle * y;
    m.m14 = 0.0f;

    m.m21 = m.m12;
    m.m22 = cos_angle * (1 - y * y) + y * y;
    m.m23 = sin_angle * x;
    m.m24 = 0.0f;

    m.m31 = sin_angle * y;
    m.m32 = -sin_angle * x;
    m.m33 = cos_angle;
    m.m34 = 0.0f;

    m.m41 = m.m42 = m.m43 = 0.0f;
    m.m44 = 1.0f;

    return m;
}

//######################################################################################################################

bool Application::initScene()
{
    /* Setup fixed camera */
    setPerspectiveProjection(45.0f, (GLfloat)WINDOW_WIDTH / WINDOW_HEIGHT, 0.01f, 1000.0f);

    /* Load scene objects to buffers objects */
    glGenBuffers(1, &vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, rescue_logo.vertex_array_size, rescue_logo.positions, GL_STATIC_DRAW);

    glGenBuffers(1, &normal_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, normal_buffer);
    glBufferData(GL_ARRAY_BUFFER, rescue_logo.vertex_array_size, rescue_logo.normals, GL_STATIC_DRAW);

    glGenBuffers(1, &index_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, rescue_logo.index_array_size, rescue_logo.indices, GL_STATIC_DRAW);

    /* Initialize shaders */
    if (!initShaderProgram()) return false;

    return true;
}

//######################################################################################################################

void Application::render()
{
    Matrix4x4 modelview_matrix = multiplyMatrix(rotation_matrix, createTranslationMatrix(0.0f, 0.0f, -4.0f));

    GLfloat light_position[] = { -1.5f, 0.0f, -1.0f };
    GLfloat material_1_color[] = { 0.0f, 0.54f, 0.8f };
    GLfloat material_2_color[] = { 0.8f, 0.8f, 0.8f };

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnableVertexAttribArray(vertex_position_location);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glVertexAttribPointer(vertex_position_location, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid *)0);

    glEnableVertexAttribArray(vertex_normals_location);
    glBindBuffer(GL_ARRAY_BUFFER, normal_buffer);
    glVertexAttribPointer(vertex_normals_location, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid *)0);

    glUseProgram(shader_program);
    glUniformMatrix4fv(projection_matrix_location, 1, GL_FALSE, projection_matrix.m);
    glUniformMatrix4fv(modelview_matrix_location, 1, GL_FALSE, modelview_matrix.m);
    glUniform3fv(light_position_location, 1, light_position);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);

    glUniform3fv(color_location, 1, material_1_color);
    glUniform1f(specular_intensity_location, 0.0f);
    glDrawElements(GL_TRIANGLES,
                   rescue_logo.material_1_index_count,
                   GL_UNSIGNED_SHORT,
                   (const GLvoid *)rescue_logo.material_1_index);

    glUniform3fv(color_location, 1, material_2_color);
    glUniform1f(specular_intensity_location, 1.0f);
    glDrawElements(GL_TRIANGLES,
                   rescue_logo.material_2_index_count,
                   GL_UNSIGNED_SHORT,
                   (const GLvoid *)rescue_logo.material_2_index);

    glfwSwapBuffers(main_window);
}

//######################################################################################################################

void Application::mouseMove(double x, double y)
{
    if (dragging)
    {
        double delta_x = x - drag_start_x;
        double delta_y = y - drag_start_y;

        if (std::abs(delta_x) > 1.0 || std::abs(delta_y) > 1.0)
        {
            rotation_speed_x += (GLfloat)(delta_x / 40.0);
            rotation_speed_y += (GLfloat)(delta_y / 40.0);

            last_rotation_matrix = rotation_matrix;
            rotation_angle = 0.0f;

            drag_start_x = x;
            drag_start_y = y;
        }
    }
}

//######################################################################################################################

void Application::mouseEnter()
{
    /* Nothing to do */
}

//######################################################################################################################

void Application::mouseLeave()
{
    dragging = false;
}

//######################################################################################################################

void Application::mouseClick(int button, int action, int mods)
{
    if (button != GLFW_MOUSE_BUTTON_LEFT) return;
    
    if (action == GLFW_PRESS)
    {
        dragging = true;
        glfwGetCursorPos(main_window, &drag_start_x, &drag_start_y);
    }
    else if (action == GLFW_RELEASE)
    {
        dragging = false;
    }
}

//######################################################################################################################

bool Application::initShaderProgram()
{
    shader_program = glCreateProgram();
    if (!shader_program) return false;
    
    /* Create vertex shader and attach to program */
    std::string vertex_shader_code =
        "uniform mat4 u_modelview_matrix;                                       "
        "uniform mat4 u_projection_matrix;                                      "

        "attribute vec4 a_vertex_position;                                      "
        "attribute vec3 a_vertex_normal;                                        "

        "varying vec3 v_position;                                               "
        "varying vec3 v_normal;                                                 "

        "void main()                                                            "
        "{                                                                      "
        "   v_position = vec3(u_modelview_matrix * a_vertex_position);          "
        "   v_normal = vec3(u_modelview_matrix * vec4(a_vertex_normal, 0.0));   "

        "   gl_Position = u_projection_matrix *                                 "
        "                 u_modelview_matrix *                                  "
        "                 a_vertex_position;                                    "
        "}                                                                      ";

    GLuint vertex_shader = createShader(GL_VERTEX_SHADER, vertex_shader_code);
    if (vertex_shader == 0 ) return false;
    glAttachShader(shader_program, vertex_shader);
    
    /* Create fragment shader and attach to program */
    std::string fragment_shader_code =
        "#ifdef GL_ES\n                                                             "
        "precision mediump float;\n                                                 "
        "#endif\n                                                                   "

        "uniform vec3 u_color;                                                      "
        "uniform vec3 u_light_position;                                             "
        "uniform float u_specular;                                                  "

        "varying vec3 v_position;                                                   "
        "varying vec3 v_normal;                                                     "
        
        "void main()                                                                "
        "{                                                                          "
        "   float distance = length(u_light_position - v_position);                 "
        "   vec3 light_vector = normalize(u_light_position - v_position);           "
        "   vec3 eye_vector = normalize(-v_position);                               "
        "   vec3 half_vector = normalize(light_vector + eye_vector);                "

        "   float diffuse = max(dot(v_normal, light_vector), 0.1);                  "
        "   diffuse = diffuse * (1.0 / (1.0 + (0.001 * distance * distance)));      "

        "   float specular = pow(max(dot(v_normal, half_vector), 0.1), 60.0);       "
        "   specular = specular * (1.0 / (1.0 + (0.001 * distance * distance)));    "

        "   gl_FragColor = vec4(u_color * (diffuse + u_specular * specular), 1.0);  "
        "}                                                                          ";
    
    GLuint fragment_shader = createShader(GL_FRAGMENT_SHADER, fragment_shader_code);
    if (fragment_shader == 0 )
    {
        glDeleteShader(vertex_shader);
        return false;
    }
    glAttachShader(shader_program, fragment_shader);

    /* Link shader program */
    glLinkProgram(shader_program);
    
    /* Check the link status */
    GLint isLinked;
    glGetProgramiv(shader_program, GL_LINK_STATUS, &isLinked);
    if (!isLinked)
    {
        GLint info_length = 0;
        glGetProgramiv(shader_program, GL_INFO_LOG_LENGTH, &info_length);
        if (info_length > 1)
        {
            std::vector< char > info_log(info_length);
            glGetProgramInfoLog(shader_program, info_length, NULL, &info_log[0]);
            std::cout << "Shader program linking error: " << &info_log[0] << std::endl;
        }

        glDeleteShader(vertex_shader);
        glDeleteShader(fragment_shader);
        glDeleteProgram(shader_program);
        return false;
    }

    /* Store attribute and uniform variable locations */
    modelview_matrix_location = glGetUniformLocation(shader_program, "u_modelview_matrix");
    projection_matrix_location = glGetUniformLocation(shader_program, "u_projection_matrix");
    color_location = glGetUniformLocation(shader_program, "u_color");
    light_position_location = glGetUniformLocation(shader_program, "u_light_position");
    specular_intensity_location = glGetUniformLocation(shader_program, "u_specular");

    vertex_position_location = glGetAttribLocation(shader_program, "a_vertex_position");
    vertex_normals_location = glGetAttribLocation(shader_program, "a_vertex_normal");
        
    return true;
}

//######################################################################################################################

GLuint Application::createShader(GLenum shader_type, const std::string& shader_code)
{
    GLuint shader = glCreateShader(shader_type);
    if (shader == 0) return 0;
    
    const char * source = &shader_code[0];
    glShaderSource(shader, 1, &source, NULL);
    glCompileShader(shader);
    
    GLint compiled;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

    if (!compiled)
    {
        GLint info_length = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_length);
        if (info_length > 1)
        {
            std::vector< char > info_log(info_length);
            glGetShaderInfoLog(shader, info_length, NULL, &info_log[0]);
            std::cout << "Shader compilation error: " << &info_log[0] << std::endl;
        }

        glDeleteShader(shader);
        return 0;
    }

    return shader;
}
