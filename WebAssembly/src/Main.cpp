/***** Main.cpp ********************************************************************************************************

    Rescue3D main

***********************************************************************************************************************/

#include "Application.h"

using namespace Rescue3D;

int main()
{
    Application * app = Application::getInstance();
    if (!app->run()) return 0;

    return 1;
}
