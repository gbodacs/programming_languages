/***** Application.h ***************************************************************************************************

    Application: Rescue3D application class

***********************************************************************************************************************/

#pragma once

#include <string>
#include <vector>

#define GLEW_STATIC
#include <gl/glew.h>
#include <GLFW/glfw3.h>

namespace Rescue3D
{
            
/**********************************************************************************************************************/
/**
 * Rescue3D main application class
 */
class Application
{
public:
                            Application() :
                                main_window(NULL),
                                last_time(0.0),
                                vertex_buffer(0),
                                normal_buffer(0),
                                index_buffer(0),
                                shader_program(0),
                                modelview_matrix_location(0),
                                projection_matrix_location(0),
                                color_location(0),
                                light_position_location(0),
                                specular_intensity_location(0),
                                vertex_position_location(0),
                                vertex_normals_location(0),
                                rotation_angle(0.0f),
                                rotation_speed_x(-1.0f),
                                rotation_speed_y(0.0f),
                                dragging(false),
                                drag_start_x(0.0),
                                drag_start_y(0.0)
                            {
                                rotation_matrix = createIdentityMatrix();
                                last_rotation_matrix = createIdentityMatrix();
                            }

    static Application *    getInstance() { static Application inst; return &inst; }
    bool                    run();

private:
    struct Matrix4x4
    {
        union
        {
            struct
            {
                GLfloat m11, m12, m13, m14;
                GLfloat m21, m22, m23, m24;
                GLfloat m31, m32, m33, m34;
                GLfloat m41, m42, m43, m44;
            };

            GLfloat m[16];
        };

        Matrix4x4& operator=(const Matrix4x4& other)
        {
            std::memcpy(m, other.m, 16 * sizeof(GLfloat));
            return *this;
        }
    };

    GLFWwindow *            main_window;

    double                  last_time;

    Matrix4x4               projection_matrix;

    GLuint                  vertex_buffer;
    GLuint                  normal_buffer;
    GLuint                  index_buffer;
    
    GLuint                  shader_program;
    
    GLint                   modelview_matrix_location;
    GLint                   projection_matrix_location;
    GLint                   color_location;
    GLint                   light_position_location;
    GLint                   specular_intensity_location;

    GLint                   vertex_position_location;
    GLint                   vertex_normals_location;

    GLfloat                 rotation_angle;
    GLfloat                 rotation_speed_x;
    GLfloat                 rotation_speed_y;

    Matrix4x4               rotation_matrix;
    Matrix4x4               last_rotation_matrix;

    bool                    dragging;
    double                  drag_start_x;
    double                  drag_start_y;

private:
    static void             emscriptenMainLoop(void * app_context);

    static void             glfwError(int error_code, const char * description);
    static void             glfwMouseCursorPositionCallback(GLFWwindow * window, double x, double y);
    static void             glfwMouseCursorEnterCallback(GLFWwindow * window, int entered);
    static void             glfwMouseButtonCallback(GLFWwindow * window, int button, int action, int mods);

    bool                    init();
    void                    cleanup();
    void                    mainLoop();
    bool                    setupOpenGL();

    void                    setPerspectiveProjection(GLfloat field_of_view_y,
                                                     GLfloat aspect_ratio, 
                                                     GLfloat near_clipping_plane,
                                                     GLfloat far_clipping_plane);
    Matrix4x4               multiplyMatrix(const Matrix4x4& a, const Matrix4x4& b);
    Matrix4x4               createIdentityMatrix();
    Matrix4x4               createTranslationMatrix(GLfloat x, GLfloat y, GLfloat z);
    Matrix4x4               createRotationMatrix(GLfloat x, GLfloat y, GLfloat angle);

    bool                    initScene();
    void                    render();

    void                    mouseMove(double x, double y);
    void                    mouseEnter();
    void                    mouseLeave();
    void                    mouseClick(int button, int action, int mods);

    bool                    initShaderProgram();
    GLuint                  createShader(GLenum shader_type, const std::string& shader_code);
};

/*--------------------------------------------------------------------------------------------------------------------*/

} // namespace Rescue3D
