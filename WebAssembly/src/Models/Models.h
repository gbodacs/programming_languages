/***** Models.h ********************************************************************************************************

    Embedded 3D models

***********************************************************************************************************************/

#include <gl/glew.h>
#include <gl/gl.h>

namespace Rescue3D
{
    struct EmbeddedModel
    {
        const GLsizeiptr    index_array_size;
        const GLushort *    indices;

        const GLsizeiptr    vertex_array_size;
        const GLfloat *     positions;
        const GLfloat *     normals;
        
        const GLsizeiptr    material_1_index;
        const GLsizei       material_1_index_count;

        const GLsizeiptr    material_2_index;
        const GLsizei       material_2_index_count;

    };

    extern const EmbeddedModel rescue_logo;
}
