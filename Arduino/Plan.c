0. Riasztások:
        - 3 fázis
        - Szekrény nyitva
        - Nyomás alacsony
        - Vízszivárgás

 
1. Arduino folyamatos működés:
    - Arduino beolvas minden bemenetet és eltárolja változókban
    - Arduino figyeli a bemenő változókat, hogy:
        - van-e kilógás a szélső (lowlow-highhigh) értékekből és a vezérlés már kapcsolt-e
            - Ha igen, riasztási változót beállít (időzítőt indít)
        - van-e kilógás a szélső (low-high) értékekből
            - Ha igen, beavatkozik (fűtés be-ki)
        - van-e speciális esemény (lásd lent: riasztások):
            - Ha igen, tovább küldi a Fő vezérlőnek
    - Ha a Fő vezérlő adatot kér, akkor:
        - Elküldi neki
    - Ha a Fő vezérlő adatot tol be, akkor:
        - eltárolni az EEPROM-ban
        - ott kiszámolni a két szélső értéket + riasztási értékeket
        - mindent értéket beírni a megfelelő változókba
    - Ha a Fő vezérlő "vészleállást bekapcsol" küld, akkor
        - Mindent lekapcsol és várja, a "vészleállást kikapcsol" jelet
    - Ha a Fő vezérlő "vészleállást kikapcsol" küld, akkor
        - Mindent visszakapcsol a változóknak megfelelően indul a vezérlés
    - Ha áramszünet után beindul az Arduino, akkor:
        - Minden adatot felolvas az EEPROM-ból és változóba rak
        - Kiszámít midnen szélső értéket (low-high, lowlow-highhigh)
        - Szól a Fő vezérlőnek, hogy "élek"
        - A változóknak megfelelően indul a vezérlés
    - Ha nem éri el a Fő vezérlőt, akkor
        - Mindent lekapcsol???

2. Fő vezérlő folyamatos működés:
    - Időközönként minden Arduinóból:
        - Felolvassuk az összes mért értéket 
        - Eltárolás változókban
        - Ha nem ér el egy Arduinót, akkor 
            - Riasztási változót beállít
    - Ha kintről (modbus) adatot kérnek, akkor:
        - Elküldi válaszban a változók tartalmát
    - Ha kintről (modbus) adatot tolnak be, akkor:
        - Változóba eltárolás (biztosan kell?)
        - A változó értéket el kell tolni a megfelelő Arduinóba
    - Ha kintről (modbus) "vészleállást bekapcsol" jön, akkor:
        - Minden Arduinónak szólni, hogy "vészleállást bekapcsol"
        - Riasztást küldeni róla
    - Ha kintről (modbus) "vészleállást kekapcsol" jön, akkor:
        - Minden Arduinónak szólni, hogy "vészleállást kikapcsol"
        - Riasztást küldeni róla
    - Ha áramszünet után beindul a Fő vezérlő, akkor
        - Megpróbál elérni minden Arduinót




3. Fő vezérlő webszerver:
    - Get: Visszaadja a default weboldalt, benne a jelenlegi értékekkel
    - Submit: Beállítja a feltöltött értékeket