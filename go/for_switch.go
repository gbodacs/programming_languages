func main() {
	var foo int = 2
	
	if (foo == 1) {
		println("equal!")
	} else {
		println("non-eq!")
	}
	
	
	switch foo = 1; foo {
	case 1:
		println(1);
	case 2:
		println(2);
	}
	
}