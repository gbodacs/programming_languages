package main


func main() {
	sayHello("hello", "from", "Go", "Plural")

	Length, FinalResult := Add(10,215,6235,65,5,362,3645,14,124,54,235)
	
	println(FinalResult, Length);
}

func sayHello(messages ...string) {
	for _, message := range messages {
		println(message);
	}
}

func Add(values ...int) (Length int, FinalResult int) {
	FinalResult = 0;
	for _, value := range values {
		FinalResult+= value;
	}
	
	Length = len(values)
	
	return
}