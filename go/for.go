package main


func main() {
	for i:=0; i<5; i++ {
		println(i)
	}
	println()
	
	i:=0;
	for{
		i++
		println(i)
		
		if i>5 {
			break;
		}
	}
	println()
	
	s:= []string {"ss", "fff", "sedrth"}
	for  idx, v := range s {
		println(idx, v)
	}
	
}