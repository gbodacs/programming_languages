package main


func main() {
	add := 1+2
	println(add)
	println()
	
	sub := 121-98
	println(sub)
	println()
		
	remainder := 10%3
	println(remainder)
	println()
		
	per := 10/3
	println(per)
	println()
	
	inc := 1
	println(inc)
	inc++
	println(inc)
	inc--
	println(inc)
	inc *= 10
	println(inc)	
	println()
}