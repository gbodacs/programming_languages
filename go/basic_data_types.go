package main

import "fmt"

func main() {
	myArray := [...]int{22, 33, 44}
	myArray[2] = 12
	fmt.Println(len(myArray))
	
	mySlice := myArray[:]
	fmt.Println(mySlice)
	
	mySlice = append(mySlice, 100)
	fmt.Println(mySlice)
	
	mySlice2 := []float32{12., 15., 18.}
	fmt.Println(mySlice2)
	fmt.Println(len(mySlice2))
	
	mySlice3 := make([]float32, 100)
	
	mySlice3[0] = 12.
	mySlice3[1] = 24.
	mySlice3[2] = 36.
	mySlice3[3] = 48.
	
	fmt.Println(mySlice3)
	fmt.Println(len(mySlice3))
	
	
	myMap := make(map[int]string)
	fmt.Println(myMap)
	myMap[42] = "foo"
	myMap[12] = "foo2"
	myMap[17] = "bar"
	myMap[54] = "bar2"
	fmt.Println(myMap)
	
}