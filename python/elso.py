#!/usr/bin/python3
import sys
import random


# Normal string
String = "Hello tesztszűŰveg"
print (String[2:]) # second comment


# List [] type
list = [ 'abcd', 786 , 2.23, 'john', 70.2 ]
print(list)

list[1] = 0
list[0] = 'xyzz'
print(list)

# Tuple () is a const list
tuple2 = ('abcd', 786 , 2.23, 'john', 70.2)
print(tuple2)

#tuple2[0] = 11 //cannot change tuple value
print(tuple2)

# Dictionary {} is a hash-table with key-value pairs
dict = {}
dict['one'] = "This is one"
dict[2] = "This is two"

tinydict = {'name': 'john', 'code':6734, 'dept': 'sales'}


print(dict['one'])       # Prints value for 'one' key
print(dict[2])           # Prints value for 2 key
print(tinydict)          # Prints complete dictionary
print(tinydict.keys())   # Prints all the keys
print(tinydict.values()) # Prints all the values

# IF statement

var = 101
if( var  == 100 ) :
    print("Value of expression is 100")
    print("Still value of expression is 100")
else :
    print("Value of expression is not 100!!!")

print("Good bye!")

# FOR loop
list = [1,2,3,4]
it = iter(list) # this builds an iterator object
print(next(it)) #prints next available element in iterator

for x in it:
    print(x, end=" ")

# While statement
#it = iter(list) # this builds an iterator object
#while True:
#    try:
#        print (next(it))
#    except StopIteration:
#        sys.exit() #you have to import sys module for this

#Random
random.seed(12)
rand1 = random.random()
rand2 = random.uniform(0, 100)

print(rand1)
print(rand2)

