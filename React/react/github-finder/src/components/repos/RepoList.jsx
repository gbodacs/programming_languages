import RepoItem from "./RepoItem";

function RepoList({ repos }) {
  return (
    <div className="rounded-lg shadow-3xl card bg-base-200">
      <div className="card-body">
        <h2 className="text-3xl my-4 font-bold card-title">Latest Repositories</h2>
        { repos.map((repo) => (
          <RepoItem key={repo.id} repo={repo}/>
        )) }
      </div>
    </div>
  );
}

export default RepoList;
