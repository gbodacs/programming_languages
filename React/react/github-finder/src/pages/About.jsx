import React from "react";

export default function About() {
  return (
    <div>
      <h1 className="text-6xl mb-4">Github Finder</h1>
      <p className="text-lg text-white">
        A program to search and use Github users and repositories
      </p>
      <p className="text-lg text-gray-400">
        Version: <span className="text-white">1.0.0</span>
      </p>
      <p className="text-lg text-gray-400">
        Created by: <span className="text-white">Gabor Bodacs</span>
      </p>
    </div>
  );
}
