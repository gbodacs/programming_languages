// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyACtLOXNPHCBhRg8QtLbHQKLvKqnc9yDZc",
  authDomain: "house-marketplace-39d4c.firebaseapp.com",
  projectId: "house-marketplace-39d4c",
  storageBucket: "house-marketplace-39d4c.appspot.com",
  messagingSenderId: "217897737422",
  appId: "1:217897737422:web:bdc6ab1fddfe4b0ed3449f"
};

// Initialize Firebase
initializeApp(firebaseConfig);
export const db = getFirestore();