import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import Home from './app/pages/Home';
import Login from './app/pages/Login';
import Register from './app/pages/Register';
import Header from './app/components/Header';
import {ToastContainer} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import NewTicket from './app/pages/NewTicket';
import PrivateRoute from './app/components/PrivateRoute';
import Tickets from './app/pages/Tickets';
import Ticket from './app/pages/Ticket';

function App() {
  return (
    <>
      <Router>
        <div className='container'>
          <Header />
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/register' element={<Register />} />
            <Route path='/login' element={<Login />} />
            <Route path='/new-ticket' element={<PrivateRoute />}>
              <Route path='/new-ticket' element={<NewTicket />}/>
            </Route>
            <Route path='/tickets' element={<PrivateRoute />}>
              <Route path='/tickets' element={<Tickets />}/>
            </Route>
            <Route path='/ticket/:ticketId' element={<PrivateRoute />}>
              <Route path='/ticket/:ticketId' element={<Ticket />}/>
            </Route>
          </Routes>
        </div>
      </Router>
      <ToastContainer />
    </>
  );
}

export default App;
