const express = require('express')
const router = express.Router()
const {loginUser, registerUser, getMe} = require('../controllers/UserController')
const {protect} = require('../middleware/AuthMiddleWare')

router.post('/', registerUser)

router.post('/login', loginUser)
router.get('/me', protect, getMe)

/*router.post('/logout', (req, res) => {
  res.send("Logout Route")
})*/

module.exports = router