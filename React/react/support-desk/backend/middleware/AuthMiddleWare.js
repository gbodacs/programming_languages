const jwt = require('jsonwebtoken')
const asyncHandler = require('express-async-handler')
const bcrypt = require('bcryptjs')
const User = require('../models/UserModel')

const protect = asyncHandler(async (req, res, next) => {
  let token

  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
    try {
      //Get Token from header
      token = req.headers.authorization.split(' ')[1]

      //Verify e token
      const decoded = jwt.verify(token, process.env.JWT_SECRET)
      
      //Get User from token and put it into the req without the password
      req.user = await User.findById(decoded.id).select('-password')

      next()
    } catch (error) {
      console.log(error);
      res.status(401)
      throw new Error('Not authorized')
    }
  }

  if(!token) {
    res.status(401)
    throw new Error('Not authorized')
  }
})

module.exports = { protect }