const asyncHandler = require("express-async-handler");

const User = require("../models/UserModel");
const Ticket = require("../models/TicketModel");

// @desc  Get user tickets
// @route GET /api/tickets
// @access Private
const getTickets = asyncHandler(async (req, res) => {
  // Get user from DB by the id in the JWT
  const user = await User.findById(req.user.id)

  if(!user) {
    res.status(401)
    throw new Error("User not authorized")
  }

  const tickets = await Ticket.find({user: req.user.id})

  res.status(200).json(tickets);
})

// @desc  Get a user ticket
// @route GET /api/tickets/:id
// @access Private
const getTicket = asyncHandler(async (req, res) => {
  // Get user from DB by the id in the JWT
  const user = await User.findById(req.user.id)

  if(!user) {
    res.status(401)
    throw new Error("User not authorized")
  }

  const ticket = await Ticket.findById(req.params.id)

  if (!ticket) {
    res.status(404)
    throw new Error("Ticket not found")
  }

  if (ticket.user.toString() !== req.user.id) {
    res.status(401)
    throw new Error("Not authorized_")
  }

  res.status(200).json(ticket);
})

// @desc  Get user tickets
// @route POST /api/tickets
// @access Private
const createTicket = asyncHandler(async (req, res) => {
  const {product, description} = req.body

  //Validation
  if (!product || !description) {
    res.status(400);
    throw new Error("Please add product and description");
  }

  // Get user fro db by th id in the JWT
  const user = await User.findById(req.user.id)

  if(!user) {
    res.status(401)
    throw new Error("User not authorized")
  }

  const ticket = await Ticket.create({
    product,
    description,
    user: req.user.id,
    status: 'new'
  })

  res.status(200).json(ticket);
})

// @desc  Delete a ticket
// @route DELETE /api/tickets/:id
// @access Private
const deleteTicket = asyncHandler(async (req, res) => {
  // Get user from DB by the id in the JWT
  const user = await User.findById(req.user.id)

  if(!user) {
    res.status(401)
    throw new Error("User not authorized")
  }

  const ticket = await Ticket.findById(req.params.id)

  if (!ticket) {
    res.status(404)
    throw new Error("Ticket not found")
  }

  if (ticket.user.toString() !== req.user.id) {
    res.status(401)
    throw new Error("Not authorized_")
  }

  await ticket.remove()

  res.status(200).json({success: true});
})

// @desc  Update a ticket
// @route PUT /api/tickets/:id
// @access Private
const updateTicket = asyncHandler(async (req, res) => {
  // Get user from DB by the id in the JWT
  const user = await User.findById(req.user.id)

  if(!user) {
    res.status(401)
    throw new Error("User not authorized")
  }

  const ticket = await Ticket.findById(req.params.id)

  if (!ticket) {
    res.status(404)
    throw new Error("Ticket not found")
  }

  if (ticket.user.toString() !== req.user.id) {
    res.status(401)
    throw new Error("Not authorized_")
  }

  const updatedTicket = await Ticket.findByIdAndUpdate(req.params.id, req.body, {new: true})

  res.status(200).json(updatedTicket);
})

module.exports = {
  getTickets,
  getTicket,
  createTicket,
  updateTicket,
  deleteTicket,
}