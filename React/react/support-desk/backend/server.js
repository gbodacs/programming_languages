const express = require("express");
const colors = require("colors");
const dotenv = require("dotenv").config();
const { errorHandler } = require("./middleware/ErrorMiddleware");
const connectDb = require("./config/db");

const PORT = process.env.PORT || 8000;

// Connect to database
connectDb();

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  res.send("hello");
});

app.use("/api/users", require("./routes/UserRoutes"));
app.use("/api/tickets", require("./routes/TicketRoutes"));

app.use(errorHandler);

app.listen(PORT, () => console.log(`Server started on port: ${PORT}`));
