import { useState } from "react";

function useLocalStorage(key, initial) {
  const [localStorageValue, setLocalStorageValue] = useState(() =>
    getLocalStorageValue(key, initial)
  );

  const setValue = (value) => {
    //Check if function
    const valueToStore =
      value instanceof Function ? value(localStorageValue) : value;

    //Set to state
    setLocalStorageValue(value);

    //Set to localstorage
    localStorage.setItem(key, JSON.stringify(valueToStore));
  };

  return [localStorageValue, setValue];
}

function getLocalStorageValue(key, initialValue) {
  const itemFromStorage = localStorage.getItem(key);
  return itemFromStorage ? JSON.parse(itemFromStorage) : initialValue;
}

export default useLocalStorage;
