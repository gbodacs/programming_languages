import { useRef } from "react";

//
// Example how to use useRef() to set color, text or focus
//

function UseRefExample1() {
  const inputRef = useRef();
  const paraRef = useRef();

  const onSubmit = (e) =>{
    e.preventDefault();
    inputRef.current.style.backgroundColor = 'red'
    paraRef.current.innerText = "hello"
  }

  return (
    <div>
      <form onSubmit={onSubmit}>
        <label htmlFor="name">name</label>
        <input
          ref={inputRef}
          type="text"
          id="name"
          className="form-control mb-2"
        />
        <p onClick={() => inputRef.current.focus()} ref={paraRef}>Ez egy paragraph!</p>
        <button className="btn btn-primary" type="submit">
          Submit
        </button>
      </form>
    </div>
  );
}

export default UseRefExample1;
