import useLocalStorage from "../hooks/useLocalStorage";

//
// Save state to local storage
//

function CustomHookExample2() {
  const [task, setTask] = useLocalStorage("task", '');
  const [tasks, setTasks] = useLocalStorage("tasks", []);

  const onSubmit = (e) => {
    e.preventDefault()

    const taskObj = {
      task: task,
      completed: false,
      date: new Date().toLocaleDateString(),
    }

    setTasks([...tasks, taskObj])
  }

  return (
    <form onSubmit={onSubmit} className="w-50">
      <div className="mb-3">
        <label className="form-label">Task</label>
        <input className="form-control"
          type="text"
          value={task}
          onChange={(e) => setTask(e.target.value)}
        />
        <button type="submit" className="btn btn-primary my-5">Submit</button>
      </div>
    </form> 
  );
}

export default CustomHookExample2;
