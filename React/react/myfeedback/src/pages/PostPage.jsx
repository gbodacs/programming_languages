import { useParams } from "react-router-dom";

function PostPage() {
    const params = useParams();
  return (
    <>
      <h1>This is the post page!</h1>
      <p>Name:{params.name}</p>
      <p>ID:{params.id}</p>
    </>
  );
}

export default PostPage;
