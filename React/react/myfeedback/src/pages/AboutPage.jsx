import { Link } from "react-router-dom";
import Card from "../components/shared/Card";

function AboutPage() {
  return (
    <Card>
      <h1>About this app</h1>
      <p>This is a React application</p>
      <p>Version: v1.0.2</p>
      <p>
          <Link to='/'>Back to the home page</Link>
      </p>
    </Card>
  );
}

export default AboutPage;
