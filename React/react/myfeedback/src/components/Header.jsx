function Header({ bgColor, textColor }) {
  const headerStyles = {
    backgroundColor: bgColor,
    color: textColor,
  };

  return (
    <header style={headerStyles}>
      <div className="container">
        <h1>Feedback UI</h1>
      </div>
    </header>
  );
}

export default Header;
