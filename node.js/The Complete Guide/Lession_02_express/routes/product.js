const express = require("express");
const path = require("path");


const router = express.Router();

router.use("/product", (req, res, next) => {
  console.log("prod");
  res.sendFile(path.join(__dirname, "..", "views", "product.html"));
});

router.get("/getproduct", (req, res, next) => {
  console.log("get prod");
  res.send("<h1>Welcome get-product!</h1>");
});

module.exports = router;
