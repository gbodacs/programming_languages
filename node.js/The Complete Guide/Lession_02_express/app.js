const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");

const adminRoutes = require("./routes/admin");
const productRoutes = require("./routes/product");

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));

app.use("/", (req, res, next) => {
  console.log("ez mindig lefut");
  next();
});

app.use("/admin", adminRoutes);
app.use(productRoutes);
app.use(express.static(path.join(__dirname,"public"))); //serve public folder with css files

app.use((req, res, next) => {
  res.status(404).sendFile(path.join(__dirname, "views", "404.html"));
});

app.listen(3000);
