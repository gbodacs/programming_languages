const crypto = require("crypto");
const User = require("../models/user");
const bcrypt = require("bcryptjs");
const nodemailer = require("nodemailer");
const sendGridTransport = require("nodemailer-sendgrid-transport");

const transporter = nodemailer.createTransport(
  sendGridTransport({
    auth: {
      api_key:
        "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",
    },
  })
);

exports.getLogin = (req, res, next) => {
  let message = req.flash("error");
  if (message.length > 0) {
    message = message[0];
  } else {
    message = null;
  }

  res.render("auth/login", {
    path: "/login",
    pageTitle: "Login",
    errorMessage: message,
  });
};

exports.getSignup = (req, res, next) => {
  let message = req.flash("error");
  if (message.length > 0) {
    message = message[0];
  } else {
    message = null;
  }

  res.render("auth/signup", {
    path: "/signup",
    pageTitle: "Signup",
    errorMessage: message,
  });
};

async function postLogin(req, res, next) {
  const email2 = req.body.email;
  const passwd = req.body.password;

  try {
    const user = await User.findOne({ email: email2 });
    if (!user) {
      req.flash("error", "Invalid email or password");
      return res.redirect("/login");
    }
    const doMatch = await bcrypt.compare(passwd, user.password);
    if (doMatch) {
      req.session.isLoggedIn = true;
      req.session.user = user;
      await req.session.save();
      res.redirect("/");
      return;
    } else {
      req.flash("error", "Invalid email or password");
      res.redirect("/login");
      return;
    }
  } catch (err) {
    console.log(err);
  }
}

async function postSignup(req, res, next) {
  const email2 = req.body.email;
  const password = req.body.password;
  const confirmPassword = req.body.confirmPassword;

  try {
    let userDoc = await User.findOne({ email: email2 });
    if (userDoc) {
      req.flash("error", "Email already exists");
      return res.redirect("/signup");
    }
    const hashPwd = await bcrypt.hash(password, 12);
    const user = new User({
      email: email2,
      password: hashPwd,
      cart: { items: [] },
    });
    await user.save();
    transporter.sendMail({
      to: email2,
      from: "ann@relaxing-sleep.com",
      subject: "Signup success!",
      html: "<h1>You successfully  signed up!</h1>",
    });
    res.redirect("/login");
  } catch (error) {
    console.log(error);
  }
}

exports.postLogout = (req, res, next) => {
  req.session.destroy((err) => {
    console.log(err);
    res.redirect("/");
  });
};

async function getResetPassword(req, res, next) {
  let message = req.flash("error");
  if (message.length > 0) {
    message = message[0];
  } else {
    message = null;
  }

  res.render("auth/reset", {
    path: "/reset",
    pageTitle: "Reset Password",
    errorMessage: message,
  });
}

async function postResetPassword(req, res, next) {
  crypto.randomBytes(32, async (err, buffer) => {
    if (err) {
      console.log(err);
      return res.redirect("/reset");
    }
    const token = buffer.toString("hex");

    try {
      let user = await User.findOne({ email: req.body.email });
      if (!user) {
        req.flash("error", "Email not registered");
        return res.redirect("/reset");
      }
      user.resetToken = token;
      user.resetTokenExpiration = Date.now() + 3600000; //1 hour
      await user.save();
      res.redirect("/");
      transporter.sendMail({
        to: req.body.email,
        from: "ann@relaxing-sleep.com",
        subject: "Password reset",
        html: `
        <p>Please click link to reset your password: </p>
        <p>Click this <a href="http://localhost:3000/reset/${token}">link</a> to set a new password</p>
        `,
      });
    } catch (error) {
      console.log(error);
    }
  });
}

async function getNewPassword(req, res, next) {
  const token = req.params.token;
  let user = null;

  try {
    user = await User.findOne({
      resetToken: token,
      resetTokenExpiration: { $gt: Date.now() },
    });
  } catch (error) {
    console.log(error);
  }

  let message = req.flash("error");
  if (message.length > 0) {
    message = message[0];
  } else {
    message = null;
  }

  res.render("auth/new-password", {
    path: "/new-password",
    pageTitle: "New Password",
    errorMessage: message,
    userId: user._id.toString(),
    passwordToken: token,
  });
}

async function postNewPassword(req, res, next) {
  const newPassword = req.body.password;
  const userId = req.body.userId;
  const passwordToken = req.body.passwordToken;

  const user = await User.findOne({
    resetToken: passwordToken,
    resetTokenExpiration: { $gt: Date.now() },
    _id: userId
  });
  const hashedPassword = await bcrypt.hash(newPassword, 12);
  user.password = hashedPassword;
  user.resetToken = null;
  user.resetTokenExpiration = null;
  await user.save();
  res.redirect("/login");
}

module.exports.postSignup = postSignup;
module.exports.postLogin = postLogin;
module.exports.getResetPassword = getResetPassword;
module.exports.postResetPassword = postResetPassword;
module.exports.getNewPassword = getNewPassword;
module.exports.postNewPassword = postNewPassword;
