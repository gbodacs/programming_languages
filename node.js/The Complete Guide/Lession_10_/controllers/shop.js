const Product = require('../models/product');

exports.getProducts = (req, res, next) => {
     Product.fetchAll((prodList) => {
          res.render('shop/index', {
               prods: prodList,
               pageTitle: 'Shop',
               path: '/products',
          });
     });
}

exports.getIndex = (req, res, next) => {
     Product.fetchAll((prodList) => {
          res.render('shop/product-list', {
               prods: prodList,
               pageTitle: 'All Products',
               path: '/',
          });
     }); 
}

exports.getCart = (req, res, next) => {
     res.render("shop/cart", {
          path: '/cart', 
          pageTitle: "Cart Items"
     })
}

exports.getOrders = (req, res, next) => {
     res.render("shop/orders", {
          path: '/orders', 
          pageTitle: "Your Orders"
     })
}

exports.getCheckout = (req, res, next) => {
     res.render("shop/checkout", {
          path: '/shop/checkout', 
          pageTitle: "Checkout"
     })
}