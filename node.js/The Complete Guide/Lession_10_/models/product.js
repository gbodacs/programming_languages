const fs = require("fs");
const path = require("path");

const p = path.join(__dirname, "..", "data", "products.json");

const getProductsFromFile = (x) => {
  fs.readFile(p, (error, fileContent) => {
    if (error) {
      console.log(error);
      x([]);
    } else {
      x(JSON.parse(fileContent));
    }
  });
};

module.exports = class Product2 {
  constructor(title, imageURL, description, price) {
    this.title = title;
    this.imageUrl = imageURL;
    this.description = description;
    this.price = price;
  }

  save() {
    getProductsFromFile( (products)=>{
      products.push(this);
      fs.writeFile(p, JSON.stringify(products), (err) => {
        console.log(err);
      });
    })
  }

  static fetchAll(cb) {
    getProductsFromFile(cb);
  }
};
