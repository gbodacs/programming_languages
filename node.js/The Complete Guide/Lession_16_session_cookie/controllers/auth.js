const User = require('../models/user');

exports.getLogin = (req, res, next) => {
  try {
    console.log(req.session);
    res.render("auth/login", {
      path: "/login",
      pageTitle: "Login",
      isAuthenticated: req.user
    });
  } catch (error) {
    console.log(error);
  }
};

async function postLogin(req, res, next){
  // check username and password
  // save session into cookie
  const user2 = await User.findById('61b9f0d3180bc0545894546c');
  //req.session.isLoggedin = true;
  req.session.user = user2;
  await req.session.save();
  res.redirect("/");
};

async function postLogout(req, res, next){
  req.session.destroy((err)=>{
    console.log(err);
    res.redirect("/");
  });
};

module.exports.postLogin = postLogin;
module.exports.postLogout = postLogout;