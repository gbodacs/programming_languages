const express = require("express");
const path = require("path");

const routes = express();

routes.use("/users", (req, res, next) => {
  res.sendFile(path.join(__dirname, "..", "views", "user.html"));
});

routes.use("/", (req, res, next) => {
  res.sendFile(path.join(__dirname, "..", "views", "welcome.html"));
});

module.exports = routes;