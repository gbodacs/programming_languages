const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const app = express();
const users = [];

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(bodyParser.urlencoded({ extended: false }));

app.post("/add-user", (req, res, next)=> {
     users.push( {user: req.body.user} );
     res.redirect('/');
})

app.get("/user", (req, res, next)=> {
     res.render('user.ejs', {title: "Users", users: users});
})

app.get("/", (req, res, next)=> {
     res.render('home.ejs', {title: "Home"});
})

app.use((req, res, next)=> {
     res.status(404).render('404.ejs');
})

app.listen(3000);