BIMcloud with OAuth2 

*Goals*
The goal is to eliminate multiple problems in our system. Using the current implementation, BIMcloud clients have to use username + password for BIMcloud API authentication. The security risk is that the clients (eg. BIMcloud API user client (like nCircle sync), BIMcloud React WebUI) have to save the username+password for steamless working experience (it would be annoying to get the username+password every time the session ends or the sever restarts etc.)

OAuth 2 is an industrial standard for authentication, the username and password would be safe all the time, access_tokens and refresh_tokens are travelling through the network most of the time. 
Another goals are:
- In case the user changes his/her username or password, the access_token still usable until the expiry_time.
- The tokens are recallable, so every user can add/remove tokens (on the WebUI), so he/she can add/remove any token in an easy and fast way.

More info: https://docs.apigee.com/api-platform/security/oauth/implementing-password-grant-type

*Acceptance criteria*
Istead the old "signed encrypted password", the system should use OAuth 2 tokens.
Old public API calls should work the same way as they should (Github repo example).
A logged in user should remain log in after a server restart or a token expiration (automatic token renewal).


*Out of scope*
BIMcloud licensing.
