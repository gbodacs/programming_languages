const { validationResult } = require("express-validator");

const Post = require('../models/post');
const { post } = require("../routes/feed");

exports.getPosts = (req, res, next) => {
  res.status(200).json({
    posts: [
      {
        _id: "1",
        title: "First post",
        content: "Blah first post",
        imageUrl: "images/duck.jpg",
        creator: {
          name: "Gabor",
        },
        createdAt: new Date(),
      },
    ],
  });
};

exports.createPost = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error("Validation failed! Entered data incorrect.");
    error.statusCode = 422;
    throw error; //Error handling by express
  }

  const title = req.body.title;
  const content = req.body.content;
  const post = new Post({
    title: title,
    content: content,
    imageUrl: 'images/duck.jpg',
    creator: {
      name: "Gabor",
    },
  })
  post.save().then( result =>{
    res.status(201).json({
      message: "Post created successfully",
      post: result
    });
  }).catch( err => {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);    //Error handling by express
  });
};
