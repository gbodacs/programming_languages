const express = require("express");
const bodyParser = require("body-parser");

const feedRoutes = require("./routes/feed");
const res = require("express/lib/response");
const req = require("express/lib/request");

const app = express();

app.use(bodyParser.json()); //Parsing json data in package content

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET,POST,PUT,PATCH,DELETE");
  res.setHeader("Access-Control-Allow-Headers", "Control-Type, Authorization");
  next();
});

app.use("/feed", feedRoutes);

app.listen(8080);
