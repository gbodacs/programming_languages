const express = require("express");
const { check, body } = require("express-validator/check");

const authController = require("../controllers/auth");
const User = require("../models/user");

const router = express.Router();

router.get("/login", authController.getLogin);

router.get("/signup", authController.getSignup);

router.post(
  "/login",
  [
    check("email")
      .isEmail()
      .normalizeEmail()
      .withMessage("Please enter a valid email.")
      .custom(async (value, { req }) => {
        let userDoc = await User.findOne({ email: req.body.email });
        if (!userDoc) {
          return Promise.reject(
            "Email does not exist, please enter a registered email."
          );
        }
      })
      .normalizeEmail(),
    body(
      "password",
      "Please enter a password with alphanumeric characters and at least 5 characters."
    )
    .trim()
    .isLength({ min: 5, max: 30 })
    .isAlphanumeric()
    .trim()
  ],
  authController.postLogin
);

router.post(
  "/signup",
  [
    check("email")
      .isEmail()
      .withMessage("Please enter a valid email.")
      .custom(async (value, { req }) => {
        let userDoc = await User.findOne({ email: req.body.email });
        if (userDoc) {
          return Promise.reject(
            "Email already exist, please pick another one."
          );
        }
      })
      .normalizeEmail(),
    body(
      "password",
      "Please enter a password with alphanumeric characters and at least 5 characters."
      )
      .trim()
      .isLength({ min: 5, max: 30 })
      .isAlphanumeric()
      ,
    body("confirmPassword")
    .trim()
    .custom((value, { req }) => {
      if (value !== req.body.password) {
        throw new Error("Passwords are not equal!");
      }
      return true;
      })
  ],
  authController.postSignup
);

router.post("/logout", authController.postLogout);

router.get("/reset", authController.getResetPassword);

router.post("/reset", authController.postResetPassword);

router.get("/reset/:token", authController.getNewPassword);
router.post("/new-password", authController.postNewPassword);

module.exports = router;
