const User = require('../models/user');
const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const sendGridTransport = require('nodemailer-sendgrid-transport');

const transporter = nodemailer.createTransport(sendGridTransport({
  auth: { 
    api_key: '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'
  }
}));

exports.getLogin = (req, res, next) => {
  let message = req.flash('error');
  if (message.length > 0) {
    message = message[0];
  } else {
    message = null;
  }

  res.render("auth/login", {
    path: "/login",
    pageTitle: "Login",
    errorMessage: message
  });
};

exports.getSignup = (req, res, next) => {
  let message = req.flash('error');
  if (message.length > 0) {
    message = message[0];
  } else {
    message = null;
  }

  res.render("auth/signup", {
    path: "/signup",
    pageTitle: "Signup",
    errorMessage: message
  });
};

async function postLogin(req, res, next) {
  const email2 = req.body.email;
  const passwd = req.body.password;

  try {
    const user = await User.findOne({ email: email2 });
    if (!user) {
      req.flash('error', "Invalid email or password");
      return res.redirect("/login");
    }
    const doMatch = await bcrypt.compare(passwd, user.password);
    if (doMatch) {
      req.session.isLoggedIn = true;
      req.session.user = user;
      await req.session.save();
      res.redirect("/");
      return;
    } else {
      req.flash('error', "Invalid email or password");
      res.redirect("/login");
      return;
    }
  } catch (err) {
    console.log(err);
  }
}

async function postSignup(req, res, next) {
  const email2 = req.body.email;
  const password = req.body.password;
  const confirmPassword = req.body.confirmPassword;

  try {
    let userDoc = await User.findOne({ email: email2 });
    if (userDoc) {
      req.flash('error', "Email already exists");
      return res.redirect("/signup");
    }
    const hashPwd = await bcrypt.hash(password, 12);
    const user = new User({
      email: email2,
      password: hashPwd,
      cart: { items: [] },
    });
    await user.save();
    transporter.sendMail({
      to: email2,
      from: 'ann@relaxing-sleep.com',
      subject: 'Signup success!',
      html: '<h1>You successfully  signed up!</h1>'

    })
    res.redirect("/login");
  } catch (error) {
    console.log(error);
  }
}

exports.postLogout = (req, res, next) => {
  req.session.destroy((err) => {
    console.log(err);
    res.redirect("/");
  });
};

module.exports.postSignup = postSignup;
module.exports.postLogin = postLogin;
