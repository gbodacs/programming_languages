const Product = require("../models/product");

exports.getProducts = (req, res, next) => {
  Product.findAll()
    .then((products) => {
      res.render("shop/product-list", {
        prods: products,
        pageTitle: "All products",
        path: "/products",
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getProduct = (req, res, next) => {
  const prodId = req.params.productId;
  product
    .findAll({ where: { id: prodId } })
    .then((products) => {
      res.render("shop/product-detail", {
        product: products[0],
        pageTitle: products[0].title,
        path: "/product",
      });
    })
    .catch((err) => {
      console.log(err);
    });

  // Product.findByPk(+prodId)
  //   .then(product2 => {
  //     console.log(product2);
  //     res.render("shop/product-detail", {
  //       product: product2.dataValues,
  //       pageTitle: product2.dataValues.title,
  //       path: "/product"})
  //     })
  //   .catch(err => {
  //     console.log(err)
  //   });
};

exports.getIndex = (req, res, next) => {
  Product.findAll()
    .then((products) => {
      res.render("shop/index", {
        prods: products,
        pageTitle: "Shop",
        path: "/",
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getCart = (req, res, next) => {
  req.user
    .getCart()
    .then((cart) => {
      return cart.getProducts();
    })
    .then((cartProducts) => {
      res.render("shop/cart", {
        path: "/cart",
        pageTitle: "Your Cart",
        products: cartProducts,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.postCart = (req, res, next) => {
  const prodId = req.body.productId;
  let fetchedCart;
  let newQuantity = 1;

  req.user
    .getCart()
    .then((cart) => {
      fetchedCart = cart; //Search for the item in the cart
      return cart.getProducts({ where: { id: prodId } });
    })
    .then((products) => {
      let prod2;
      if (products.length > 0) {
        prod2 = products[0];
      }

      if (prod2) {
        // Product already in the cart -> qty+1
        oldQuantity = prod2.cartItem.quantity;
        newQuantity = oldQuantity + 1;
        return prod2;
      } else {
        // Product not in the cart
        return Product.findByPk(prodId);
      }
    })
    .then((prod3) => {
      return fetchedCart.addProduct(prod3, {
        through: { quantity: newQuantity },
      });
    })
    .then(() => {
      res.redirect("/cart");
    })
    .catch((err) => {});
};

exports.postCartDeleteProduct = (req, res, next) => {
  const prodId = req.body.productId;
  req.user
    .getCart()
    .then((cart) => {
      return cart.getProducts({ where: { id: prodId } });
    })
    .then((products_) => {
      if (products_.length > 0) {
        const prod2 = products_[0];
        return prod2.cartItem.destroy();
      }
    })
    .then((result) => {
      res.redirect("/cart");
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.postOrder = (req, res, next) => {
  let fetchedCart;
  req.user
    .getCart()
    .then((cart) => {
      fetchedCart = cart;
      return cart.getProducts();
    })
    .then((products_) => {
      req.user
        .createOrder()
        .then((order) => {
          return order.addProducts(
            products_.map((product) => {
              product.orderItem = { quantity: product.cartItem.quantity };
              return product;
            })
          );
        })
        .then((result) => {
          return fetchedCart.setProducts(null);
        })
        .then(result=>{
          res.redirect("/orders");
        })
        .catch((err) => {
          console.log(err);
        });
    });
};

exports.getOrders = (req, res, next) => {
  req.user
  .getOrders({include: ['products']})
  .then(orders => {
    res.render("shop/orders", {
      path: "/orders",
      pageTitle: "Your Orders",
      orders: orders
    });
  })
  .catch(err=>{
    console.log(err);
  })

};
