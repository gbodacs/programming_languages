const Product = require("../models/product");

function getAddProduct(req, res, next) {
  res.render("admin/edit-product", {
    pageTitle: "Add Product",
    path: "/admin/add-product",
    editing: false,
  });
}

async function getEditProduct(req, res, next) {
  const editMode = req.query.edit;

  if (!editMode) {
    return res.redirect("/");
  }

  const prodId = req.params.productId;

  try {
    let prods = await req.user.getProducts({ where: { id: prodId } });
    let product = prods[0];
    if (!product) {
      return res.redirect("/");
    }

    res.render("admin/edit-product", {
      pageTitle: "Edit existing Product",
      path: "/admin/edit-product",
      editing: editMode,
      product: product,
    });
  } catch (error) {
    console.log(error);
  }
}

async function postEditProduct(req, res, next) {
  const prodId = req.body.productId;
  const updatedTitle = req.body.title;
  const updatedImageUrl = req.body.imageUrl;
  const updatedPrice = req.body.price;
  const updatedDescription = req.body.description;

  try {
    let prod2 = await Product.findByPk(prodId);

    prod2.title = updatedTitle;
    prod2.price = updatedPrice;
    prod2.imageUrl = updatedImageUrl;
    prod2.description = updatedDescription;

    await prod2.save();
    res.redirect("/admin/products");
  } catch (error) {
    console.log(error);
  }
}

async function postDeleteProduct(req, res, next) {
  const prodId = req.body.productId;
  try {
    let prod2 = await Product.findByPk(prodId);
    if (prod2) {
      await prod2.destroy();
      console.log("Product deleted!");
      res.redirect("/admin/products");
    }
  } catch (error) {
    console.log(error);
  }
}

async function postAddProduct(req, res, next) {
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;

  try {
    await req.user.createProduct({
      title: title,
      imageUrl: imageUrl,
      price: price,
      description: description,
    });
    //console.log(result);
    res.redirect("/admin/products");
  } catch (error) {
    console.log(error);
  }
}

async function getProducts(req, res, next) {
  try {
    let products2 = await req.user.getProducts();
    await res.render("admin/products", {
      prods: products2,
      pageTitle: "Admin Products",
      path: "/admin/products",
    });
  } catch (error) {
    console.log(error);
  }
}

module.exports.getProducts = getProducts;
module.exports.postAddProduct = postAddProduct;
module.exports.postDeleteProduct = postDeleteProduct;
module.exports.postEditProduct = postEditProduct;
module.exports.getEditProduct = getEditProduct;
module.exports.getAddProduct = getAddProduct;