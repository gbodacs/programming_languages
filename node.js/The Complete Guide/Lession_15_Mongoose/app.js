const path = require("path");

const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const errorController = require("./controllers/error");
const User = require("./models/user");

const app = express();

app.set("view engine", "ejs");
app.set("views", "views");

const adminRoutes = require("./routes/admin");
const shopRoutes = require("./routes/shop");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

//Store user in req
app.use(async (req, res, next) => {
  try {
    const user2 = await User.findById("61b9f0d3180bc0545894546c");
    req.user = user2;
    next();
  } catch (err) {
    console.log(err);
  }
});

// Routes
app.use("/admin", adminRoutes);
app.use(shopRoutes);
app.use(errorController.get404);

async function Main() {
  try {
    await mongoose.connect("mongodb://127.0.0.1:27017/test");
    let user2 = await User.findOne();
    if (!user2) {
      const user = new User({
        name: "Gabor",
        email: "gabor@gmail.com",
        cart: { items: [] },
      });
      user.save();
    }

    app.listen(3000);
  } catch (error) {
    console.log(error);
  }
}

Main();
