const Product = require("../models/product");
const Order = require("../models/order");

async function getProducts(req, res, next) {
  const products = await Product.find();
  res.render("shop/product-list", {
    prods: products,
    pageTitle: "All products",
    path: "/products",
  });
}

async function getIndex(req, res, next) {
  const products = await Product.find();
  res.render("shop/index", {
    prods: products,
    pageTitle: "Shop",
    path: "/",
  });
}

async function getProduct(req, res, next) {
  const prodId = req.params.productId;
  const prod = await Product.findById(prodId);
  res.render("shop/product-detail", {
    product: prod,
    pageTitle: prod.title,
    path: "/product",
  });
}

async function getCart(req, res, next) {
  try {
    let user2 = await req.user.populate("cart.items.productId"); //ezzel belekerul a product adata a user alatti cart-ba
    console.log(user2.cart.items);
    let products = user2.cart.items;
    res.render("shop/cart", {
      path: "/cart",
      pageTitle: "Your Cart",
      products: products,
    });
  } catch (err) {
    console.log(err);
  }
}

async function postCart(req, res, next) {
  const prodId = req.body.productId;
  let prod = await Product.findById(prodId);
  let res22 = await req.user.addToCart(prod);
  res.redirect("/cart");
}

async function postCartDeleteProduct(req, res, next) {
  const prodId = req.body.productId;
  try {
    await req.user.removeFromCart(prodId);
  } catch (err) {
    console.log(err);
  }

  res.redirect("/cart");
}

async function postOrder(req, res, next) {
  try {
    let user2 = await req.user.populate("cart.items.productId"); //ezzel belekerul a product adata a user alatti cart-ba
    console.log(user2.cart.items);
    let products = user2.cart.items.map((i) => {
      return { quantity: i.quantity, product: { ...i.productId._doc } };
    });

    let fetchedCart;
    const order = new Order({
      user: {
        name: req.user.name,
        userId: req.user, //Az 'Id' automatikusan belekerul!
      },
      products: products,
    });
    await order.save();
    await req.user.clearCart();

    res.redirect("/orders");
  } catch (err) {
    console.log(err);
  }
}

async function getOrders(req, res, next) {
  try {
    let orders = await Order.find({ "user.userId": req.user._id });

    res.render("shop/orders", {
      path: "/orders",
      pageTitle: "Your Orders",
      orders: orders,
    });
  } catch (err) {
    console.log(err);
  }
}

module.exports.getIndex = getIndex;
module.exports.getProducts = getProducts;
module.exports.getProduct = getProduct;
module.exports.getCart = getCart;
module.exports.getOrders = getOrders;
module.exports.postCart = postCart;
module.exports.postCartDeleteProduct = postCartDeleteProduct;
module.exports.postOrder = postOrder;
