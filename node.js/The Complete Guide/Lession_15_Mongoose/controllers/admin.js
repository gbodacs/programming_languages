const product = require("../models/product");
const Product = require("../models/product");

function getAddProduct(req, res, next) {
  res.render("admin/edit-product", {
    pageTitle: "Add Product",
    path: "/admin/add-product",
    editing: false,
  });
}

async function postAddProduct(req, res, next) {
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;
  const product = new Product({
    title: title,
    price: price,
    description: description,
    imageUrl:imageUrl,
    userId: req.user //mongoose use only the _id from the object
  });

  try {
    await product.save();
    res.redirect("/admin/products");
  } catch (error) {
    console.log(error);
  }
}

async function getEditProduct(req, res, next) {
  const editMode = req.query.edit;

  if (!editMode) {
    return res.redirect("/");
  }

  const prodId = req.params.productId;

  try {
    let product = await Product.findById(prodId);
    if (!product) {
      return res.redirect("/");
    }

    res.render("admin/edit-product", {
      pageTitle: "Edit existing Product",
      path: "/admin/edit-product",
      editing: editMode,
      product: product,
    });
  } catch (error) {
    console.log(error);
  }
}

async function postEditProduct(req, res, next) {
  const prodId = req.body.productId;
  const updatedTitle = req.body.title;
  const updatedImageUrl = req.body.imageUrl;
  const updatedPrice = req.body.price;
  const updatedDescription = req.body.description;

  try {
    let product = await Product.findById(prodId);
    product.title = updatedTitle;
    product.price = updatedPrice;
    product.description = updatedDescription;
    product.imageUrl = updatedImageUrl;
    await product.save();
    res.redirect("/admin/products");
  } catch (error) {
    console.log(error);
  }
}

async function postDeleteProduct(req, res, next) {
  const prodId = req.body.productId;
  try {
    let prod2 = await Product.findByIdAndRemove(prodId);
    console.log("Product deleted!");
    res.redirect("/admin/products");
  } catch (error) {
    console.log(error);
  }
}

async function getProducts(req, res, next) {
  try {
    let products = await Product.find();
    //.select('title price -_id') // mi legyen a lekérdezés eredménye, mit rakjon bele és mit hagyjon ki belöle
    //.populate('userId'); //UserId alapjaán a user adatok automatikusan bekerülnek az eredménybe az _id helyére!
    await res.render("admin/products", {
      prods: products,
      pageTitle: "Admin Products",
      path: "/admin/products",
    });
  } catch (error) {
    console.log(error);
  }
}

module.exports.getProducts = getProducts;
module.exports.getAddProduct = getAddProduct;
module.exports.postAddProduct = postAddProduct;
module.exports.postDeleteProduct = postDeleteProduct;
module.exports.postEditProduct = postEditProduct;
module.exports.getEditProduct = getEditProduct;
