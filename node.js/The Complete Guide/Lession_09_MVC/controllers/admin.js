const Product = require('../models/product');

exports.getAddProduct = (req, res, next) => {
     res.render('admin/add-product', {
       pageTitle: 'Add Product',
       path: '/admin/add-product',
       formsCSS: true,
       productCSS: true,
       activeAddProduct: true
     });
}

exports.postAddProduct = (req, res, next) => {
     const Product2 = new Product(req.body.title);
     Product2.save();
     res.redirect('/');
};

exports.getEditProduct = (req, res, next) => {
     res.render('admin/edit-product', {
       pageTitle: 'Edit Product',
       path: '/admin/edit-product',
     });
}

exports.postEditProduct = (req, res, next) => {
     res.redirect('/');
};

exports.getProducts = (req, res, next) => {
     Product.fetchAll((prodList) => {
          res.render('admin/products', {
               prods: prodList,
               pageTitle: 'Admin Products',
               path: '/admin/products',
          });
     }); 
}