const Product = require('../models/product');

exports.getProducts = (req, res, next) => {
     Product.fetchAll((prodList) => {
          res.render('shop/index', {
               prods: prodList,
               pageTitle: 'Shop',
               path: '/',
          });
     });
}

exports.getIndex = (req, res, next) => {
     Product.fetchAll((prodList) => {
          res.render('shop/product-list', {
               prods: prodList,
               pageTitle: 'All Products',
               path: '/products',
          });
     }); 
}

exports.getCart = (req, res, next) => {
     res.render("shop/cart", {
          path: '/cart', 
          pageTitle: "Cart Items"
     })
}

exports.getCheckout = (req, res, next) => {
     res.render("shop/checkout", {
          path: '/shop/checkout', 
          pageTitle: "Checkout"
     })
}