const express = require('express');

const app = express();

app.use('/add-product',(req, res, next) => {
     console.log("first!");
     res.send("<h1>Add prod!</h1>");
});

app.use('/', (req, res, next) => {
     console.log("second!");
     res.send("<h1>Greetings!</h1>");
});

app.listen(3000);