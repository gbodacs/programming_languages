const mongodb = require("mongodb");
const GetDb = require("../util/database").GetDb;

class Product {
  constructor(title, price, description, imageUrl, id, userId) {
    this.title = title;
    this.price = price;
    this.description = description;
    this.imageUrl = imageUrl;
    this.userId = userId;
    if (id) {
      this._id = new mongodb.ObjectId(id);
    } else {
      this._id = null;
    }

    
  }

  async save() {
    const db = GetDb();
    let dbOp;
    if (this._id) {
      try { //UPDATE!
        let coll = await db.collection("products")
        coll.updateOne({ _id: this._id }, { $set: this });
        console.log(res);
      } catch (error) {
        console.log(error);
      }
    } else {
      try { //ADD NEW PRODUCT
        let coll = await db.collection("products")
        let res = await coll.insertOne(this);
        console.log(res);
      } catch (error) {
        console.log(error);
      }
    }
  }

  static async getProduct(prodId) {
    const db = GetDb();
    let prod = null;
    try {
      let coll = await db.collection("products");
      prod = coll.find({ _id: new mongodb.ObjectId(prodId) });
      console.log(prod);
    } catch (error) {
      console.log(error);
    }
    return prod;
  }

  static async fetchAll() {
    const db = GetDb();
    let prods = [];
    try {
      prods = await db.collection("products").find().toArray();
    } catch (error) {
      console.log(error);
    }
    return prods;
  }

  static async deleteProduct(prodId) {
    const db = GetDb();
    try {
      let res = await db.collection('products').deleteOne({_id: new mongodb.ObjectId(prodId)});
      console.log("Product deleted!");
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = Product;
