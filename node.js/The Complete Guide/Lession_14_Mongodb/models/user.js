const GetDb = require("../util/database").GetDb;
const mongodb = require("mongodb");

class User {
  constructor(username, email, cart, id) {
    this.name = username;
    this.email = email;
    this.cart = cart; // {items: {}}
    this._id = id;
  }

  async save() {
    const db = GetDb();
    try {
      //ADD NEW User
      let res = await db.collection("users").insertOne(this);
      console.log(res);
    } catch (error) {
      console.log(error);
    }
  }

  async deleteItemFromCart(productId) {
    const updatedCartItems = this.cart.items.filter((item) => {
      return item.productId.toString() !== productId.toString();
    });

    const db = GetDb();
    const coll = await db.collection("users");
    await coll.updateOne(
      { _id: new mongodb.ObjectId(this._id) },
      { $set: { cart: { items: updatedCartItems } } }
    );
  }

  async addToCart(product) {
    const cartProductIndex = this.cart.items.findIndex((cp) => {
      return cp.productId.toString() === product._id.toString();
    });
    let newQty = 1;
    const updatedCartItems = [...this.cart.items];

    if (cartProductIndex >= 0) {
      newQty = updatedCartItems[cartProductIndex].quantity + 1;
      updatedCartItems[cartProductIndex].quantity = newQty;
    } else {
      updatedCartItems.push({
        productId: new mongodb.ObjectId(product._id),
        quantity: newQty,
      });
    }

    const updatedCart = { items: updatedCartItems };
    const db = GetDb();
    const coll = await db.collection("users");
    await coll.updateOne(
      { _id: new mongodb.ObjectId(this._id) },
      { $set: { cart: updatedCart } }
    );
  }

  async getCart() {
    const db = GetDb();
    const prodIds = this.cart.items.map((i) => {
      return i.productId;
    });

    let products = await db
      .collection("products")
      .find({ _id: { $in: prodIds } })
      .toArray();

    return products.map((p) => {
      return {
        ...p,
        quantity: this.cart.items.find((i) => {
          return i.productId.toString() === p._id.toString();
        }).quantity,
      };
    });
  }

  async getOrders() {
    const db = GetDb();
    const user2 = await db
      .collection("orders")
      .find({ "user._id": new mongodb.ObjectId(this._id) });
      return user2.toArray();
  }

  async addOrder() {
    const db = GetDb();
    let prods = await this.getCart();

    try {
      const order = {
        items: prods,
        user: {
          _id: new mongodb.ObjectId(this._id),
          name: this.name,
          email: this.email,
        },
      };
      let res = await db.collection("orders").insertOne(order); // Save cart to db

      this.cart = { items: [] }; // Clear cart in memory
      const coll = await db.collection("users");
      await coll.updateOne(
        { _id: new mongodb.ObjectId(this._id) },
        { $set: { cart: { items: [] } } } // Clear cart in db
      );
    } catch (err) {
      console.log(err);
    }
  }

  static async getUserById(id) {
    const db = GetDb();
    try {
      const coll = await db.collection("users");
      const res = coll.findOne({ _id: new mongodb.ObjectId(id) });
      return res;
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = User;
