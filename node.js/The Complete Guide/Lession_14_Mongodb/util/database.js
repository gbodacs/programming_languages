const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

let _db;

async function mongoConnect() {
    try {
        let client = await MongoClient.connect("mongodb://127.0.0.1:27017");
        _db = client.db("test");
    } catch (error) {
        console.log(error);
        throw error;
    }
}

const GetDb = ()=>{
    if (_db) {
        return _db;
    }
    throw 'No database found!';
}

exports.mongoConnect = mongoConnect;
exports.GetDb = GetDb;