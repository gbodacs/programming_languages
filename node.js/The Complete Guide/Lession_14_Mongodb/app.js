const path = require("path");

const express = require("express");
const bodyParser = require("body-parser");
const errorController = require("./controllers/error");
const mongoConnect = require("./util/database").mongoConnect;
const User = require("./models/user");

const app = express();

app.set("view engine", "ejs");
app.set("views", "views");

const adminRoutes = require("./routes/admin");
const { GetDb } = require("./util/database");
const shopRoutes = require("./routes/shop");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

//Store user in req
app.use(async(req, res, next) => {
  try {
    const user2 = await User.getUserById("61b767462dce0c515c62a1a6");
    req.user = new User(user2.name, user2.email, user2.cart, user2._id);
    next();
  } catch (err) {
    console.log(err);
  }
});

// Routes
app.use("/admin", adminRoutes);
app.use(shopRoutes);
app.use(errorController.get404);

async function Main() {
  await mongoConnect();
  app.listen(3000);
}

Main();
