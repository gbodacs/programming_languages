const Product = require("../models/product");

async function getProducts(req, res, next) {
  const products = await Product.fetchAll();
  res.render("shop/product-list", {
    prods: products,
    pageTitle: "All products",
    path: "/products",
  });
}

async function getIndex(req, res, next) {
  const products = await Product.fetchAll();
  res.render("shop/index", {
    prods: products,
    pageTitle: "Shop",
    path: "/",
  });
}

async function getProduct(req, res, next) {
  const prodId = req.params.productId;
  const prod = await Product.getProduct(prodId);
  res.render("shop/product-detail", {
    product: prod,
    pageTitle: prod.title,
    path: "/product",
  });
}

exports.getCart = (req, res, next) => {
  req.user
    .getCart()
    .then((products) => {
      res.render("shop/cart", {
        path: "/cart",
        pageTitle: "Your Cart",
        products: products,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

async function postCart(req, res, next) {
  const prodId = req.body.productId;
  let prod = await Product.getProduct(prodId);
  let prod2 = await prod.next();

  let res22 = await req.user.addToCart(prod2);
  console.log(res22);
  res.redirect("/cart");
}

async function postCartDeleteProduct(req, res, next) {
  const prodId = req.body.productId;
  try {
    await req.user.deleteItemFromCart(prodId);
  } catch (err) {
    console.log(err);
  }

  res.redirect("/cart");
}

async function postOrder(req, res, next) {
  let fetchedCart;
  try {
    await req.user.addOrder();
    res.redirect("/orders");
  } catch (err) {
    console.log(err);
  }
}

exports.getOrders = (req, res, next) => {
  req.user
    .getOrders()
    .then((orders) => {
      res.render("shop/orders", {
        path: "/orders",
        pageTitle: "Your Orders",
        orders: orders,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

module.exports.getIndex = getIndex;
module.exports.getProducts = getProducts;
module.exports.getProduct = getProduct;
module.exports.postCart = postCart;
module.exports.postCartDeleteProduct = postCartDeleteProduct;
module.exports.postOrder = postOrder;
