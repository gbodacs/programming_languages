const Product = require("../models/product");

function getAddProduct(req, res, next) {
  res.render("admin/edit-product", {
    pageTitle: "Add Product",
    path: "/admin/add-product",
    editing: false,
  });
}

async function postAddProduct(req, res, next) {
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;
  const product = new Product(title, price, description, imageUrl, null, req.user._id);

  try {
    await product.save();
    res.redirect("/admin/products");
  } catch (error) {
    console.log(error);
  }
}

async function getEditProduct(req, res, next) {
  const editMode = req.query.edit;

  if (!editMode) {
    return res.redirect("/");
  }

  const prodId = req.params.productId;

  try {
    let product = await Product.getProduct(prodId);
    if (!product) {
      return res.redirect("/");
    }

    res.render("admin/edit-product", {
      pageTitle: "Edit existing Product",
      path: "/admin/edit-product",
      editing: editMode,
      product: product,
    });
  } catch (error) {
    console.log(error);
  }
}

async function postEditProduct(req, res, next) {
  const prodId = req.body.productId;
  const updatedTitle = req.body.title;
  const updatedImageUrl = req.body.imageUrl;
  const updatedPrice = req.body.price;
  const updatedDescription = req.body.description;

  try {
    const product = new Product(
      updatedTitle,
      updatedPrice,
      updatedDescription,
      updatedImageUrl,
      prodId
    );
    await product.save();
    res.redirect("/admin/products");
  } catch (error) {
    console.log(error);
  }
}

async function postDeleteProduct(req, res, next) {
  const prodId = req.body.productId;
  try {
    let prod2 = await Product.deleteProduct(prodId);
    console.log("Product deleted!");
    res.redirect("/admin/products");
  } catch (error) {
    console.log(error);
  }
}

async function getProducts(req, res, next) {
  try {
    let products = await Product.fetchAll();
    await res.render("admin/products", {
      prods: products,
      pageTitle: "Admin Products",
      path: "/admin/products",
    });
  } catch (error) {
    console.log(error);
  }
}

module.exports.getProducts = getProducts;
module.exports.getAddProduct = getAddProduct;
module.exports.postAddProduct = postAddProduct;
module.exports.postDeleteProduct = postDeleteProduct;
module.exports.postEditProduct = postEditProduct;
module.exports.getEditProduct = getEditProduct;
