import { Globals } from "./base/globals";
import Log from "./base/log/log";
import Config from "./base/config/config";
import Mongoose from "./base/mongoose/mongoose";

console.log("test");

Globals.Init();
let log: Log = Globals.GetLog();
let config: Config = Globals.GetConfig();
//let mongoose: Mongoose = Globals.GetMongoose();

log.LogInfo("Info test");
log.LogWarning("Warning test");
log.LogError("Error test");

const version: number = config.getConfigNumber("version");
const name: string = config.getConfigString("name");
const arr: [] = config.getConfigArray("testArray");

Globals.Done();