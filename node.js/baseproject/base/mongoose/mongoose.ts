import { GlobalBase } from "../globalbase";
import { Globals } from "./../globals";
import Log from "../log/log";
import Config from "../config/config";

import { Schema, model, connect, disconnect } from "mongoose";
//import session from "express-session";
//const MongoDBStore = require("connect-mongodb-session")(session);

export = class MongoDB22 extends GlobalBase {
  constructor() {
    super();
  }

  //   mongoose_: any = mongoose;
  //   session_: any = session;
  //   mongoDbStore: any = undefined;
  mongoDbURI: string = "";

  async Init(): Promise<void> {
    let Logger: Log = Globals.GetLog();

    this.mongoDbURI = Globals.GetConfig().getConfigString("MongoDB_URI");
    if (this.mongoDbURI == "") {
      Logger.LogExit("MongoDB URI is empty in the config file!", 3);
    }

    /*  const mongoDbStore = new MongoDBStore({
      uri: this.mongoDbURI,
      collection: "sessions",
    });*/

    try {
      const result = await connect(this.mongoDbURI);
    } catch (error) {
      Logger.LogError("Cannot connect to database! /mongodb/");
      Logger.LogExit(error.toString(), 2);
    }
  }

  async Done(): Promise<void> {
    let Logger: Log = Globals.GetLog();
    try {
      const result = await disconnect();
    } catch (error) {
      Logger.LogError("Cannot disconnect from database! /mongodb/");
      Logger.LogExit(error.toString(), 2);
    }
  }
};
