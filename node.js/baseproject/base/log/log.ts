import { GlobalBase } from "../globalbase";
import * as fs from "fs/promises";
import { stringify } from "querystring";
import { createWriteStream, WriteStream } from "fs";

export = class Log extends GlobalBase {
  constructor() {
    super();
  }

  fileHandle: WriteStream;

  async Init(): Promise<void> {
    try {
      const dateNow: Date = new Date(Date.now());
      const year: string = dateNow.getFullYear().toString();
      const month: string = dateNow.getMonth().toString();
      const date: string = dateNow.getDate().toString();

      const fileName: string =
        "log/" + year + "-" + month + "-" + date + "-log.txt";

        this.fileHandle = createWriteStream(fileName);
    } finally {
      this.fileHandle.close();
    }
  }

  async Done(): Promise<void> {
    await this.fileHandle.close();
  }

  PrepareOutString(level: string, outStr: string): string {
    const dateNow: Date = new Date(Date.now());
    const hour: string = dateNow.getUTCHours().toString();
    const minute: string = dateNow.getUTCMinutes().toString();
    const second: string = dateNow.getUTCSeconds().toString();
    const msecond: string = dateNow.getMilliseconds().toString();

    return (hour+":"+minute+":"+second+"."+msecond+"|"+level+"|"+outStr);
  }

  async WriteOutLogLine(level: string, outLine: string) {
    try {
      let outStr:string = this.PrepareOutString(level, outLine);
      console.log(outStr);
      let outBuf:Buffer = Buffer.from(outStr, "utf-8");
      this.fileHandle.write(outStr);  
    } catch (error) {
      console.log(Error);
    }
  }

  LogInfo(out: string) {
    this.WriteOutLogLine("Info", out);
  }

  LogWarning(out: string) {
    this.WriteOutLogLine("Warn", out);
  }

  LogError(out: string) {
    this.WriteOutLogLine("Erro", out);
  }

  LogExit(out: string, exitCode: number) {
    this.WriteOutLogLine("Exit", out);
    process.exitCode = exitCode; // Graceful shutdown, does not exit immediately!
  }
};
