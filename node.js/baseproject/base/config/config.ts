import { readFile } from "fs/promises";
import { GlobalBase } from "../globalbase";
import { readFileSync } from "fs";
import { Globals } from "./../globals";
import Log from "../log/log";

export = class Config extends GlobalBase {
  constructor() {
    super();
  }

  configStorage: string = "";

  Init(): void {
    try {
      const tempString = readFileSync("appconfig.json").toString();
      this.configStorage = JSON.parse(tempString);
    } catch (error) {
      let Logger:Log = Globals.GetLog();
      Logger.LogError("Cannot log config file! /appconfig.json/");
      Logger.LogExit(error.toString(), 1);
    }
  }

  Done(): void {}

  getConfigString(configName: string): string {
    return this.configStorage[configName];
  }

  getConfigNumber(configName: string): number {
    return this.configStorage[configName];
  }

  getConfigArray(configName: string): [] {
    return this.configStorage[configName];
  }
};
