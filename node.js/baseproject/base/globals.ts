import Log from "./log/log";
import Config from "./config/config";
import Mongoose from "./mongoose/mongoose";

export class Globals_ {
  //Singleton class
  private static instance: Globals_ = undefined;
  public static getInstance(): Globals_ {
    if (!Globals_.instance) {
      Globals_.instance = new Globals_();
    }

    return Globals_.instance;
  }

  // Global variables coming from this point
  private _log: Log = new Log();
  private _config: Config = new Config();
  //private _mongoose: Mongoose = new Mongoose();

  private InitSignals() {
    {
      process.on("SIGTERM", () => {
        this._log.LogInfo("SIGTERM signal received.");

        this._log.LogInfo("Closing http server.");
        //express server close
        //server.close(() => {
        //  console.log('Http server closed.');
        Globals.Done();
        process.exit(0);
        //  });
        //});
      });

      process.on("SIGINT", () => {
        this._log.LogInfo("SIGINT signal received.");

        //log.LogInfo("Closing http server.");
        //express server close
        //server.close(() => {
        //  console.log('Http server closed.');
        Globals.Done();
        process.exitCode = 0;
        //  });
        //});
      });
    }
  }

  private constructor() {}

  public Init(): void {
    this._log.Init();
    this._config.Init();
    //this._mongoose.Init();
    this._log.LogInfo("Globals.Init() called");
  }

  public Done(): void {
    //this._mongoose.Done();
    this._config.Done();
    this._log.LogInfo("Globals.Done() called");
    this._log.Done();
  }

  GetLog(): Log {
    return this._log;
  }
  GetConfig(): Config {
    return this._config;
  }
  //GetMongoose(): Mongoose {return this._mongoose;}
}

export let Globals: Globals_ = Globals_.getInstance();
