
export abstract class GlobalBase {
    abstract Init(): void; // The Init function called after initialization of everything else
    abstract Done(): void; // The Done function called before quit
}