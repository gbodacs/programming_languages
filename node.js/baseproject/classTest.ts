interface iView {
  width: number;
};

class iGreat implements iView
{
  private len: any;
  width: number;

  Add(x: number, y: number): boolean {
    return false;
  }
};

class Greeter extends iGreat{
  greeting: string;

  constructor(message: string) {
    super();
    this.greeting = message;
    this.width = 12;
  }

  greet():string {
    return "Hello, " + this.greeting;
  }
};

let greeter = new Greeter("world");

type NumOrString = number | string;

function Val(i: Greeter, o: NumOrString) {

}

//---------------------------------------
//---------------------------------------
abstract class Base {
  abstract getName(): string;
 
  printName() {
    console.log("Hello, " + this.getName());
  }
}
 
//const b = new Base();
class Derived extends Base {
  getName() {
    return "world";
  }
}
 
const d = new Derived();
d.printName();