const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var prioritySchema = new Schema({
    priority: {
         type: String,
         enum : ['low','medium', 'high', 'critical'],
         default: 'medium'
     },
 })

 module.exports = mongoose.model('Priority', prioritySchema);