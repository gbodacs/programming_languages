const express = require('express');
const { body } = require('express-validator/check');

const ticketController = require('../controllers/ticket');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

// GET 
router.get('/tickets', isAuth, ticketController.getTickets);

// POST 
router.post('/ticket',isAuth,
  [
    body('title')
      .trim()
      .isLength({ min: 5 }),
    body('description')
      .trim()
      .isLength({ min: 5 }),
    body('status')
      .trim()
      .isLength({ min: 3 })
  ],
  ticketController.createTicket
);

router.get('/ticket/:ticketId', isAuth, ticketController.getTicket);

router.put(
  '/ticket/:ticketId',
  isAuth,
  [
    body('title')
      .trim()
      .isLength({ min: 5 }),
    body('content')
      .trim()
      .isLength({ min: 5 }),
    body('status')
        .trim()
        .isLength({ min: 3 })
  ],
  ticketController.updateTicket
);

router.delete('/ticket/:ticketId', isAuth, ticketController.deleteTicket);

module.exports = router;
