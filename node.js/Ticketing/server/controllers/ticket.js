const fs = require('fs');
const path = require('path');

const { validationResult } = require('express-validator/check');

const Ticket = require('../models/ticket');
const User = require('../models/user');

exports.getTickets = (req, res, next) => {
  const currentPage = req.query.page || 1;
  const perPage = 2;
  let totalItems;
  Ticket.find()
    .countDocuments()
    .then(count => {
      totalItems = count;
      return Ticket.find()
        .skip((currentPage - 1) * perPage)
        .limit(perPage);
    })
    .then(tickets => {
      res.status(200).json({
        message: 'Fetched tickets successfully.',
        tickets: tickets,
        totalItems: totalItems
      });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.createTicket = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error('Validation failed, entered data is incorrect.');
    error.statusCode = 422;
    throw error;
  }

  const title = req.body.title;
  const status = req.body.content;
  const description = req.body.description;
  const priority = req.body.priority;
  const assignedTo = req.body.assignedTo;
  const creator = req.userId;
  
  const ticket = new Ticket({
    title: title,
    description: description,
    status: status,
    priority: priority,
    creator: creator,
    assignedTo: assignedTo,
  });
  ticket
    .save()
    .then(result => {
      res.status(201).json({
        message: 'Ticket created successfully!',
        ticket: ticket,
        creator: { _id: creator }
      });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.getTicket = (req, res, next) => {
  const ticketId = req.params.ticketId;
  Ticket.findById(ticketId)
    .then(ticket => {
      if (!ticket) {
        const error = new Error('Could not find ticket.');
        error.statusCode = 404;
        throw error;
      }
      res.status(200).json({ message: 'Ticket fetched.', ticket: ticket });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.updateTicket = (req, res, next) => {
  const ticketId = req.params.ticketId;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error('Validation failed, entered data is incorrect.');
    error.statusCode = 422;
    throw error;
  }

  const title = req.body.title;
  const status = req.body.content;
  const description = req.body.description;
  const priority = req.body.priority;
  const assignedTo = req.body.assignedTo;
  const creator = req.userId;

  Ticket.findById(ticketId)
    .then(ticket => {
      if (!ticket) {
        const error = new Error('Could not find ticket.');
        error.statusCode = 404;
        throw error;
      }
      if (ticket.creator.toString() !== creator) {
        const error = new Error('Not the author!');
        error.statusCode = 403;
        throw error;
      }

      ticket.title = title;
      ticket.status = status;
      ticket.description = description;
      ticket.priority = priority;
      ticket.assignedTo = assignedTo;
      //ticket.creator = creator;
      return ticket.save();
    })
    .then(result => {
      res.status(200).json({ message: 'Ticket updated!', ticket: result });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.deleteTicket = (req, res, next) => {
  const ticketId = req.params.ticketId;
  Ticket.findById(ticketId)
    .then(ticket => {
      if (!ticket) {
        const error = new Error('Could not find ticket.');
        error.statusCode = 404;
        throw error;
      }
      if (ticket.creator.toString() !== req.userId) {
        const error = new Error('Not the author!');
        error.statusCode = 403;
        throw error;
      }
      return Ticket.findByIdAndRemove(ticketId);
    })
    .then(result => {
      res.status(200).json({ message: 'Deleted ticket.' });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
