import React, { Component } from 'react';

import Image from '../../../components/Image/Image';
import './SinglePost.css';

class SingleTicket extends Component {
  state = {
    title: '',
    description: '',
    status: '',
    priority: '',
    creator: '',
    assignedTo: ''
  };

  componentDidMount() {
    const ticketId = this.props.match.params.ticketId;
    fetch('http://localhost:8080/ticket/' + ticketId, {
      headers: {
        Authorization: 'Bearer ' + this.props.token
      }
    })
      .then(res => {
        if (res.status !== 200) {
          throw new Error('Failed to fetch ticket');
        }
        return res.json();
      })
      .then(resData => {
        this.setState({
          title: resData.ticket.title,
          description: resData.ticket.description,
          status: resData.ticket.status,
          priority: resData.ticket.priority,
          creator: resData.ticket.creator,
          assignedTo: resData.ticket.assignedTo
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    return (
      <section className="single-ticket">
        <h1>{this.state.title}</h1>
        <h2>
          Created by {this.state.creator} Assigned to: {this.state.assignedTo}
        </h2>
        <div className="single-ticket">
          Description: {this.state.description}
        </div>
        <div className="single-ticket">
          Status: {this.state.status}
        </div>
        <div className="single-ticket">
          Priority: {this.state.priority}
        </div>
        <p>{this.state.content}</p>
      </section>
    );
  }
}

export default SingleTicket;
