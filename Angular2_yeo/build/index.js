System.register("index", ["angular2/core", "angular2/platform/browser", "angular-2-yeo"], function($__export) {
  "use strict";
  var Component,
      View,
      bootstrap,
      Angular2Yeo,
      Main;
  return {
    setters: [function($__m) {
      Component = $__m.Component;
      View = $__m.View;
    }, function($__m) {
      bootstrap = $__m.bootstrap;
    }, function($__m) {
      Angular2Yeo = $__m.Angular2Yeo;
    }],
    execute: function() {
      Main = function() {
        function Main() {}
        return ($traceurRuntime.createClass)(Main, {}, {});
      }();
      Object.defineProperty(Main, "annotations", {get: function() {
          return [new Component({selector: 'main'}), new View({
            directives: [Angular2Yeo],
            template: "\n    <angular-2-yeo></angular-2-yeo>\n  "
          })];
        }});
      bootstrap(Main);
    }
  };
});
