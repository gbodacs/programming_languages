System.register("angular-2-yeo", ["angular2/core"], function($__export) {
  "use strict";
  var Component,
      View,
      Angular2Yeo;
  return {
    setters: [function($__m) {
      Component = $__m.Component;
      View = $__m.View;
    }],
    execute: function() {
      Angular2Yeo = function() {
        function Angular2Yeo() {
          console.info('Angular2Yeo Component Mounted Successfully');
          this.cur = 0;
          this.saved = 0;
          this.operation = '';
          this.color2 = '112233';
        }
        return ($traceurRuntime.createClass)(Angular2Yeo, {
          clicked0: function(event) {
            this.cur = this.cur * 10 + 0;
          },
          clicked1: function(event) {
            this.cur = this.cur * 10 + 1;
          },
          clicked2: function(event) {
            this.cur = this.cur * 10 + 2;
          },
          clicked3: function(event) {
            this.cur = this.cur * 10 + 3;
          },
          clicked4: function(event) {
            this.cur = this.cur * 10 + 4;
          },
          clicked5: function(event) {
            this.cur = this.cur * 10 + 5;
          },
          clicked6: function(event) {
            this.cur = this.cur * 10 + 6;
          },
          clicked7: function(event) {
            this.cur = this.cur * 10 + 7;
          },
          clicked8: function(event) {
            this.cur = this.cur * 10 + 8;
          },
          clicked9: function(event) {
            this.cur = this.cur * 10 + 9;
          },
          clickedPlus: function(event) {
            this.saved = this.cur;
            this.cur = 0;
            this.operation = '+';
            this.color2 = '33ee55';
          },
          clickedMinus: function(event) {
            this.saved = this.cur;
            this.cur = 0;
            this.operation = '-';
            this.color2 = '9955cc';
          },
          clickedMultiple: function(event) {
            this.saved = this.cur;
            this.cur = 0;
            this.operation = '*';
            this.color2 = 'ccee55';
          },
          clickedPer: function(event) {
            this.saved = this.cur;
            this.cur = 0;
            this.operation = '/';
            this.color2 = '33eeff';
          },
          clickedEqual: function(event) {
            if (this.operation == '+') {
              this.cur = this.saved + this.cur;
              this.operation = '';
              return;
            }
            if (this.operation == '*') {
              this.cur = this.saved * this.cur;
              this.operation = '';
              return;
            }
            if (this.operation == '/') {
              this.cur = this.saved / this.cur;
              this.operation = '';
              return;
            }
            if (this.operation == '-') {
              this.cur = this.saved - this.cur;
              this.operation = '';
              return;
            }
            if (this.operation == '') {
              return;
            }
          }
        }, {});
      }();
      $__export("Angular2Yeo", Angular2Yeo);
      Object.defineProperty(Angular2Yeo, "annotations", {get: function() {
          return [new Component({selector: 'angular-2-yeo'}), new View({templateUrl: 'angular-2-yeo.html'})];
        }});
    }
  };
});
