
export class Memory {

  clickedMRecall() {
    return this.memory;
  }

  clickedMPlus(NumberString) {
    this.temp = parseFloat(this.memory) + parseFloat(NumberString);
    this.memory = this.temp.toString();

    if (this.memory == '0')
      return false;
    else
      return true;
  }

  clickedMClear() {
    this.memory = '0';
  }

  constructor() {
    this.memory = '0';
  }
}
