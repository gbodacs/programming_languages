import {Component, View} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
import {Angular2Yeo} from 'angular-2-yeo';

@Component({
  selector: 'main'
})

@View({
  directives: [Angular2Yeo],
  template: `
    <angular-2-yeo></angular-2-yeo>
  `
})

class Main {

}

bootstrap(Main);
