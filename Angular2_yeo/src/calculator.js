export class Calculator {
  //----------------------------
  // Helper functions
  //----------------------------
  clearOperation() {
    this.operation = '';
    this.error = false;
  }

  getError() {
    return this.error;
  }

  setError() {
    this.error = true;
  }

  clearError() {
    this.error = false;
  }

  getResult() {
    return this.ret;
  }

  //----------------------------
  // Math functions
  //----------------------------
  CalcPlus() {
    this.operation = '+';
  }

  CalcMinus() {
    this.operation = '-';
  }

  CalcMultiple() {
    this.operation = '*';
  }

  CalcPer() {
    this.operation = '/';
  }

  CalcSquared2(Cur) {
    this.ret = 0;

    try {
      this.ret = parseFloat(Cur) * parseFloat(Cur);
    }
    catch(err) {
      this.ret = 0;
      //TODO: error flag!!!
    }

    this.CheckRet();
  }

  CalcSquareRoot2(Cur) {
    this.ret = 0;

    try {
      this.ret = Math.sqrt( parseFloat(Cur) ) ;
    }
    catch(err) {
      this.ret = 0;
      //TODO: error flag!!!
    }

    this.CheckRet();
  }

  CalcSin(Cur) {
    this.ret = 0;
    debugger;
    try{
      this.ret = Math.sin( parseFloat(Cur) ) ;
    }
    catch(err) {
      this.ret = 0;
      //TODO: error flag!
    }

    this.CheckRet();
  }

  CalcCos(Cur) {
    this.ret = 0;

    try{
      this.ret = Math.cos( parseFloat(Cur) ) ;
    }
    catch(err) {
      this.ret = 0;
      //TODO: error flag!
    }

    this.CheckRet();
  }

  CalcTg(Cur) {
    this.ret = 0;

    try{
      this.ret = Math.tan( parseFloat(Cur) ) ;
    }
    catch(err) {
      this.ret = 0;
      //TODO: error flag!
    }

    this.CheckRet();
  }

  CalcCtg(Cur) {
    this.ret = 0;

    try{
      this.ret = (1/ ( Math.tan( parseFloat(Cur) ) ));
    }
    catch(err) {
      this.ret = 0;
      //TODO: error flag!
    }
    this.CheckRet();
  }

  CalcC() {
    this.clearOperation();
    this.clearError();
  }

  CalcCe() {
    this.clearOperation();
    this.clearError();
  }

  CalcNegative(Cur) {
    this.ret = '';
    if (Cur[0] == '-')
    {
      this.i=1;
      for (this.i=1; this.i<Cur.length; this.i++)
      {
        this.ret += Cur[this.i];
      }
      this.CheckRet();
    }
    else {
      this.ret = '-' + Cur;
      this.CheckRet();
    }
  }

  CheckRet() {
    var Ret2 = this.ret.toString();
    if (Ret2 == "NaN")
    {
      Ret2 = '0';
      this.setError();
    }

    if (Ret2 == "Infinity")
    {
      Ret2 = '0';
      this.setError();
    }
    this.ret = Ret2;
  }

  CalcEqual(Cur, Saved) {
    this.ret = 0;
    if (this.operation == '+') {
      this.ret = parseFloat(Saved) + parseFloat(Cur);
      this.clearOperation();
      this.CheckRet();
      return;
    }

    if (this.operation == '*') {
      this.ret = parseFloat(Saved) * parseFloat(Cur);
      this.clearOperation();
      this.CheckRet();
      return;
    }

    if (this.operation == '/') {
      this.ret = parseFloat(Saved) / parseFloat(Cur);
      this.clearOperation();
      this.CheckRet();
      return;
    }

    if (this.operation == '-') {
      this.ret = parseFloat(Saved) - parseFloat(Cur);
      this.clearOperation();
      this.CheckRet();
      return;
    }

    if (this.operation == '') {
      this.ret = Cur;
      return;
    }
  }

  constructor() {
    //super();
    this.error = false;
    this.ret = '0';
    this.operation = '';
  }
}
