import {Component, View} from 'angular2/core';
import { Calculator } from 'calculator.js';
import { Memory } from 'memory.js';

@Component({
  selector: 'angular-2-yeo'
})

@View({
  templateUrl: 'angular-2-yeo.html'
})

export class Angular2Yeo {

  //----------------------------
  // Helper functions
  //----------------------------
  clearMemory() {
    this.isMemory = ' ';
  }

  setMemory() {
    this.isMemory = 'M';
  }

  clearError() {
    this.isError = ' ';
  }

  setError() {
    this.isError = 'E';
  }

  clearCur() {
    this.cur = '0';
  }

  clearSaved() {
    this.saved = '';
  }

  commandPressed() {
    this.saved = this.cur;
    this.clearCur();
  }

  copyCalcResult() {
    this.cur = this.calc.getResult();
    if (this.calc.getError() == true)
      this.setError();
    else
      this.clearError();
  }

  //----------------------------
  // Memory button press events
  //----------------------------
  clickedMPlus(event) {
    if (this.mem.clickedMPlus(this.cur) == false)
      this.clearMemory();
    else
      this.setMemory();
  }

  clickedMRecall(event) {
    this.cur = this.mem.clickedMRecall();
  }

  clickedMClear(event) {
    this.mem.clickedMClear();
    this.clearMemory();
  }

  //----------------------------
  // Number button press events
  //----------------------------
  clickedPoint(event) {
    var t = this.cur.indexOf('.');
    if (t > -1)
      return;

    this.cur = this.cur + '.';
  }

  clicked0(event) {
    if (this.cur != '0')
      this.cur = this.cur + '0';
  }
  clicked1(event) {
    if (this.cur != '0')
      this.cur = this.cur + '1';
    else
      this.cur = '1';
  }
  clicked2(event) {
    if (this.cur != '0')
      this.cur = this.cur + '2';
    else
      this.cur = '2';
  }
  clicked3(event) {
    if (this.cur != '0')
      this.cur = this.cur + '3';
    else
      this.cur = '3';
  }
  clicked4(event) {
    if (this.cur != '0')
      this.cur = this.cur + '4';
    else
      this.cur = '4';
  }
  clicked5(event) {
    if (this.cur != '0')
      this.cur = this.cur + '5';
    else
      this.cur = '5';
  }
  clicked6(event) {
    if (this.cur != '0')
      this.cur = this.cur + '6';
    else
      this.cur = '6';
  }
  clicked7(event) {
    if (this.cur != '0')
      this.cur = this.cur + '7';
    else
      this.cur = '7';
  }
  clicked8(event) {
    if (this.cur != '0')
      this.cur = this.cur + '8';
    else
      this.cur = '8';
  }
  clicked9(event) {
    if (this.cur != '0')
      this.cur = this.cur + '9';
    else
      this.cur = '9';
  }

  //----------------------------
  // Command button press events
  //----------------------------
  clickedPlus(event) {
    this.commandPressed();
    this.calc.CalcPlus();
  }

  clickedMinus(event) {
    this.commandPressed();
    this.calc.CalcMinus();
  }

  clickedMultiple(event) {
    this.commandPressed();
    this.calc.CalcMultiple();
  }

  clickedPer(event) {
    this.commandPressed();
    this.calc.CalcPer();
  }

  clickedNegative(event) {
    this.calc.CalcNegative(this.cur);
    this.copyCalcResult();
  }

  clickedSquared2(event) {
    this.calc.CalcSquared2(this.cur);
    this.copyCalcResult();
  }

  clickedSquareRoot2(event){
    this.calc.CalcSquareRoot2(this.cur);
    this.copyCalcResult();
  }

  //----------------------------
  // Sin button press events
  //----------------------------
  clickedSin(event) {
    this.calc.CalcSin(this.cur);
    this.copyCalcResult();
  }

  clickedCos(event) {
    this.calc.CalcCos(this.cur);
    this.copyCalcResult();
  }

  clickedTg(event) {
    this.calc.CalcTg(this.cur);
    this.copyCalcResult();
  }

  clickedCtg(event) {
    this.calc.CalcCtg(this.cur);
    this.copyCalcResult();
  }

  //----------------------------
  // Clear button press events
  //----------------------------
  clickedC(event) {
    this.clearCur();
    this.clearSaved();
    this.clearError();
    this.calc.CalcC();
  }

  clickedCe(event) {
    this.clearCur();
    this.clearSaved();
    this.clearError();
    this.calc.CalcCe();
  }

  //----------------------------
  // Equal button press event
  //----------------------------
  clickedEqual(event) {
    this.calc.CalcEqual(this.cur, this.saved);
    this.copyCalcResult();
  }

  //----------------------------
  //----------------------------
  constructor() {
    //super();
    this.cur = '0';
    this.saved = '';
    this.mem = new Memory();
    this.calc = new Calculator();
    this.isMemory = ' ';
    this.isError = ' ';
    console.info('Angular2Yeo Component Mounted Successfully');
  }

}
