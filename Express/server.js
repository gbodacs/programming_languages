var express = require('express');
var app = express();

//Add static folder
app.use(express.static('public'));

// This responds a POST request for the /testpost page.
app.post('/testpost', function (req, res) {
   res.send('Hello World testpost');
})

//GET default website homepage
app.get('/', function (req, res) {
   res.send('Hello World');
})



var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})