// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#pragma once

#ifdef _WIN32
#define GINLINE _inline
#else
#define GINLINE __inline
#endif

#ifdef _WIN32
#define MySocket SOCKET
#else
#define MySocket int
#endif

#ifdef _WIN32
#define MySleep(x) Sleep(x)
#else
#define MySleep(x) usleep(x*1000)
#include <unistd.h>
#endif

#define MAX_NETBUFFER_SIZE 4096   /* Maximum size of the network packet */

#define SAFE_DELETE(a) if( (a) != nullptr ) delete (a); (a) = nullptr;

#define uint16 unsigned short int
#define int16  short int
#define uint32 unsigned int
#define int32 int

#define MyDataHelper cBoss::Get()->GetDataHelper()
#define MyLogLine cBoss::Get()->GetLogger()->LogLine

#define SERVER_IP "192.168.1.200"
#define RMU1_IP "192.168.1.201"
#define MODBUS_PORT 5002
#define CONTROLLINO_PORT 5001
#define CONTROLLINO_PORT_STR "5001"


