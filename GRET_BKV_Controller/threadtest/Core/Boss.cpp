// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#include "Boss.h"

#include "../Logger/Logger.h"
#include "DataHelper.h"
#include "../Network/SocketManager.h"
#include "../Job/JobManager.h"
#include "../ModBus/ModBusServer.h"
#include "../Data/Data_RMU-1.h"
//#include <pthread.h>
#include <thread>
#include <ctime>
#include <chrono>

using namespace std;
using namespace std::chrono;

//------------------------------------------------------------
//------------------------------------------------------------
cBoss* cBoss::gObject = nullptr;  // SINGLETON !!
//------------------------------------------------------------
//------------------------------------------------------------

//------------------------------------------------------------
cBoss::cBoss() noexcept
{
	gObject = this;
	mLogger = nullptr;
	mJobManager = nullptr;
	mDataHelper = nullptr;
	mModBusServer = nullptr;
	mSocketManager = nullptr;

	mCurrTime = mLastTime = GetTimeInMillisecs();
	mQuitFlag = false;
}

//------------------------------------------------------------
cBoss::~cBoss()
{
}


//------------------------------------------------------------
void cBoss::Init()
{
	mLogger			= new cLogger();
	MyLogLine("cBoss", "Logger init done.");

	if (DataStore::TestRMU1_Input() == false)
	{
		SetQuitFlag();
	}

	if (DataStore::TestRMU1_Output() == false)
	{
		SetQuitFlag();
	}

	mJobManager		= new cJobManager();
	mJobManager->Init();

	mDataHelper		= new cDataHelper();

	MySleep(1000);

	//Windows-on a WSAStart es WSACleanup meghivasaval vigyazni kell, mert a modbus meghulyul tole!
	mModBusServer = new cModBusServer(); mModBusServerThread = new std::thread(&cModBusServer::Run, mModBusServer);   // Modbus server thread starts
	MySleep(300);

	mSocketManager	= new cSocketManager(); mSocketManagerThread = new std::thread(&cSocketManager::Run, mSocketManager);   // Network thread starts

	MyLogLine("cBoss", "cBoss Init done.");
}

//------------------------------------------------------------
void cBoss::Shutdown()
{
    mJobManager->Done(); // Turn off heat, etc.
    mSocketManager->SendControllerRequest(RMU1_IP); //Send out commands to turn off heat, etc.

	// Set quitflag to threads
	mSocketManager->SetQuitFlag();
	mModBusServer->SetQuitFlag();

	// Stop threads
	mSocketManagerThread->join();
	SAFE_DELETE(mSocketManager);
	SAFE_DELETE(mSocketManagerThread);

	mModBusServerThread->join();
	SAFE_DELETE(mModBusServer);
	SAFE_DELETE(mModBusServerThread);


	// Delete members
	SAFE_DELETE(mJobManager);
	SAFE_DELETE(mDataHelper);
	//Logger is the last one
	SAFE_DELETE(mLogger);
}

//------------------------------------------------------------
void cBoss::Run()
{
	while (mQuitFlag == false)
	{
		mCurrTime = GetTimeInMillisecs();
		float deltaT = (float)mCurrTime - mLastTime; //Calculate deltaTime
		mLastTime = mCurrTime;

		
		//mSocketManager->Run(deltaT); //NEM KELL HIVNI, masik threaden van!!
		mJobManager->Run(deltaT);
		//mLogger->Run(deltaT);
		MySleep(5);
	}
	MyLogLine("cBoss", "Run() exiting...");
}

//--------------------------------------------------------------
void cBoss::SetQuitFlag()
{
	MyLogLine("cBoss", "Quit signal received");
	mQuitFlag = true;
}

//--------------------------------------------------------------
long long cBoss::GetTimeInMillisecs()
{
	using namespace std::chrono;
	milliseconds ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
	return ms.count();
}

//--------------------------------------------------------------
void cBoss::GetTimeAndDate(std::tm & aTM)
{
	std::time_t now = std::time(NULL);
	aTM = *std::localtime(&now);
}


