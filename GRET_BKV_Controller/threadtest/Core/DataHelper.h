// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#pragma once

#include "../Core/Defines.h"
#include "../Data/Data_RMU-1.h"
#include "../Network/NetworkPacket.h"
#include <vector>
#include <string>

//-----------------------------------------------------------
class cDataHelper
{
public:
	cDataHelper() noexcept;
	~cDataHelper();

    DataStore::eRMU1_Input  Get_Input_IndexByName(std::string& aName);
	const   float Get_Input_ValueByIndex(DataStore::eRMU1_Input aIndex);
	const   char* Get_Input_NameByIndex(DataStore::eRMU1_Input aIndex) { return GetRMU1_Input_Name(aIndex); }
	        void  Set_Input_ValueByIndex(DataStore::eRMU1_Input aIndex, float aValue);

    DataStore::eRMU1_Output Get_Output_IndexByName(std::string& aName);
    const   bool Get_Output_ValueByIndex(DataStore::eRMU1_Output aIndex);
    const   char* Get_Output_NameByIndex(DataStore::eRMU1_Output aIndex) { return GetRMU1_Output_Name(aIndex); }
            void  Set_Output_ValueByIndex(DataStore::eRMU1_Output aIndex, bool aValue);
    

	void UpdateMappingWithNewData(cNetworkPacket* aPacket, const char* aRMU_IP);
	//resetAlert

private:
	//void Explode(const std::string& s, const char& c, std::vector<std::string>& aList);
	void ParseData(cNetworkPacket* aPacket, std::vector<std::string>& aName, std::vector<float>& aValue);
	void UpdateDatabase(std::vector<std::string>& aName, std::vector<float>& aValue, const char* aRMU_IP);
};

//todo: Amikor bejon egy true riasztas, akkor nem szabad felulcsapni, am�g ki nem ment a Mango fel�
// Amikor kimegy egy parancs, azt addig kell pr�b�lni kiadni, am�g nem siker�l - pipa