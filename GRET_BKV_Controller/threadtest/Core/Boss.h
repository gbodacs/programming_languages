// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#pragma once

#include "Defines.h"
#include <thread>
#include <chrono>
#include <ctime>

class cLogger;
class cConstManager;
class cJobManager;
class cSocketManager;
class cDataHelper;
class cModBusServer;


//--------------------------------------------------------------
// A fo tartalmazo osztaly, ezt barhonnan el lehet erni
class cBoss
{
private:
	static cBoss*	gObject;

	long long	mLastTime; // Last frame time
	long long	mCurrTime; // Current frame time

	bool		mQuitFlag; // True if ctrl+c pressed

	cLogger*		 	mLogger = nullptr;
	cJobManager*		mJobManager = nullptr;
	cDataHelper*		mDataHelper = nullptr;

	cModBusServer*		mModBusServer = nullptr;
	std::thread*		mModBusServerThread = nullptr;

	cSocketManager*		mSocketManager = nullptr;
	std::thread*		mSocketManagerThread = nullptr;


public:
	cBoss() noexcept;
	~cBoss();

	static cBoss* Get() { return gObject; }

	//--------------------------------
	void Init();
	void Run();
	void Shutdown();
	void SetQuitFlag();

	//--------------------------------
	void		GetTimeAndDate(std::tm & aTM);
	long long	GetTimeInMillisecs();

	//--------------------------------
	GINLINE cLogger*		 	GetLogger() const noexcept;			//Logger lekerdezese
	GINLINE cSocketManager*	    GetSocketManager() const noexcept;	//Network thread lekerdezese
	GINLINE cJobManager*		GetJobManager() const noexcept;	    //JobManager lekerdezese
	GINLINE cDataHelper*		GetDataHelper() const noexcept;     //Datahelper to reach input data for the jobs
	GINLINE cModBusServer*		GetModBusServer() const noexcept;   //ModBus server to handle requests from Mango
};



//--------------------------------------------------------------
//--------------------------------------------------------------
cLogger* cBoss::GetLogger() const noexcept
{
	return mLogger;
}

//--------------------------------------------------------------
cSocketManager* cBoss::GetSocketManager() const noexcept
{
	return mSocketManager;
}

//--------------------------------------------------------------
cModBusServer* cBoss::GetModBusServer() const noexcept
{
	return mModBusServer;
}

//--------------------------------------------------------------
cJobManager* cBoss::GetJobManager() const noexcept
{
	return mJobManager;
}

//--------------------------------------------------------------
cDataHelper* cBoss::GetDataHelper() const noexcept
{
	return mDataHelper;
}