// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#include <algorithm>
#include "Defines.h"
#include "DataHelper.h"
#include "../Core/Boss.h"
#include "../Logger/Logger.h"
#include "../ModBus/ModBusServer.h"
#include <vector>
#include <string>

using namespace std;
using namespace DataStore;

//-----------------------------------------------------------
cDataHelper::cDataHelper() noexcept
{
}

//-----------------------------------------------------------
cDataHelper::~cDataHelper()
{
}

//-----------------------------------------------------------
const float cDataHelper::Get_Input_ValueByIndex(eRMU1_Input aIndex) 
{
	return cBoss::Get()->GetModBusServer()->GetInput(aIndex);
}

void cDataHelper::Set_Input_ValueByIndex(eRMU1_Input aIndex, float aValue)
{
	switch (aIndex)
	{
		case RMU1_Nyomas:
		{
			cBoss::Get()->GetModBusServer()->SetInputFixed(aIndex, aValue); //Nem kell a tizedesjegy, fixpontos ertek
			break;
		}
		case RMU1_Tkor_nyomas_alacsony:
		{
			cBoss::Get()->GetModBusServer()->SetInputFloat(aIndex, aValue, true); //Delay-es beallitas
			break;
		}
		default:
		{
			cBoss::Get()->GetModBusServer()->SetInputFloat(aIndex, aValue); // Ket tizedesjegyig pontos /100 kell neki a Mangoban
		}
	}
}

//-----------------------------------------------------------
const bool cDataHelper::Get_Output_ValueByIndex(eRMU1_Output aIndex)
{
	float ret = cBoss::Get()->GetModBusServer()->GetOutput(aIndex);

	return ((ret > 0.5f) ? true : false);
}

void cDataHelper::Set_Output_ValueByIndex(eRMU1_Output aIndex, bool aValue)
{
    cBoss::Get()->GetModBusServer()->SetOutput(aIndex, aValue);
}

//-----------------------------------------------------------
eRMU1_Input cDataHelper::Get_Input_IndexByName(std::string& aName)
{
	for (int i = 0; i < eRMU1_Input::RMU1_input_enum_num; ++i)
	{
		if (aName == GetRMU1_Input_Name((eRMU1_Input)i))
		{
			return (eRMU1_Input) i; //Megvan!!
		}
	}

	return (eRMU1_Input) -1; //nincs meg!
}

//-----------------------------------------------------------
eRMU1_Output cDataHelper::Get_Output_IndexByName(std::string& aName)
{
    for (int i = 0; i < eRMU1_Output::RMU1_output_enum_num; ++i)
    {
        if (aName == GetRMU1_Output_Name((eRMU1_Output)i))
        {
            return (eRMU1_Output) i; //Megvan!!
        }
    }

    return (eRMU1_Output) -1;
}

//-----------------------------------------------------------
void cDataHelper::UpdateMappingWithNewData(cNetworkPacket* aPacket, const char* aRMU_IP)
{
	std::vector<std::string> Name;
	std::vector<float> Value;

	ParseData(aPacket, Name, Value);
	UpdateDatabase(Name, Value, aRMU_IP);
}

//-----------------------------------------------------------
void cDataHelper::UpdateDatabase(std::vector<std::string>& aName, std::vector<float>& aValue, const char* aRMU_IP)
{
	int Index = 0;

	for (unsigned int i=0; i<aName.size(); ++i)
	{
		Index = Get_Input_IndexByName(aName[i]);
		if (Index == -1)
		{
			MyLogLine("DataHelper::Update invalid string", aName[i].c_str());
		}
		else
		{
			Set_Input_ValueByIndex((eRMU1_Input)Index, aValue[i]);
		}
	}
}

//-----------------------------------------------------------
void cDataHelper::ParseData(cNetworkPacket* aPacket, std::vector<std::string>& aName, std::vector<float>& aValue)
{
	std::string::size_type sz;   // alias of size_t
	std::string TestString;
	int DataSize;
	TestString = (char*) aPacket->GetBuffer2(DataSize);

	int currpos = 0;
	while (1)
	{
		int IDBegin = TestString.find('@', currpos);
		IDBegin++;
		int IDEnd = TestString.find('=', currpos);
		//IDEnd--;
		int ValueBegin = IDEnd + 1;
		int ValueEnd = TestString.find(';', currpos);
		//ValueEnd--;

		string Name = TestString.substr(IDBegin, IDEnd - IDBegin);  //Get value
		string Value = TestString.substr(ValueBegin, ValueEnd - ValueBegin);

		Name.erase(std::remove(Name.begin(), Name.end(), ' '), Name.end()); // TRIM!!
		Name.erase(std::remove(Name.begin(), Name.end(), '\r'), Name.end());
		Name.erase(std::remove(Name.begin(), Name.end(), '\n'), Name.end());
		Value.erase(std::remove(Value.begin(), Value.end(), ' '), Value.end());
		Value.erase(std::remove(Value.begin(), Value.end(), '\n'), Value.end());
		Value.erase(std::remove(Value.begin(), Value.end(), '\r'), Value.end());

		float iValue = 0;
		if ( !Value.empty() )
		{
			iValue = std::stof(Value, &sz);
		}

		aName.push_back(Name);
		aValue.push_back(iValue);

		if ( ((int)TestString.length()) == (ValueEnd + 1) )
			break; //Kilepes

		TestString = TestString.substr(ValueEnd + 1);
	}
	
}

/*void cDataHelper::Explode(const string& s, const char& c, vector<string>& aList)
{
	string buff{ "" };

	for (auto n : s)
	{
		if (n != c) buff += n; else
			if (n == c && buff != "") { aList.push_back(buff); buff = ""; }
	}
	if (buff != "") aList.push_back(buff);

}*/