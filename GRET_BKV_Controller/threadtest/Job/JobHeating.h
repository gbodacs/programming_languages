// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#pragma once

#include "IJob.h"
#include "../Data/Data_RMU-1.h"

#include <chrono>

enum class HeatingPrgState: int
{
	LPS_WAIT_FOR_START,
	LPS_HEAT,

	LPS_INVALID_STATE,
};


//--------------------------------------------
class cJobHeating : public IJob
{
public:
	cJobHeating(DataStore::eRMU1_Input temperature, DataStore::eRMU1_Output control, int minutes, float tempLow, float tempHigh);
	virtual ~cJobHeating() {};

	virtual int  Init() override;  // One-time init before every frame to set the time
	virtual int  Run(float dtime) override;  // The function called in the loop
    virtual void Done() override;  // The function called in the loop

private:
	DataStore::eRMU1_Input  mTemperature;
	DataStore::eRMU1_Output mControl;
    int                     mHeatTimeMinutes;
    int                     mSavedDay = -1;

    std::tm                 mStartTime;
    std::tm                 mEndTime;

	float					mTempLow;
	float					mTempHigh;

	HeatingPrgState			mState = HeatingPrgState::LPS_WAIT_FOR_START;

	void SetLowTemp(float aLowTemp);
	void SetHighTemp(float aHighTemp);
	void SetLowAndHighTemp(float aLow, float aHigh);
};

