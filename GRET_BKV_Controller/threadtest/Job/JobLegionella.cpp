// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#include "JobLegionella.h"
#include "../Data/Data_RMU-1.h"
#include "../Core/DataHelper.h"
#include "../Core/Boss.h"
#include "../Core/Defines.h"
#include "../Logger/Logger.h"

using namespace DataStore;

//---------------------------------------------
cJobLegionella::cJobLegionella(eRMU1_Input temperature, eRMU1_Output control, eRMU1_Output turnoff, int minutes, float tempLow, float tempHigh)
{
    mTemperature = temperature;
    mControl = control;
    mHeatTimeMinutes = minutes;
    mTempLow = tempLow;
    mTempHigh = tempHigh;
	mTurnOff = turnoff;

	cBoss::Get()->GetTimeAndDate(mForceEndTime);
	cBoss::Get()->GetTimeAndDate(mEndTime);
	cBoss::Get()->GetTimeAndDate(mStartTime);
};

//---------------------------------------------
int cJobLegionella::Init()
{
	return 0;
}

//---------------------------------------------
void cJobLegionella::Done()
{
    MyDataHelper->Set_Output_ValueByIndex(mControl, false); //Heating off
    mState = LegionellaPrgState::LPS_WAIT_FOR_START;
}

//---------------------------------------------
bool cJobLegionella::CheckForceEndTime()
{
	std::tm CurrTime;
	cBoss::Get()->GetTimeAndDate(CurrTime);

	if (mktime(&mForceEndTime) < mktime(&CurrTime)) // Time is out!
	{
		MyDataHelper->Set_Output_ValueByIndex(mControl, false); //Heating off
		mState = LegionellaPrgState::LPS_WAIT_FOR_START;    //End of program, start again in next day
		MyLogLine("LegionellaJob", "ERROR: CheckForceEndTime\n");
		return true;
	}

	return false;
}

//---------------------------------------------
void cJobLegionella::CheckEndTime()
{
	std::tm CurrTime;
	cBoss::Get()->GetTimeAndDate(CurrTime);

	if (mktime(&mEndTime) < mktime(&CurrTime)) // Time is out!
	{
		MyDataHelper->Set_Output_ValueByIndex(mControl, false); //Heating off
		mState = LegionellaPrgState::LPS_WAIT_FOR_START;    //End of program, start again in next day
		MyLogLine("LegionellaJob", "INFO: CheckEndTime\n");
	}
}
//---------------------------------------------
int cJobLegionella::Run(float dtime)
{
    //Parameterek - min homerseklet | max homerseklet | idotartam, amig a legionella kinyiffan | max idotartam, amig a teljes program futhat | kikapcsolva
    //1. program start 0:02 AM
    //2. futeni min hofokra
    //3. stopper indul, tartani min es max kozott a homersekletet
    //4. amikor eleri az X idot a stopper, program allj egy napra

	// Veszuzemmod: PROGRAM_RUNNING_MAX_HOURS

	switch (mState)
	{
		case LegionellaPrgState::LPS_WAIT_FOR_START:
		{
            std::tm CurrTime;
            cBoss::Get()->GetTimeAndDate(CurrTime);
            if ((CurrTime.tm_hour == PROGRAM_START_HOUR) && 
				(CurrTime.tm_min == PROGRAM_START_MINUTE) && 
				(CurrTime.tm_year > 2018) &&
				(CurrTime.tm_wday != mSavedDay) &&						//Time and day check!
				(!MyDataHelper->Get_Output_ValueByIndex(mTurnOff)) )	//Check the feature is turned on or not.
            {
                mSavedDay = CurrTime.tm_wday;							//Save the day for safety!
                mState = LegionellaPrgState::LPS_HEAT_FOR_MIN_TEMP;
				mForceEndTime = CurrTime;
				mForceEndTime.tm_hour += PROGRAM_RUNNING_MAX_HOURS;
				std::mktime(&mForceEndTime); //Normalised time and date

                break;
            }
		}
		break;

		case LegionellaPrgState::LPS_HEAT_FOR_MIN_TEMP:
		{
			if (CheckForceEndTime()) //Ha lejart az ido, nem megyunk tovabb
				break;

            float CurrTemp = MyDataHelper->Get_Input_ValueByIndex(mTemperature);

            if (CurrTemp > mTempLow)
            {
                MyDataHelper->Set_Output_ValueByIndex(mControl, false); //Heating off

                cBoss::Get()->GetTimeAndDate(mStartTime);

                mEndTime = mStartTime;
                mEndTime.tm_min += mHeatTimeMinutes; //Heating time set
                std::mktime(&mEndTime); //Normalised time and date

                mState = LegionellaPrgState::LPS_KEEP_BETWEEN_MIN_AND_MAX;
                break;
            }

            if (MyDataHelper->Get_Output_ValueByIndex(mControl) == false)
            {
                MyDataHelper->Set_Output_ValueByIndex(mControl, true); //If heating was turned off, turn it on now!
            }
		}
		break;

		case LegionellaPrgState::LPS_KEEP_BETWEEN_MIN_AND_MAX:
		{
			if (CheckForceEndTime()) //Ha lejart az ido, nem megyunk tovabb
				break;

            float CurrTemp = MyDataHelper->Get_Input_ValueByIndex(mTemperature);

            if ((CurrTemp < mTempLow) && (MyDataHelper->Get_Output_ValueByIndex(mControl) == false))
            {
                MyDataHelper->Set_Output_ValueByIndex(mControl, true); //Heating on
            }
            else if ((CurrTemp > mTempHigh) && (MyDataHelper->Get_Output_ValueByIndex(mControl) == true))
            {
                MyDataHelper->Set_Output_ValueByIndex(mControl, false); //Heating off
            }

			CheckEndTime();
		}
		break;

		default:
			MyLogLine("LegionellaJob", "Invalid state in state switch:", (int)mState );
			break;
	}

	return 0; //minden OK
//	return 2; //vege a jobnak, lehet torolni
}

//
////---------------------------------------------
//void cJobLegionella::SetLowTemp(float aLowTemp)
//{
//	mTempLow = aLowTemp;
//}
//
////---------------------------------------------
//void cJobLegionella::SetHighTemp(float aHighTemp)
//{
//	mTempHigh = aHighTemp;
//}
//
////---------------------------------------------
//void cJobLegionella::SetLowAndHighTemp(float aLow, float aHigh)
//{
//	float Low = aLow;
//	float High = aHigh;
//
//	if (Low > High)
//	{
//		float temp = Low;
//		Low = High;
//		High = temp;
//	}
//
//	SetLowTemp(Low);
//	SetHighTemp(High);
//}