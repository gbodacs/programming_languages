// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#include "JobManager.h"
#include "JobLegionella.h"
#include "JobHeating.h"

using namespace DataStore;

//---------------------------------------------
cJobManager::cJobManager() noexcept
{
    mJobList.push_back(new cJobLegionella(RMU1_HMV_konyha, RMU1_HMV_antileg_konyha, RMU1_AntiLeg_Tiltas, 32, 61, 64));
    mJobList.push_back(new cJobLegionella(RMU1_HMV_szoc, RMU1_HMV_antileg_szoc, RMU1_AntiLeg_Tiltas, 32, 61, 64));
}

//---------------------------------------------
cJobManager::~cJobManager()
{
    delete *mJobList.begin();
	mJobList.pop_front();

    delete *mJobList.begin();
	mJobList.pop_front();
}

//---------------------------------------------
void cJobManager::Run(float dtime)
{
	if (mJobList.empty())
		return;

	std::list<IJob*>::iterator It = mJobList.begin();

	while (It != mJobList.end())
	{
		IJob * JobItem = *It;

		if (JobItem->Run(dtime) == 0)
		{
			++It;
		}
		else
		{
			It = mJobList.erase(It);
		}
	}
}

//---------------------------------------------
void cJobManager::Done()
{
    if (mJobList.empty())
	{
		return;
	}

	std::list<IJob*>::iterator It = mJobList.begin();

    while (It != mJobList.end())
    {
        IJob * JobItem = *It;
        JobItem->Done();
		++It;
    }
}

//---------------------------------------------
void cJobManager::Init()
{
	if (mJobList.empty())
		return;

	std::list<IJob*>::iterator It = mJobList.begin();

	while (It != mJobList.end())
	{
		IJob * JobItem = *It;
		JobItem->Init();
		++It;
	}
}