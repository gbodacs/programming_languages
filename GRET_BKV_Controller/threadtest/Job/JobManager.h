// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#pragma once

#include "IJob.h"
#include "list"

//--------------------------------------------
class cJobManager
{
public:
    cJobManager() noexcept;
    ~cJobManager();

	void Run(float dtime);
    void Done();
	void Init();

private:
	std::list<IJob*> mJobList;
};

