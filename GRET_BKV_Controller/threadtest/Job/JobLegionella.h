// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#pragma once

#include "IJob.h"
#include "../Data/Data_RMU-1.h"

#include <chrono>
#include <ctime>

//--------------------------------------------
enum class LegionellaPrgState: int
{
	LPS_WAIT_FOR_START,
	LPS_HEAT_FOR_MIN_TEMP,
	LPS_KEEP_BETWEEN_MIN_AND_MAX,

	LPS_INVALID_STATE,
};

//--------------------------------------------
class cJobLegionella : public IJob
{
public:
	cJobLegionella(DataStore::eRMU1_Input temperature, DataStore::eRMU1_Output control, DataStore::eRMU1_Output turnoff, int minutes, float tempLow, float tempHigh);
	virtual ~cJobLegionella() {};

	virtual int  Init() override;  // One-time init before every frame to set the time
	virtual int  Run(float dtime) override;  // The function called in the loop
    virtual void Done() override;  // The function called in the loop

	static constexpr auto PROGRAM_RUNNING_MAX_HOURS = 4; //The program will force shut down after 3 hours
	static constexpr auto PROGRAM_START_HOUR = 0;		 //The program will start at 0:02 AM
	static constexpr auto PROGRAM_START_MINUTE = 2;		 //The program will start at 0:02 AM

private:
	DataStore::eRMU1_Input  mTemperature;
	DataStore::eRMU1_Output mControl;
	DataStore::eRMU1_Output mTurnOff;
    int                     mHeatTimeMinutes;
    int                     mSavedDay = -1;

    std::tm                 mStartTime;
    std::tm                 mEndTime;
	std::tm                 mForceEndTime;

	float					mTempLow;
	float					mTempHigh;

	LegionellaPrgState		mState = LegionellaPrgState::LPS_WAIT_FOR_START;

	void CheckEndTime();			//check the curr time against the end time
	bool CheckForceEndTime();		//check the curr time against the force end time (max 3 hours to run the program)
};

