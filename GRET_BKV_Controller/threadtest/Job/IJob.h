// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#pragma once

#include "memory.h"



//--------------------------------------------
class IJob
{
public:
	IJob() noexcept {};
	virtual ~IJob() {};

	virtual int  Init() = 0;    //One-time init before the first Run()
	virtual int  Run(float dtime) = 0;           //Called in every loop
    virtual void Done() = 0;                     //Emergeny gracefully shutdown

};

