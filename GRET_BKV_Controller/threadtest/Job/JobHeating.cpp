// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#include "JobHeating.h"
#include "../Data/Data_RMU-1.h"
#include "../Core/DataHelper.h"
#include "../Core/Boss.h"
#include "../Core/Defines.h"
#include "../Logger/Logger.h"

using namespace DataStore;

//---------------------------------------------
cJobHeating::cJobHeating(eRMU1_Input temperature, eRMU1_Output control, int minutes, float tempLow, float tempHigh)
{
    mTemperature = temperature;
    mControl = control;
    mHeatTimeMinutes = minutes;
    mTempLow = tempLow;
    mTempHigh = tempHigh;

	cBoss::Get()->GetTimeAndDate(mEndTime);
	cBoss::Get()->GetTimeAndDate(mStartTime);
};

//---------------------------------------------
int cJobHeating::Init()
{
	return 0;
}

//---------------------------------------------
void cJobHeating::Done()
{
    MyDataHelper->Set_Output_ValueByIndex(mControl, false); //Heating off
    mState = HeatingPrgState::LPS_WAIT_FOR_START;
}

//---------------------------------------------
int cJobHeating::Run(float dtime)
{
    //Parameterek - min homerseklet | max homerseklet
    //1. program start ejfel utan 2 perccel
    //2. futeni min hofokra
    //3. stopper indul, tartani min es max kozott a homersekletet
    //4. amikor eleri az X idot a stopper, program allj egy napra

	switch (mState)
	{
		case HeatingPrgState::LPS_WAIT_FOR_START:
		{
            std::tm CurrTime;
            cBoss::Get()->GetTimeAndDate(CurrTime);
            if ((CurrTime.tm_hour == 1) && (CurrTime.tm_min == 2) && (CurrTime.tm_wday != mSavedDay)) //Time check!! Save the day for safety!!
            {
                mSavedDay = CurrTime.tm_wday;
                mState = HeatingPrgState::LPS_HEAT;
                break;
            }
		}
		break;

		case HeatingPrgState::LPS_HEAT:
		{
            float CurrTemp = MyDataHelper->Get_Input_ValueByIndex(mTemperature);

            if (CurrTemp > mTempLow)
            {
                MyDataHelper->Set_Output_ValueByIndex(mControl, false); //Heating off

                cBoss::Get()->GetTimeAndDate(mStartTime);

                mEndTime = mStartTime;
                mEndTime.tm_min += mHeatTimeMinutes; //Heating time set
                std::mktime(&mEndTime);

                mState = HeatingPrgState::LPS_WAIT_FOR_START;
                break;
            }

            if (MyDataHelper->Get_Output_ValueByIndex(mControl) == false)
            {
                MyDataHelper->Set_Output_ValueByIndex(mControl, true); //If heating was turned off, turn it on now!
            }
		}
		break;

		default:
			MyLogLine("HeatingJob", "Invalid state in state switch:", (int)mState );
			break;
	}

	return 0; //minden OK
//	return 2; //vege a jobnak, lehet torolni
}


//---------------------------------------------
void cJobHeating::SetLowTemp(float aLowTemp)
{
	mTempLow = aLowTemp;
}

//---------------------------------------------
void cJobHeating::SetHighTemp(float aHighTemp)
{
	mTempHigh = aHighTemp;
}

//---------------------------------------------
void cJobHeating::SetLowAndHighTemp(float aLow, float aHigh)
{
	float Low = aLow;
	float High = aHigh;

	if (Low > High)
	{
		float temp = Low;
		Low = High;
		High = temp;
	}

	SetLowTemp(Low);
	SetHighTemp(High);
}