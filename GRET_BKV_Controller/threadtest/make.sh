#!/bin/bash
#
#  build webio
#
# ===========================================================================

function compile()
{
    local outfile="${intDir}/$(basename ${1} ${2}).o"

    objs="${objs} ${outfile}"

    echo "Compiling: ${CC} -c ${opt} \"${1}\" -o \"${outfile}\""
    ${CC} -c ${OPTS} "${1}" -o "${outfile}" 2>&1

    return $?
}

# ===========================================================================

function build()
{
    compile main.cpp                    .cpp || exit 1

    compile Core/Boss.cpp               .cpp || exit 1
    compile Core/DataHelper.cpp     .cpp || exit 1
    compile Data/Data_RMU-1.cpp     .cpp || exit 1
    compile Job/JobManager.cpp     .cpp || exit 1
    compile Job/JobLegionella.cpp     .cpp || exit 1
	compile Job/JobHeating.cpp     .cpp || exit 1
    compile Job/IJob.cpp     .cpp || exit 1
    compile Logger/Logger.cpp           .cpp || exit 1
    compile ModBus/ModBusServer.cpp           .cpp || exit 1

    compile Network/NetworkPacket.cpp   .cpp || exit 1
    compile Network/SocketManager.cpp   .cpp || exit 1

    compile NetworkBase/ISocket.cpp     .cpp || exit 1
    compile NetworkBase/LinuxSocket.cpp .cpp || exit 1

    echo

    echo "Linking: ${CC} ${objs} -o ${exec}"
    ${CC} ${objs} ${LIBS} -o ${exec} 2>&1
}

# ===========================================================================
#  main()
#
#   gcc webtest.c webio.c webutils.c webfs.c  webobjs.c webclib.c websys.c wsfdata.c -o webio.exe
#
    intDir=./temp
    exec=gretserver

    if [ "$(uname)" == "Linux" ]; then
        CC="g++"
#        CC="gcc"
        OPTS="-D_LINUX -ICore -ILogger -INetwork -INetworkBase"
       LIBS="-lpthread -lmodbus"
#   else
#       CC="i686-w64-mingw32-c++.exe"
#       OPTS="-static -Wpedantic -Wall -std=gnu++11 -Iwebio"
#       # -v :: verbose mode
#       LIBS=
   fi

    echo "============================================================================="
    echo "Building ${exec}..."
    echo "============================================================================="

    mkdir -p ${intDir}
    build |tee ${intDir}/build.log

