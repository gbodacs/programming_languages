// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#pragma once

#include <stdio.h>
#include <winsock2.h>
#include <queue>
#include <mutex>
#include "../Network/NetworkPacket.h"
#include "../Core/Defines.h"
#include "ISocket.h"

#define BACKLOG 10      /* Passed to listen() */

//----------------------------------------------
class cSocket : public ISocket
{
public:
	cSocket() noexcept;
	virtual ~cSocket();

	//virtual void	ReceivedPacket(cNetworkPacket& aReceived, MySocket& aRecvSocket) override;
	virtual bool	SendAnswer(const cNetworkPacket& aSend, MySocket& aRecvSocket) override;

	virtual bool	InitClientSocket(const char* aDestinantionIP, int aPorti, const char* aPorts, const cNetworkPacket& aSendPacket, cNetworkPacket* aRecvPacket) override;
	//virtual int	InitServerSocket(const char* aPort, const bool aListen) override;

	virtual void	DoneSocket() override;
	virtual int		Loop() override;

private:
	MySocket	_Socket;
	bool		_ServerSocket;

	int		SetNonBlockingSocket(MySocket& aSocket);
	int		AcceptConnection();
	bool	ReceiveFullPacket(MySocket& aSocket, cNetworkPacket* aPacket);

	bool	SendOnMembersocket(const cNetworkPacket& aSend);
};
