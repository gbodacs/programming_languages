
#pragma once

#include <stdio.h>
#include <queue>
#include <mutex>
#include <../Network/NetworkPacket.h>
#include <../Core/Boss.h>
#include <ISocket.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>


#define BACKLOG 10      /* Passed to listen() */

//----------------------------------------------
class cSocket : public ISocket
{
public:
	cSocket();
	virtual ~cSocket();

	//virtual void	ReceivedPacket(cNetworkPacket& aReceived, MySocket& aRecvSocket) override;
	virtual bool	SendAnswer(const cNetworkPacket& aSend, MySocket& aRecvSocket) override;

	virtual bool		InitClientSocket(const char* aDestinantionIP, int aPorti, const char* aPorts, const cNetworkPacket& aSendPacket, cNetworkPacket* aRecvPacket) override;
	//virtual int		InitServerSocket(const char* aPort, const bool aListen) override;

	virtual void	DoneSocket() override;
	virtual int		Loop() override;

private:
	MySocket	_Socket = 0;
	bool		_ServerSocket;

	bool 	SendPacket(MySocket& socket, const void *buffer, size_t length);
	int		SetNonBlockingSocket(MySocket& aSocket);
	//int		AcceptConnection();
	//bool	IncomingConnection(MySocket& aSocket);

	bool	ReceiveFullPacket(MySocket& aSocket, cNetworkPacket* aPacket);

};
