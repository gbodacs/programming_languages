// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#pragma once

#include "../Core/Defines.h"
#include "../Network/NetworkPacket.h"

//----------------------------------------------
class ISocket
{
public:
	ISocket() noexcept {};
	virtual ~ISocket() {};

	//virtual int	InitServerSocket(const char* aPort, const bool aListen) = 0;
	virtual bool	InitClientSocket(const char* aDestinantionIP, int aPorti, const char* aPorts, const cNetworkPacket& aSendPacket, cNetworkPacket* aRecvPacket) = 0;

	virtual void	DoneSocket() = 0;
	virtual int		Loop() = 0;

	//virtual void	ReceivedPacket(cNetworkPacket& aReceived, MySocket& aRecvSocket) = 0;
	virtual bool	SendAnswer(const cNetworkPacket& aSend, MySocket& aRecvSocket) = 0;

	
};
