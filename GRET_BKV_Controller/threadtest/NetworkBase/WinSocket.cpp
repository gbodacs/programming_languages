// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#include <iostream>
#include <thread>

#include <ws2tcpip.h>
#include "ws2def.h"
#include "WinSocket.h"
#include "../Core/Boss.h"
#include "../Core/DataHelper.h"
#include "../Logger/Logger.h"

//---------------------------------------------
// Ctor
cSocket::cSocket() noexcept
	: _ServerSocket(false)
	, _Socket(INVALID_SOCKET)
{
}

cSocket::~cSocket()
{
	DoneSocket();
}
////---------------------------------------------
//void cSocket::ReceivedPacket(cNetworkPacket & aReceived, MySocket & aRecvSocket)
//{
//	int Size = 0;
//
//	const char *Buffer = aReceived.GetBuffer2(Size) ;
//
//	if ((Buffer[0] == 'G') && (Buffer[1] == 'E') && (Buffer[2] == 'T'))
//	{
//		char* SendString = "HTTP/1.1 200 OK\r\n" \
//			"Date : Sat, 21 Apr 2018 13:11:22 GMT\r\n" \
//			"Server : WebStar_2.11\r\n" \
//			/*			"Content - Length : 10899\r\n" \ */
//			"Keep-Alive : timeout = 5, max = 100\r\n"\
//			"Connection : Keep-Alive\r\n" \
//			"Content-Type: text/html; charset: UTF-8\r\n" \
//			"\r\n" \
//			"<!DOCTYPE html>\r\n" \
//			"<html lang = \"hu\">\r\n" \
//			"<head>\r\n" \
//			"<title>Teszt weboldal</title>\r\n" \
//			"</head>\r\n" \
//			"<body>\r\n" \
//			"Teszt sz�veg\r\n" \
//			"<h3>Elstartolt a Diana Fil�horgol�s Special magazin</h3>\r\n" \
//			"<h4>Tesztsz�veg34</h4>\r\n" \
//			"</body>\r\n" \
//			"</html>\r\n";
//
//		cNetworkPacket Packet;
//		Packet.SetBuffer(SendString, (const int) strlen(SendString));
//		if (!SendAnswer(Packet, aRecvSocket))
//		{
//			cBoss::Get()->GetLogger()->LogLine("cSocket::reqreceived", "Could not send packet!", (int) aRecvSocket);
//		}
//
//		closesocket(aRecvSocket);
//	}
//}

//---------------------------------------------
bool cSocket::SendOnMembersocket(const cNetworkPacket& aSend)
{
	return SendAnswer(aSend, _Socket);
}

//---------------------------------------------
bool cSocket::SendAnswer(const cNetworkPacket& aSend, MySocket& aSocket)
{
	int DataSize;
	const char* Buffer = aSend.GetBuffer2(DataSize);

	int Result = send(aSocket, Buffer, DataSize, 0);
	if (Result == 0) 
	{
		cBoss::Get()->GetLogger()->LogLine("cSocket::SendAnswer", "Could not send packet!", (int)aSocket);
		return false;
	}

	return true;
}

//---------------------------------------------
int cSocket::SetNonBlockingSocket(MySocket& aSocket)
{
	//-------------------------
	// Set the socket I/O mode: In this case FIONBIO
	// enables or disables the blocking mode for the 
	// socket based on the numerical value of iMode.
	// If iMode = 0, blocking is enabled; 
	// If iMode != 0, non-blocking mode is enabled.
	u_long iMode = 1;
	return (int) ioctlsocket(aSocket, FIONBIO, &iMode);
}

//---------------------------------------------
bool cSocket::InitClientSocket(const char* aDestinantionIP, int aPorti, const char* aPorts, const cNetworkPacket& aSendPacket, cNetworkPacket* aRecvPacket)
{
	WORD wVersion = MAKEWORD(2, 2);
	WSADATA wsaData;
	int iResult;
	struct addrinfo hints, *res;
	int reuseaddr = 1; /* True */

	/* Initialise Winsock */
	if ((iResult = WSAStartup(wVersion, &wsaData)) != 0)
	{
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "WSAStartup failed:", iResult);
		return 1;
	}

	/* Get the address info */
	ZeroMemory(&hints, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	if (getaddrinfo(NULL, aPorts, &hints, &res) != 0)
	{
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "InitServerSocket - getaddrinfo failed");
		return true;
	}

	/* Create the socket */
	_Socket = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (_Socket == INVALID_SOCKET)
	{
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "Create socket failed.");
		WSACleanup();
		return true;
	}

	/* Enable the socket to non-blocking */
	/*int Res = SetNonBlockingSocket(_Socket);
	if (Res != NO_ERROR)
	{
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "ioctlsocket failed.", Res);
		WSACleanup();
		return 1;
	}*/

	/* Enable the socket to reuse the address */
	if (setsockopt(_Socket, SOL_SOCKET, SO_REUSEADDR, (const char *)&reuseaddr, sizeof(int)) == SOCKET_ERROR)
	{
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "setsockopt failed");
		WSACleanup();
		return true;
	}

	///////////////////////////////////////////
	struct timeval tout, tsnd, trcv;
	socklen_t ntrcv, ntsnd;
	ntrcv = sizeof(timeval);
	ntsnd = sizeof(timeval);

	tout.tv_sec = 3;
	tout.tv_usec = 0;

	if (getsockopt(_Socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&trcv, &ntrcv) < 0)
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "setsockopt failed-2");
	else if (getsockopt(_Socket, SOL_SOCKET, SO_SNDTIMEO, (char *)&tsnd, &ntsnd) < 0)
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "setsockopt failed-3");
	else if (setsockopt(_Socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tout, sizeof(tout)) < 0)
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "setsockopt failed-4");
	else if (setsockopt(_Socket, SOL_SOCKET, SO_SNDTIMEO, (char *)&tout, sizeof(tout)) < 0)
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "setsockopt failed-5");
	else {
	
		if (setsockopt(_Socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&trcv, ntrcv) < 0)
			cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "setsockopt failed-6");
		else 
			if (setsockopt(_Socket, SOL_SOCKET, SO_SNDTIMEO, (char *)&tsnd, ntsnd) < 0)
				cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "setsockopt failed-7");
	}
	///////////////////////////////////////////
	
	struct sockaddr_in conn_addr;
	memset(&conn_addr, '0', sizeof(conn_addr));

	conn_addr.sin_family = AF_INET;
	conn_addr.sin_port = htons(aPorti);

	// Convert IPv4 and IPv6 addresses from text to binary form
	if (inet_pton(AF_INET, aDestinantionIP, &conn_addr.sin_addr) <= 0)
	{
		MyLogLine("cSocket::InitClientSocket", "Invalid address/Address not supported");
		return true;
	}

	//SetNonBlockingSocket(_Socket); // Ez nagyon nem jo ide!!!!!
	//TODO: connect timeout problemat megoldani!!!
	if (connect(_Socket, (struct sockaddr *)&conn_addr, sizeof(conn_addr)) < 0)
	{
		DoneSocket();
		MyLogLine("cSocket::InitClientSocket", "connect failed");
		return true;
	}

	printf("-Connect success!!\n");
	
	_ServerSocket = false;

	if (!SendAnswer(aSendPacket, _Socket))
	{
		DoneSocket();
		cBoss::Get()->GetLogger()->LogLine("cSocket::SendAnswer", "Could not send packet!", (int)_Socket);
		return true;
	}

	printf("-Req message sent\n");

	SetNonBlockingSocket(_Socket); // Ez at lett rakva ide!!!!!

	//aRecvPacket.SetBuffer
	aRecvPacket->FillNull();
	if ( ReceiveFullPacket(_Socket, aRecvPacket) )
	{
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "ReceiveFullPacket failed!", (int)_Socket);
		return true;
	}

	DoneSocket();

	return false;
}

//---------------------------------------------
//int cSocket::InitServerSocket(const char* aPort, const bool aListen)
//{
//	WORD wVersion = MAKEWORD(2, 2);
//	WSADATA wsaData;
//	int iResult;
//	struct addrinfo hints, *res;
//	int reuseaddr = 1; /* True */
//
//	/* Initialise Winsock */
//	if ((iResult = WSAStartup(wVersion, &wsaData)) != 0) 
//	{
//		cBoss::Get()->GetLogger()->LogLine("cSocket", "WSAStartup failed:", iResult);
//		return 1;
//	}
//
//	/* Get the address info */
//	ZeroMemory(&hints, sizeof hints);
//	hints.ai_family = AF_INET;
//	hints.ai_socktype = SOCK_STREAM;
//	if (getaddrinfo(NULL, aPort, &hints, &res) != 0) 
//	{
//		cBoss::Get()->GetLogger()->LogLine("cSocket", "InitServerSocket - getaddrinfo failed");
//		return 1;
//	}
//
//
//	/* Create the socket */
//	_Socket = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
//	if (_Socket == INVALID_SOCKET) 
//	{
//		cBoss::Get()->GetLogger()->LogLine("cSocket", "create socket failed.");
//		WSACleanup();
//		return 1;
//	}
//
//	/* Enable the socket to non-blocking */
//	int Res = SetNonBlockingSocket(_Socket);
//	if (Res != NO_ERROR)
//	{
//		cBoss::Get()->GetLogger()->LogLine("cSocket", "ioctlsocket failed.", Res);
//		WSACleanup();
//		return 1;
//	}
//
//	/* Enable the socket to reuse the address */
//	if (setsockopt(_Socket, SOL_SOCKET, SO_REUSEADDR, (const char *)&reuseaddr, sizeof(int)) == SOCKET_ERROR) 
//	{
//		cBoss::Get()->GetLogger()->LogLine("cSocket", "setsockopt failed");
//		WSACleanup();
//		return 1;
//	}
//
//	/* Bind to the address */
//	if (bind(_Socket, res->ai_addr, (int)res->ai_addrlen) == SOCKET_ERROR) 
//	{
//		cBoss::Get()->GetLogger()->LogLine("cSocket", "bind failed");
//		WSACleanup();
//		return 1;
//	}
//
//	if (aListen) /* Listen */
//	{
//		_ServerSocket = true;
//		if (listen(_Socket, BACKLOG) == SOCKET_ERROR)
//		{
//			cBoss::Get()->GetLogger()->LogLine("cSocket", "listen failed");
//			WSACleanup();
//			return 1;
//		}
//	}
//
//	freeaddrinfo(res);
//
//	return 0;
//}

//---------------------------------------------
int cSocket::AcceptConnection()
{
	size_t size = sizeof(struct sockaddr);
	struct sockaddr_in their_addr;
	MySocket newsock;

	ZeroMemory(&their_addr, sizeof(struct sockaddr));
	newsock = accept(_Socket, (struct sockaddr*)&their_addr, (int*)&size);
	if (newsock == INVALID_SOCKET)
	{
		return 200;
		// No incoming connection
	}
	else 
	{
		char String2[64];
		//inet_ntoa(their_addr.sin_addr), ntohs(their_addr.sin_port));

		printf("Got a connection from %s on port %d\n", inet_ntop(AF_INET, &their_addr.sin_addr, String2, 62), ntohs(their_addr.sin_port));

		cNetworkPacket RecvPacket;
		ReceiveFullPacket(newsock, &RecvPacket);
//		ReceivedPacket(RecvPacket, newsock);
	}
	return 0;
}

//---------------------------------------------
bool cSocket::ReceiveFullPacket(MySocket& aSocket, cNetworkPacket* aRecvPacket)
{
	int Result = 0;
	int Pos = 0;
	int BufferSize = aRecvPacket->GetBufferMemorySize();
	int DataSize;
	int exit = 0;
	char* Buffer = aRecvPacket->GetBuffer2_(DataSize);

	// itt jott be a kapcsolat
	do {
	start:
		Result = recv(aSocket, Buffer+Pos, BufferSize-Pos-2, 0);

		if (Result == -1)
		{
			int Code = WSAGetLastError();
			if (Code == 10035)
			{
				if (Pos != 0)
				{
					printf("\r-Message received(ok): %d\r\n", Pos);
					return false;
				}
					
				//Nem mehet tovabb, mert a -1 elronthja a buffert
				//Sleep kell, mert valamiert meg nem jott meg a valasz
				MySleep(20);
				exit++;

				if (exit > 200)
				{
					printf("\r-Message received(problem): %d\r\n", Pos);
					return true;
				}

				goto start;
			} 
			else
			{
				cBoss::Get()->GetLogger()->LogLine("cSocket", "ReceiveFullPacket:receive failed", Code);
			}
		}
		exit=0;

		if ((Result + Pos) > BufferSize)
		{ //Ha kilog a bejovo csomag, akkor torlunk mindent
			memset(Buffer, 0, DataSize);
			aRecvPacket->SetBuffer("", 0);
			cBoss::Get()->GetLogger()->LogLine("cSocket", "ReceiveFullPacket:too big packet!");
			return true;
		}

		if (Result > 0)
		{
			printf("|");
		}
		else
		{
			//cBoss::Get()->GetLogger()->LogLine("cSocket", "ReceiveFullPacket:receive failed2", WSAGetLastError());
		}

		Pos += Result;
	} while (Result > 0);

	printf("-Bytes received: %d\n", Pos);

	return false;
}

//---------------------------------------------
void cSocket::DoneSocket()
{
	closesocket(_Socket);
	//WSACleanup();

	_Socket = INVALID_SOCKET;
}

//---------------------------------------------
int cSocket::Loop()
{
	int ret = 0;
	if (_ServerSocket)
	{
		ret = AcceptConnection();
		//cBoss::Get()->GetLogger()->LogLine("cSocket", "accept connection", ret);
	}
	return 0;
}
