#include <stdio.h>
#include <string.h> /* memset() */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <fcntl.h>
#include <bits/errno.h>

#include <../Core/Defines.h>
#include "../Core/Boss.h"
#include "../Core/DataHelper.h"
#include "../Logger/Logger.h"

#include <LinuxSocket.h>

#define PORT    "8081" /* Port to listen on */
#define BACKLOG 10  /* Passed to listen() */

//---------------------------------------------
// Ctor
cSocket::cSocket()
	: _Socket(0)
	, _ServerSocket(false)

{
}

//---------------------------------------------
// Ctor
cSocket::~cSocket()
{
	if (_Socket != 0)
	{
		DoneSocket();
	}
}

//---------------------------------------------
bool cSocket::InitClientSocket(const char* aDestinantionIP, int aPorti, const char* aPorts, const cNetworkPacket& aSendPacket, cNetworkPacket* aRecvPacket)
{
    struct sockaddr_in server;
    fd_set fdset;
    struct timeval tv;

	if (_Socket != 0)
	{
		DoneSocket();
	}

    //Create socket
    _Socket = socket(AF_INET , SOCK_STREAM , 0);
	if (_Socket == -1)
	{
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "create socket failed");
		return true;
	}

	server.sin_addr.s_addr = inet_addr(aDestinantionIP);
	server.sin_family = AF_INET;
	server.sin_port = htons( aPorti );


    if (fcntl(_Socket, F_SETFL, fcntl(_Socket, F_GETFL) | O_NONBLOCK) < 0)
    {
        cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "set non-blocking failed");
		DoneSocket();
		return true;
	}

    connect(_Socket , (struct sockaddr *)&server , sizeof(server));

    FD_ZERO(&fdset);
    FD_SET(_Socket, &fdset);
    tv.tv_sec = 3;             /* 3 second timeout */
    tv.tv_usec = 0;

	int retVal = select(_Socket + 1, NULL, &fdset, NULL, &tv);

    if ( retVal == 1)
    {
        int so_error;
        socklen_t len = sizeof(so_error);

        int retV2 = getsockopt(_Socket, SOL_SOCKET, SO_ERROR, &so_error, &len);
		if (retV2)
		{
			cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket getsockopt", "failed");
			DoneSocket();
			return true;
		}
		
        if (so_error == 0)
        {
            //printf("%s:%d is open\n", aDestinantionIP, aPorti);
        }
    }
	else if (retVal == 0)
	{
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket select", "timeout");
		DoneSocket();
		return true;
	}
	else if (retVal == -1)
	{
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket select", "error");
		DoneSocket();
		return true;
	}

	/*
	int reuseaddr = 1; //True
	if (setsockopt(_Socket, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(int)) == -1)
	{
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "reuseaddr failed");
		return true;
	}*/

	if (SendAnswer(aSendPacket, _Socket) == true)
	{
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "send failed");
		DoneSocket();
		return true;
	}

	if (fcntl(_Socket, F_SETFL, fcntl(_Socket, F_GETFL) | O_NONBLOCK) < 0)
    {
        cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "set non-blocking failed2");
		DoneSocket();
		return true;
	}

	//Receive a reply from the server
	if ( ReceiveFullPacket(_Socket, aRecvPacket) == true)
	{
		cBoss::Get()->GetLogger()->LogLine("cSocket::InitClientSocket", "receive failed");
		DoneSocket();
		return true;
	}

	DoneSocket();
	return false;
}

//---------------------------------------------
bool cSocket::SendAnswer(const cNetworkPacket& aSend, MySocket& aRecvSocket)
{
	//Send some data
	int DataSize;
	const char* Buffer = aSend.GetBuffer2(DataSize);
	if( SendPacket(aRecvSocket , Buffer , DataSize) == false)
	{
		cBoss::Get()->GetLogger()->LogLine("cSocket::SendAnswer", "send failed");
		return true;
	}

	return false;
}

//---------------------------------------------
bool cSocket::ReceiveFullPacket(MySocket& aSocket, cNetworkPacket* aPacket)
{
	int CurrRecvSize = 0;
	int Pos = 0;
	int DataSize;
	int BufferSize = aPacket->GetBufferMemorySize();
	char* Buffer = aPacket->GetBuffer2_(DataSize);
	int exit = 0;

	// itt jott be a kapcsolat
	do {
	start:
		CurrRecvSize = recv(aSocket, Buffer+Pos, BufferSize-Pos-2, 0);
		if (CurrRecvSize == -1)
		{
			int Code = errno;
			if ((Code == EWOULDBLOCK) || (Code == EAGAIN))
			{
				if (Pos != 0)
				{
					//printf("\r-Message received(ok): %d\r\n", Pos);
					return false;
				}

				//Nem mehet tovabb, mert a -1 elronthja a buffert
				//Sleep kell, mert valamiert meg nem jott meg a valasz
				MySleep(20);
				exit++;

				if (exit > 200)
				{
					MyLogLine("ERROR: Message received (issue):", Pos, Code);
					return true;
				}

				goto start;
			}
			else
			{
				cBoss::Get()->GetLogger()->LogLine("cSocket", "ReceiveFullPacket:receive failed", Code);
			}
		}
		exit = 0;

		if ((CurrRecvSize + Pos) > BufferSize)
		{ //Ha kilog a bejovo csomag, akkor torlunk mindent
			memset(Buffer, 0, DataSize);
			aPacket->SetBuffer("", 0);
			cBoss::Get()->GetLogger()->LogLine("cSocket", "ReceiveFullPacket:too big packet!");
			return true;
		}

		if (CurrRecvSize > -1)
		{
			//printf("|"); //A packet received
		}
		else
		{
			MyLogLine("cSocket", "ReceiveFullPacket:receive failed2", errno);
		}

		Pos += CurrRecvSize;

	} while (CurrRecvSize > 0);

	printf("-Bytes received: %d\n", Pos);

	return false;
}



//-------------------------------------------------------------
void cSocket::DoneSocket()
{
	shutdown(_Socket, SHUT_RDWR);
	close(_Socket);
	_Socket = 0;
}

//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
bool cSocket::SendPacket(MySocket& socket, const void *buffer, size_t length)
{
	char *ptr = (char*) buffer;
	while (length > 0)
	{
		int i = send(socket, (const void*) ptr, length, 0);
		if (i < 1)
			return false;
		ptr += i;
		length -= i;
	}
	return true;
}

//---------------------------------------------
int cSocket::Loop()
{
	if (_ServerSocket)
	{
		//AcceptConnection();
	}

	return 0;
}

//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------

//-------------------------------------------------------------
//int cSocket::AcceptConnection()
//{
//	socklen_t size = sizeof(struct sockaddr_in);
//	struct sockaddr_in their_addr;
//	int newsock = accept(_Socket, (struct sockaddr*)&their_addr, &size);
//
//	if (newsock == -1)
//	{
//		perror("accept");
//	}
//	else
//	{
//		printf("Got a connection from %s on port %d\n", inet_ntoa(their_addr.sin_addr), htons(their_addr.sin_port));

//		if (fcntl(newsock, F_SETFL, fcntl(newsock, F_GETFL) | O_NONBLOCK) < 0)
//		{
			//error! cannot set non-blocking flag!
//		}
//
//		IncomingConnection(newsock);
//		close(sock);
//	}
//}


//---------------------------------------------
//bool IncomingConnection(MySocket & newsock)
//{
 //   int Pos = 0;
 //   int Result = 0;
 //   int DataSize = 2048;
//	char Buffer[2048];

    // itt jott be a kapcsolat
//	do {
//		Result = recv(newsock, Buffer+Pos, sizeof(DataSize - 2), 0);

//		if (Result == -1)
//		{
//		    printf("recv: %s (%d)\n", strerror(errno), errno);
//			int Code = errno;

//			if ( (Code == EWOULDBLOCK) || (Code == EAGAIN) )
//			{
//				//Result = 0;
//				if (Pos > 0)
 //                   break;
//			}
//			else
//			{
//			    printf("recv failed: \n");
				//printf("recv failed: %d\n", Code);
//			}
//		}

//		if ((Result + Pos) > DataSize)
//		{ //Ha kilog a bejovo csomag, akkor torlunk mindent
//			memset(Buffer, 0, DataSize);
//			Result = 0;
//			Pos = 0;
//		}

//		if (Result > 0)
//		{
//			printf("Bytes received: %d\n", Result);
//		}
//		else
//		{
//			printf("recv failed: %d\n", WSAGetLastError());
//		}

//		if (Result>0)
 //       {
 //           Pos += Result;
//        }

//	} while (Result != 0);

//	const char* SendString = "HTTP/1.1 200 OK\r\n" \
//				"Date : Sat, 21 Apr 2018 13:11:22 GMT\r\n" \
//				"Server : WebStar_2.11\r\n" \
//	/*			"Content - Length : 10899\r\n" \ */
//				"Keep-Alive : timeout = 5, max = 100\r\n"\
//				"Connection : Keep-Alive\r\n" \
//				"Content-Type: text/html; charset: UTF-8\r\n" \
//				"\r\n" \
//				"<!DOCTYPE html>\r\n" \
//				"<html lang = \"hu\">\r\n" \
//				"<head>\r\n" \
//				"<title>Teszt weboldal</title>\r\n" \
//				"</head>\r\n" \
//				"<body>\r\n" \
//				"Teszt sz�veg\r\n" \
//				"<h3>Elstartolt a Diana Fil�horgol�s Special magazin</h3>\r\n" \
//				"<h4>Tesztsz�veg34</h4>\r\n" \
//				"</body>\r\n" \
//				"</html>\r\n";

//    //Result = send(newsock, SendString, strlen(SendString), 0);
//    if (Result < 0) //Todo: test this functionality!
//    {
//        printf("error!!!!");
//    }
//    else
//    {
//        printf("Bytes sent: %d\n", Result);
//    }
//
 //   sleep(1);
    //close(newsock);
//
 //   return 0;
//}

//---------------------------------------------
//int cSocket::InitSocket(const char* aPort, const bool aListen);
//{
//	struct addrinfo hints, *res;
//	int reuseaddr = 1; /* True */
//
//	/* Get the address info */
//	memset(&hints, 0, sizeof hints);
//	hints.ai_family = AF_INET;
//	hints.ai_socktype = SOCK_STREAM;
//	if (getaddrinfo(NULL, PORT, &hints, &res) != 0)
//	{
// 		perror("getaddrinfo");
//		return 1;
//	}

	/* Create the socket */
//	_Socket = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
//	if (_Socket == -1)
//	{
//		perror("socket");
//		return 1;
//	}

	/* Enable the socket to reuse the address */
//	if (setsockopt(_Socket, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(int)) == -1)
//	{
//		perror("setsockopt");
//		return 1;
//	}

	/* Bind to the address */
//	if (bind(_Socket, res->ai_addr, res->ai_addrlen) == -1)
//	{
//		perror("bind");
//		return 1;
//	}

//	if (aListen) /* Listen */
//	{
//		_ServerSocket = true;

		/* Listen */
//		if (listen(_Socket, BACKLOG) == -1) {
//			perror("listen");
//			return 1;
//		}
//	}

// freeaddrinfo(res);
//}
