// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>

#include "../ModBus/modbus.h"
#include "../ModBus/modbus-tcp.h"
#include "ModBusServer.h"
#include "../Core/Defines.h"
#include "../Core/Boss.h"
#include "../Logger/Logger.h"
#include "../Data/Data_RMU-1.h"

#if defined(_WIN32)
	#include <ws2tcpip.h>
#else
	#include <sys/select.h>
	#include <sys/socket.h>
	#include <sys/time.h>
	#include <string.h>
#define TIMEVAL timeval
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <unistd.h>
#endif

#define NB_CONNECTION    5

using namespace DataStore;

//---------------------------------------------
cModBusServer::cModBusServer() noexcept
	: QuitNow(false)
{

}

//---------------------------------------------
cModBusServer::~cModBusServer()
{
}

//---------------------------------------------
void cModBusServer::Done()
{
	if (server_socket != -1) {
#ifdef _WIN32
		closesocket(server_socket);
#else
		close(server_socket);
#endif

	}
	modbus_free(ctx);
	ctx = nullptr;
	modbus_mapping_free(mb_mapping);
}

//---------------------------------------------
bool cModBusServer::Init()
{
	MyLogLine("ModBusServer", "Init start");

	ctx = modbus_new_tcp(SERVER_IP, MODBUS_PORT);

	mb_mapping = modbus_mapping_new( eRMU1_Output::RMU1_output_enum_num+2, 1, 1, eRMU1_Input::RMU1_input_enum_num+2); //Max : 1800, 1800, 120, 120
	if (mb_mapping == NULL)
	{
		MyLogLine("ModBusServer: Failed to allocate the mapping:", modbus_strerror(errno));
		modbus_free(ctx);
		ctx = nullptr;
		return true;
	}

	SetOutput(RMU1_AntiLeg_Tiltas, true); //Default value

//==========================================================
	//SetOutput(RMU1_VezKI12, true); //Holding register (output bits data)
	//SetOutput(RMU1_VezKI12, false); //Holding register (output bits data)
	//SetOutput(RMU1_VezKI34, false);
	//SetOutput(RMU1_HMV_antileg_szoc, true);
	//SetOutput(RMU1_HMV_antileg_konyha, true);
	//SetOutput(RMU1_Szellozo_sziv_tiltas, true);
	//SetOutput(RMU1_Szellozo_sziv_tiltas, false); 

	//SetInput(RMU1_EVU3FOK12, 120.22f); //Input registers
	//SetInput(RMU1_EVU3FOK34, 1.f);
	//SetInput(RMU1_HSZ1ON, 12.66f);
	//SetInput(RMU1_HSZ2ON, -34.1f);
	//SetInput(RMU1_HSZ3ON, -110.1f);
	//SetInput(RMU1_HSZ4ON, -1.11111f);
	//
	//mb_mapping->tab_input_bits[0] = 0xff; //Input status
	//mb_mapping->tab_bits[0] = 0xaa; //Coil status
//==========================================================

	server_socket = modbus_tcp_listen(ctx, NB_CONNECTION);
	if (server_socket == -1)
	{
		MyLogLine("ModBusServer", "Unable to listen! (Wrong IP? Missing SUDO? Unable to bind port - already running?)", errno);

		modbus_free(ctx);
		ctx = nullptr;
		return true;
	}

	/* Clear the reference set of socket */
	FD_ZERO(&refset);
	/* Add the server socket */
	FD_SET(server_socket, &refset);

	/* Keep track of the max file descriptor */
	fdmax = server_socket;

	return false;
}

//---------------------------------------------
/*void cModBusServer::DelayedInputLoop()
{
	if (!DelayStarted)
		return;
}*/

//---------------------------------------------
bool cModBusServer::ModBusLoop()
{
	rdset = refset;
	TIMEVAL TimeVal;
	TimeVal.tv_sec = 2;
	TimeVal.tv_usec = 2;
	if (select(fdmax + 1, &rdset, NULL, NULL, &TimeVal) == -1)
	{
		MyLogLine("ModBusServer", "ERROR: Server select() failed!");
		MySleep(500);
		return true;
	}

	/* Run through the existing connections looking for data to be
	* read */
	for (master_socket = 0; master_socket <= fdmax; master_socket++)
	{

		if (!FD_ISSET(master_socket, &rdset))
		{
			continue;
		}

		if (master_socket == server_socket)
		{
			/* A client is asking a new connection */
			socklen_t addrlen;
			struct sockaddr_in clientaddr;
			int newfd;

			/* Handle new connections */
			addrlen = sizeof(clientaddr);
			memset(&clientaddr, 0, sizeof(clientaddr));
			newfd = accept(server_socket, (struct sockaddr *)&clientaddr, &addrlen);
			if (newfd == -1)
			{
				MyLogLine("ModBusServer", "Server accept() error");
			}
			else
			{
				FD_SET(newfd, &refset);

				if (newfd > fdmax)
				{
					/* Keep track of the maximum */
					fdmax = newfd;
				}

				//printf("New connection from %s:%d on socket %d\n", inet_ntoa(clientaddr.sin_addr), clientaddr.sin_port, newfd);
			}
		}
		else
		{
			modbus_set_socket(ctx, master_socket);
			rc = modbus_receive(ctx, query);
			if (rc > 0)
			{
				modbus_reply(ctx, query, rc, mb_mapping);
			}
			else if (rc == -1)
			{
				/* This example server in ended on connection closing or
				* any errors. */
				//printf("Connection closed on socket %d\n",master_socket);
				
				#ifdef _WIN32
						closesocket(master_socket);
				#else
						close(master_socket);
				#endif

				/* Remove from reference set */
				FD_CLR(master_socket, &refset);

				if (master_socket == fdmax)
				{
					fdmax--;
				}
			}
		}
	}

	return false;
}

//---------------------------------------------
void cModBusServer::Run()
{
	Init();

	while (!QuitNow)
	{
		ModBusLoop();
		//DelayedInputLoop();
		MySleep(10);
	}

	Done();
}

//---------------------------------------------
void cModBusServer::SetQuitFlag()
{
	QuitNow = true;
}

//---------------------------------------------
float cModBusServer::GetInput(eRMU1_Input aReg)
{
	if (!mb_mapping)
	{
		MyLogLine("cModusServer", "GetInput - mb_mapping == NULL");
	}
	else
	{
		return (((float)mb_mapping->tab_input_registers[(int)aReg])/100.f);
	}

	return -1;
}

//---------------------------------------------
void cModBusServer::_SetInput(DataStore::eRMU1_Input aReg, float aValue)
{
	mb_mapping->tab_input_registers[(int)aReg] = ((short)aValue);
}

//---------------------------------------------
void cModBusServer::SetInputFloat(eRMU1_Input aReg, float aValue, bool aDelayed)
{
	if (aValue > 325.f)
	{
		MyLogLine("cModusServer", "Signed Input value > 325!", (int)aReg);
		aValue = 325.f;
	}

	if (aValue < -325.f)
	{
		MyLogLine("cModusServer", "Signed Input value < -325!", (int)aReg);
		aValue = -325.f;
	}

	if (!mb_mapping)
	{
		MyLogLine("cModusServer", "SetInputFloat - mb_mapping == NULL");
	}
	else
	{
		if (!aDelayed)
		{
			_SetInput(aReg, aValue*100.f); //Azonnal beirjuk
		}
		else
		{
			SetDelayedInput(aReg, aValue*100.f); //Kesobb irodik be
		}
		//printf("id: %d\n", aReg);
	}
}

//---------------------------------------------
void cModBusServer::SetDelayedInput(eRMU1_Input aReg, float aValue)
{
	if (aReg != RMU1_Tkor_nyomas_alacsony)
	{
		MyLogLine("cModusServer", "SetDelayedInput != RMU1_Tkor_nyomas_alacsony");
	}

	if (aValue > 0.8f) //Riasztas nincs vagy mar vege van
	{ 
		DelayStarted = false;
		_SetInput(aReg, aValue);
	}
	else if (aValue < 0.2f) //1 es 100 is lehet az aValue erteke!
	{
		std::tm CurrTime;
		cBoss::Get()->GetTimeAndDate(CurrTime);

		if (!DelayStarted)
		{
			CurrTime.tm_min += TKOR_NYOMAS_DELAY_MIN;
			DelayTimeTkor = CurrTime;
			std::mktime(&DelayTimeTkor); //Normalised time and date
			DelayValue = aValue;
			DelayStarted = true;
		}
		else
		{
			//Minden ok, ujra jo ertek jott, mar csak ki kell varni a delay idejet, nem csinálunk semmit itt
			if (mktime(&DelayTimeTkor) < mktime(&CurrTime)) // Time is out!
			{
				_SetInput(aReg, DelayValue);
				DelayStarted = false;
			}
		}
	}
}

//---------------------------------------------
void cModBusServer::SetInputFixed(eRMU1_Input aReg, float aValue, bool aDelayed)
{
	if (aValue > 32500.f)
	{
		MyLogLine("cModusServer", "Fixed Input value > 32500!", (int) aReg);
		aValue = 32500.f;
	}

	if (aValue < -32500.f)
	{
		MyLogLine("cModusServer", "Fixed Input value < -32500!", (int) aReg);
		aValue = -32500.f;
	}

	if (!mb_mapping)
	{
		MyLogLine("cModusServer", "SetInputFixed - mb_mapping == NULL");
	}
	else
	{
		if (!aDelayed)
		{
			_SetInput(aReg, aValue); //Azonnal beirjuk
		}
		else
		{
			SetDelayedInput(aReg, aValue); //Kesobb irodik be
		}
		//printf("id: %d\n", aReg);
	}
}

//---------------------------------------------
bool cModBusServer::GetOutput(eRMU1_Output aReg)
{
	if (!mb_mapping)
	{
		MyLogLine("cModusServer", "ERROR: GetOutput - mb_mapping == NULL");
		return false; //ajaj
	}
	else
	{
		//return (bool)(mb_mapping->tab_bits[0] >> (unsigned short)aReg) & 1U;
		return ((bool) mb_mapping->tab_bits[(int)aReg]);
	}
}

//---------------------------------------------
void cModBusServer::SetOutput(eRMU1_Output aReg, bool aValue)
{
	if (!mb_mapping)
	{
		MyLogLine("cModusServer", "ERROR: SetOutput - mb_mapping == NULL");
	}
	else
	{
		/*if (aValue == true)
			mb_mapping->tab_bits[0] |= 1UL << (unsigned short)aReg;
		else
			mb_mapping->tab_bits[0] &= ~(1UL << (unsigned short)aReg);*/
		mb_mapping->tab_bits[(int)aReg] = aValue;
	}
}

//---------------------------------------------
/*int cModBusServer::GetInputBits(int aReg)
{
	if (!mb_mapping)
	{
		MyLogLine("cModusServer", "GetInputBit - mb_mapping == NULL");
	}
	else
		return mb_mapping->tab_input_bits[aReg];

	return -1;
}

//---------------------------------------------
void cModBusServer::SetInputBits(int aReg, int aValue)
{
	if (!mb_mapping)
	{
		MyLogLine("cModusServer", "SetInputBit - mb_mapping == NULL");
	}
	else
		mb_mapping->tab_input_bits[aReg] = aValue;
}

//---------------------------------------------
int cModBusServer::GetCoil(int aReg)
{
	if (!mb_mapping)
	{
		MyLogLine("cModusServer", "GetCoil - mb_mapping == NULL");
	}
	else
		return mb_mapping->tab_bits[aReg];

	return -1;
}

//---------------------------------------------
void cModBusServer::SetCoil(int aReg, int aValue)
{
	if (!mb_mapping)
	{
		MyLogLine("cModusServer", "SetCoil - mb_mapping == NULL");
	}
	else
		mb_mapping->tab_bits[aReg] = aValue;
}
*/

