// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#include <stdio.h>
//#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <chrono>
#include <ctime>

#include "../Data/Data_RMU-1.h"

#include "../ModBus/modbus.h"
#include "../ModBus/modbus-tcp.h"

#if defined(_WIN32)
#include <ws2tcpip.h>
#else
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif



//---------------------------------------------
class cModBusServer
{
public:
	cModBusServer() noexcept;
	~cModBusServer();

	bool Init();
	void Run();
	void Done();

	void SetQuitFlag();

	float GetInput(DataStore::eRMU1_Input aReg);
	void  _SetInput(DataStore::eRMU1_Input aReg, float aValue);

	void SetDelayedInput(DataStore::eRMU1_Input aReg, float aValue); //Ha nem valtozik az ertek aDelayMin percig, akkor allitja be az ujat csak

	void SetInputFloat(DataStore::eRMU1_Input aReg, float aValue, bool aDelayed=false); // False-immediately. True-delayed if no other value arrives
	void SetInputFixed(DataStore::eRMU1_Input aReg, float aValue, bool aDelayed=false);

	bool GetOutput(DataStore::eRMU1_Output aReg);
	void SetOutput(DataStore::eRMU1_Output aReg, bool aValue);

	//int GetInputBits(int aReg);
	//void SetInputBits(int aReg, int aValue);

	//int GetCoil(int aReg);
	//void SetCoil(int aReg, int aValue);

private:
	bool ModBusLoop();
	//void DelayedInputLoop();

private:
	modbus_t *ctx = NULL;
	modbus_mapping_t *mb_mapping = NULL;
	int server_socket = -1;

	uint8_t query[MODBUS_TCP_MAX_ADU_LENGTH] = {0};
	int master_socket = 0;
	int rc = 0;
	fd_set refset;
	fd_set rdset;
	/* Maximum file descriptor number */
	int fdmax = 0;
	
	bool QuitNow = false;	// Quit thread flag

	static constexpr auto TKOR_NYOMAS_DELAY_MIN = 59; //The delay for the Tkor_nyomas_alacsony alert 
	std::tm DelayTimeTkor;			//Tkor_nyomas_alacsony delay end time
	bool	DelayStarted = false;	//Tkor_nyomas_alacsony delay started or not
	float	DelayValue = 0.f;		//Tkor_nyomas_alacsony delay value
};
