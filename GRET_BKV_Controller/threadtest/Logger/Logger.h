// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#pragma once

#include <stdio.h>
#include <ctime>
#include <string>
#include <mutex>


//--------------------------------------------------------------
class cLogger
{
public:
	cLogger() noexcept;
	~cLogger();

	void LogLine(const char * aModule, const int aCode);
	void LogLine(const char * aModule, const int aCode1, const int aCode2);

	void LogLine(const char * aModule, const char* aLine);
	void LogLine(const char * aModule, const char* aLine, const int aCode);
	void LogLine(const char * aModule, const char* aLine, const int aCode1, const int aCode2);

private:
	FILE*		LogFile;
	std::mutex	LogMutex;

	void _LogLine(std::string& aString);
	void OpenLogFile();
	void CloseLogFile();
};