// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#include "Logger.h"
#include "../Core/Boss.h"
#include <stdio.h>
#include <string>
#include <sstream>
#include <ctime>

//--------------------------------------------------------------
cLogger::cLogger() noexcept
	: LogFile(NULL)
{
	LogLine("cLogger", "Inited succesfully.");
}

//--------------------------------------------------------------
cLogger::~cLogger()
{
	CloseLogFile();
}

//--------------------------------------------------------------
//--------------------------------------------------------------
void cLogger::OpenLogFile()
{
	tm Time;
	int error = 0;
	cBoss::Get()->GetTimeAndDate(Time);

	char filename[32];
	std::strftime(filename, 32, "GretLog_%Y-%m-%d.log", &Time);
#ifdef _WIN32
	error = fopen_s(&LogFile, filename, "a");
#else
	LogFile = fopen(filename, "a");
#endif
	if ((error != 0) || (LogFile == NULL))
	{
		printf("********************************\r\n");
		printf("*** Cannot create log file!! ***\r\n");
		printf("********************************\r\n");
		return;
	}
}

//--------------------------------------------------------------
void cLogger::CloseLogFile()
{
	if (LogFile) //Ha nem sikerult legyartani, akkor NULL lehet
	{
		fclose(LogFile);
		LogFile = NULL;
	}
}

//--------------------------------------------------------------
//--------------------------------------------------------------
void cLogger::_LogLine(std::string& aString)
{
	tm Time;
	cBoss::Get()->GetTimeAndDate(Time);

	char Logtime[32];
	std::strftime(Logtime, 32, "[%Y.%m.%d]-%H:%M:%S - ", &Time);

	std::ostringstream FullStream;
	FullStream << Logtime << aString << std::endl;

	{
		std::lock_guard<std::mutex> guard(LogMutex);

		OpenLogFile();
		if (LogFile)
		{
			fputs(FullStream.str().c_str(), LogFile); //Log to file
		}
		CloseLogFile();
	}

	printf("%s", FullStream.str().c_str()); //Log to console
}

//--------------------------------------------------------------
void cLogger::LogLine(const char * aModule, const int aCode1)
{
	std::ostringstream FullStream;
	FullStream << aModule << " (" << aCode1 << ") ";

	std::string out = FullStream.str();
	_LogLine(out); //Log to file
}

//--------------------------------------------------------------
void cLogger::LogLine(const char * aModule, const int aCode1, const int aCode2)
{
	std::ostringstream FullStream;
	FullStream  << aModule << ": " << " (" << aCode1 << ") (" << aCode2 << ")";

	std::string out = FullStream.str();
	_LogLine(out); //Log to file
}

//--------------------------------------------------------------
void cLogger::LogLine(const char * aModule, const char* aLine)
{
	std::ostringstream FullStream;
	FullStream << aModule << ": " << aLine;

	std::string out = FullStream.str();
	_LogLine(out); //Log to file
}

//--------------------------------------------------------------
void cLogger::LogLine(const char * aModule, const char * aLine, const int aCode)
{
	std::ostringstream FullStream;
	FullStream << aModule << ": " << aLine << " (" << aCode << ")";

	std::string out = FullStream.str();
	_LogLine(out); //Log to file
}

//--------------------------------------------------------------
void cLogger::LogLine(const char * aModule, const char * aLine, const int aCode1, const int aCode2)
{
	std::ostringstream FullStream;
	FullStream << aModule << ": " << aLine << " (" << aCode1 << ") (" << aCode2 << ")";

	std::string out = FullStream.str();
	_LogLine(out); //Log to file
}
