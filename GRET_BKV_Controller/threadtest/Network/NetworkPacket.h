// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#pragma once

#include "../Core/Defines.h"

//----------------------------------------------
class cNetworkPacket
{
public:
	cNetworkPacket() noexcept;
	
	int GetBufferMemorySize() const { return sizeof(buffer); }
	int GetBufferDataLength() const { return datasize; }

	int			SetBuffer(const char* aBuffer, const int aDataSize);
	const char* GetBuffer2(int& aDataSize) const;
	char*		GetBuffer2_(int& aDataSize); //only for buffer loading
	void		FillNull();
private:
	char	buffer[MAX_NETBUFFER_SIZE] = {0};
	int		datasize;
};