// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#define _CRT_SECURE_DEPRECATE_MEMORY

#include "NetworkPacket.h"
#include "memory.h"



//---------------------------------------------
cNetworkPacket::cNetworkPacket() noexcept :
	datasize(0)
{
}

//---------------------------------------------
int cNetworkPacket::SetBuffer(const char * aBuffer, const int aDataSize)
{
	if (aDataSize > MAX_NETBUFFER_SIZE)
		return 1;

	memcpy(buffer, aBuffer, aDataSize);
	datasize = aDataSize;
	return 0;
}

//---------------------------------------------
const char * cNetworkPacket::GetBuffer2(int & aDataSize) const
{
	aDataSize = datasize;
	return buffer;
}

//---------------------------------------------
char * cNetworkPacket::GetBuffer2_(int & aDataSize)
{
	aDataSize = datasize;
	return buffer;
}

//---------------------------------------------
void cNetworkPacket::FillNull()
{
	for (int i = 0; i < MAX_NETBUFFER_SIZE - 1; ++i)
	{
		buffer[i] = 0;
	}
}
