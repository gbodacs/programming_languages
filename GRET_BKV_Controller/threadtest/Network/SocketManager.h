// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#pragma once

#ifdef _WIN32
	#include <winsock2.h>
#endif

#include <stdio.h>
#include <queue>
#include <mutex>
#include "NetworkPacket.h"

#ifdef _WIN32
#include "../NetworkBase/WinSocket.h"
#else
#include "../NetworkBase/LinuxSocket.h"
#endif

//----------------------------------------------
class cSocketManager
{
public:
	cSocketManager();
	void SetQuitFlag();
	void Run();

	bool SendControllerRequest(const char* aContIp);

private:
	bool	QuitNow;	// Quit thread flag
	cSocket	ControllerSocket;	// Get data from other controllers and set data

	int		InitSockets();
	void	DoneSockets();
	void	SocketsLoop();
	void	CreateSendString(std::string& aOutputData);
};
