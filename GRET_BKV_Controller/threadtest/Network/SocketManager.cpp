// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#include <iostream>
#include <thread>

#ifdef _WIN32
	#include <ws2tcpip.h>
	#include "ws2def.h"
#else
	#include <unistd.h>
	#include <string.h>
#endif


#include "SocketManager.h"
#include "../Core/Defines.h"
#include "../Core/Boss.h"
#include "../Core/DataHelper.h"
#include "../ModBus/ModBusServer.h"
#include "../Logger/Logger.h"
#include "../Data/Data_RMU-1.h"
#include <string>

using namespace DataStore;
//---------------------------------------------
// Ctor
cSocketManager::cSocketManager():
	QuitNow(false)
{
}

//---------------------------------------------
void cSocketManager::SetQuitFlag()
{
	QuitNow = true;
}

//---------------------------------------------
int cSocketManager::InitSockets()
{
	MyLogLine("cSocketManager", "Inited ended.");
	return 0;
}

//---------------------------------------------
void cSocketManager::DoneSockets()
{
	MyLogLine("cSocketManager", "Done ended.");
}

//---------------------------------------------
void cSocketManager::SocketsLoop()
{
	bool Ret = SendControllerRequest(RMU1_IP);

	if (Ret)
	{
		MyLogLine("cSocketManager", "SocketLoop problem!");
	}

//	MySleep(250);
}

//---------------------------------------------
void cSocketManager::Run()
{
	InitSockets();

	while (!QuitNow)
	{
		SocketsLoop();
	}

	DoneSockets();
}

//---------------------------------------------
void cSocketManager::CreateSendString(std::string& aOutputData)
{
	aOutputData = "";
	for (int i = 0; i < RMU1_output_end_controllino; ++i) //TODO: hack to test!!
	{
		aOutputData += "@";
		aOutputData += GetRMU1_Output_Name((eRMU1_Output)i);
		aOutputData += "=";
		aOutputData += std::to_string( cBoss::Get()->GetModBusServer()->GetOutput((eRMU1_Output)i) );
		aOutputData += ";";
	}
}

//---------------------------------------------
bool cSocketManager::SendControllerRequest(const char* aContIp)
{
	cNetworkPacket SendPacket, RecvPacket;
	std::string SendData;
	CreateSendString(SendData);

	/*printf("\n");
	printf(SendData.c_str());
	printf("\n");*/

	SendPacket.SetBuffer(SendData.c_str(), strlen(SendData.c_str())+1);

	if (ControllerSocket.InitClientSocket(aContIp, CONTROLLINO_PORT, CONTROLLINO_PORT_STR, SendPacket, &RecvPacket) != 0)
	{
		cBoss::Get()->GetLogger()->LogLine("cSocketManager::SendControllerRequest", "Initclientsocket failed!");
		return true;
	}

	MyDataHelper->UpdateMappingWithNewData(&RecvPacket, aContIp);

	return false;
}

