// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#pragma once


namespace DataStore
{
	//-------------------------------------------------------------
	enum eRMU1_Input
	{
		// Input - potencialmentes kontakt
		RMU1_EVU3FOK12,
		RMU1_EVU3FOK34,
		RMU1_HSZ1ON,
		RMU1_HSZ2ON,
		RMU1_HSZ3ON,
		RMU1_HSZ4ON,
		RMU1_HSZ1Error,
		RMU1_HSZ2Error,
		RMU1_HSZ3Error,
		RMU1_HSZ4Error,
		RMU1_motor_ved1,
		RMU1_motor_ved2,
		RMU1_motor_ved3,
		RMU1_Ajto_nyitva,
		RMU1_Szivarg,
		RMU1_Tkor_nyomas_alacsony,

		// Input analog 0-10V
		RMU1_Nyomas,

		// Input PT100
		RMU1_Kulso,
		RMU1_HMV_konyha,
		RMU1_HMV_szoc,
		RMU1_Puffer,
		RMU1_Hoforras_elor,
		RMU1_Hoforras_viss,
		RMU1_Zona1,
		RMU1_Zona3,
		RMU1_Zona4,
	
		RMU1_input_enum_num
	};

	//-------------------------------------------------------------
	enum eRMU1_Output
	{
		// Digital Output 12V
		RMU1_VezKI12,
		RMU1_VezKI34,
		RMU1_HMV_antileg_szoc,
		RMU1_HMV_antileg_konyha,
		RMU1_Szellozo_sziv_tiltas,

		RMU1_output_end_controllino, 
		RMU1_AntiLeg_Tiltas = RMU1_output_end_controllino,

		RMU1_output_enum_num
	};

	//-------------------------------------------------------------
	const char* GetRMU1_Input_Name(eRMU1_Input Input);
	const char* GetRMU1_Output_Name(eRMU1_Output Output);

	//-------------------------------------------------------------
	bool TestRMU1_Input();
	bool TestRMU1_Output();
};