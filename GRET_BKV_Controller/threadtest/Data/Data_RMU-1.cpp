// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#include "Data_RMU-1.h"
#include "../Logger/Logger.h"
#include "../Core/Defines.h"
#include "../Core/Boss.h"

//-------------------------------------------------------------
const char* DataStore::GetRMU1_Input_Name(DataStore::eRMU1_Input Input)
{

	switch (Input)
	{
			//Input - potencialmentes kontakt
		case eRMU1_Input::RMU1_EVU3FOK12:
			return "EVU3FOK12";
		case eRMU1_Input::RMU1_EVU3FOK34:
			return "EVU3FOK34";
		case eRMU1_Input::RMU1_HSZ1ON:
			return "HSZ1ON";
		case eRMU1_Input::RMU1_HSZ2ON:
			return "HSZ2ON";
		case eRMU1_Input::RMU1_HSZ3ON:
			return "HSZ3ON";
		case eRMU1_Input::RMU1_HSZ4ON:
			return "HSZ4ON";
		case eRMU1_Input::RMU1_HSZ1Error:
			return "HSZ1Error";
		case eRMU1_Input::RMU1_HSZ2Error:
			return "HSZ2Error";
		case eRMU1_Input::RMU1_HSZ3Error:
			return "HSZ3Error";
		case eRMU1_Input::RMU1_HSZ4Error:
			return "HSZ4Error";
		case eRMU1_Input::RMU1_motor_ved1:
			return "Motor_ved1";
		case eRMU1_Input::RMU1_motor_ved2:
			return "Motor_ved2";
		case eRMU1_Input::RMU1_motor_ved3:
			return "Motor_ved3";
		case eRMU1_Input::RMU1_Ajto_nyitva:
			return "Ajto_nyitva";
		case eRMU1_Input::RMU1_Szivarg:
			return "Szivarg";
		case eRMU1_Input::RMU1_Tkor_nyomas_alacsony:
			return "Tkor_nyomas_alacsony";
		case eRMU1_Input::RMU1_Nyomas:
			return "Nyomas";
			// Input PT1000
		case eRMU1_Input::RMU1_Kulso:
			return "Kulso";
		case eRMU1_Input::RMU1_HMV_konyha:
			return "HMV_konyha";
		case eRMU1_Input::RMU1_HMV_szoc:
			return "HMV_szoc";
		case eRMU1_Input::RMU1_Puffer:
			return "Puffer";
		case eRMU1_Input::RMU1_Hoforras_elor:
			return "Hoforras_elor";
		case eRMU1_Input::RMU1_Hoforras_viss:
			return "Hoforras_viss";
		case eRMU1_Input::RMU1_Zona1:
			return "Zona1";
		case eRMU1_Input::RMU1_Zona3:
			return "Zona3";
		case eRMU1_Input::RMU1_Zona4:
			return "Zona4";
		case eRMU1_Input::RMU1_input_enum_num:
			break;
		default:
		{
			MyLogLine("DataStore", "ERROR: String is missing from input enum!\n");
			break;
		}
	}
	return "";
};

//-------------------------------------------------------------
const char* DataStore::GetRMU1_Output_Name(DataStore::eRMU1_Output Output)
{
	switch (Output)
	{
			//Digital Output 12V
		case eRMU1_Output::RMU1_VezKI12:
			return "VezKI12";
		case eRMU1_Output::RMU1_VezKI34:
			return "VezKI34";
		case eRMU1_Output::RMU1_HMV_antileg_szoc:
			return "HMV_antileg_szoc";
		case eRMU1_Output::RMU1_HMV_antileg_konyha:
			return "HMV_antileg_konyha";
		case eRMU1_Output::RMU1_Szellozo_sziv_tiltas:
			return "Szellozo_sziv_tiltas";
		case eRMU1_Output::RMU1_AntiLeg_Tiltas:
			return "AntiLeg_Tiltas";
		case eRMU1_Output::RMU1_output_enum_num:
			break;
		default:
		{
			MyLogLine("DataStore", "ERROR: String is missing from output enum!");
			break;
		}
	}
	return "";
};

//-------------------------------------------------------------
bool DataStore::TestRMU1_Input()
{
	for (int i = 0; i < RMU1_input_enum_num; ++i)
	{
		if (GetRMU1_Input_Name((eRMU1_Input)i)[0] == 0)
		{
			MyLogLine("DataStore", "!!Input!! String is missing from the enum!");
			return false;
		}
	}

	if (GetRMU1_Input_Name(RMU1_input_enum_num)[0] != 0)
	{
		MyLogLine("DataStore", "!!Input!! String is assigned to the enum_num!");
		return false;
	}

	return true;
}

//-------------------------------------------------------------
bool DataStore::TestRMU1_Output()
{
	for (int i = 0; i < RMU1_output_enum_num; ++i)
	{
		if (GetRMU1_Output_Name((eRMU1_Output)i)[0] == 0)
		{
			MyLogLine("DataStore", "!!Output!! String is missing from the enum!");
			return false;
		}
	}

	if (GetRMU1_Output_Name(RMU1_output_enum_num)[0] != 0)
	{
		MyLogLine("DataStore", "!!Output!! String is assigned to the enum_num!");
		return false;
	}

	return true;
}

