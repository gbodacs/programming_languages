// Copyright(C) 2019 - Gabor Bodacs - All Rights Reserved

#include <iostream>
#include <thread>
#include "signal.h"

#include "Logger/Logger.h"
#include "Core/Boss.h"

#include "Network/SocketManager.h"


#ifdef _WIN32
//---------------------------------------------------------------------
// WIN32 signal handler
//---------------------------------------------------------------------
#include <windows.h>
BOOL WINAPI SignalHandler(DWORD event1)
{
	switch (event1)
	{
		case CTRL_BREAK_EVENT:
		case CTRL_CLOSE_EVENT:
		case CTRL_LOGOFF_EVENT:
		case CTRL_SHUTDOWN_EVENT:
		case CTRL_C_EVENT:
		{
			printf("Exit signal received. Setting quit flag...\r\n");
			cBoss::Get()->SetQuitFlag();
			
			break;
		}

		default:
			break;
	}

	return TRUE;
}
#else
//---------------------------------------------------------------------
// Linux signal handler
//---------------------------------------------------------------------
//#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>


void SignalHandler(int signum)
{
	printf("Exit signal received. Setting quit flag...\r\n");
	cBoss::Get()->SetQuitFlag();
}
#endif

//---------------------------------------------------------------------
int main()
{
#ifdef _WIN32
	if (!SetConsoleCtrlHandler(SignalHandler, TRUE))
	{
		printf_s("ERROR: Could not set control handler\n");
		return 1;
	}
#else
	struct sigaction action;
	memset(&action, 0, sizeof(struct sigaction));
	action.sa_handler = SignalHandler;
	sigaction(SIGTERM, &action, NULL);
	sigaction(SIGINT, &action, NULL);
	sigaction(SIGILL, &action, NULL);
	sigaction(SIGABRT, &action, NULL);
#endif



	cBoss* Boss = new cBoss;

	cBoss::Get()->Init();
	cBoss::Get()->Run();
	cBoss::Get()->Shutdown();

	SAFE_DELETE(Boss);

	return 0;
}


//3. Network hibakezelest megcsinalni

//1. Lek�rdezni az adatokat a vezerloktol.
//	- RBP: egy csomagban minden erteket
//		- trigger: 30mp-enkent (timeout: 10mp)
//	- Arduino: valaszol minden esetben !!!
//	- RBP: kapott adatokat ellenorizni es eltarolni!
//  - RBP: alerteket beallitani
//2. Figyelni, hogy j�tt-e a Mangbol request vagy bealliatas
//	- Ha beallitas j�tt:
//		- RBP eltarolja es szol a megfelelo Controllinonak
//		- Controllino eltarolja az EEPROM-ban es hasznalja az uj erteket
//	- Ha request j�tt:
//		- valaszolni (ModBus)
//3. Dontest hozni a beavatkozasrol
//	- Minden donteshez tartozik egy osztaly!
//	- Mindegyik iCommand osztaly Run() fuggvenye lefut minden alkalommal
//  - Ha a Run() true-t ad vissza, ki kell szedni a listabol!
//	- Lehet regisztralni idozeitett hivast!
//4. Idozeitett donteseket figyelni
//	- iCommand -> timer hivasakor lefut es kiszedi magat automatikusan az idozitett hivas listabol!
//5. Diag socket?
