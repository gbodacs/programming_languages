#include <Controllino.h>

void InputReadSetup()
{
  pinMode(CONTROLLINO_A0, INPUT);
  pinMode(CONTROLLINO_A1, INPUT);
  pinMode(CONTROLLINO_A2, INPUT);
  pinMode(CONTROLLINO_A3, INPUT);
  pinMode(CONTROLLINO_A4, INPUT);
  pinMode(CONTROLLINO_A5, INPUT);
  pinMode(CONTROLLINO_A6, INPUT);
  pinMode(CONTROLLINO_A7, INPUT);
  pinMode(CONTROLLINO_A8, INPUT);
  pinMode(CONTROLLINO_A9, INPUT);
  pinMode(CONTROLLINO_A10, INPUT);
  pinMode(CONTROLLINO_A11, INPUT);
  pinMode(CONTROLLINO_A12, INPUT);
  pinMode(CONTROLLINO_A13, INPUT);
  pinMode(CONTROLLINO_A14, INPUT);
  pinMode(CONTROLLINO_I16, INPUT);
  pinMode(CONTROLLINO_I17, INPUT);
}

void InputReadRun()
{
  int measurementResult;
  int digitalValue;

  digitalValue = digitalRead(CONTROLLINO_A0);
  measurementResult = analogRead(CONTROLLINO_A0);
  
  RMU1_Input_Data[RMU1_EVU3FOK12] = (digitalRead(CONTROLLINO_I16) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_EVU3FOK34] = (digitalRead(CONTROLLINO_I17) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_HSZ1ON] = (digitalRead(CONTROLLINO_A14) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_HSZ2ON] = (digitalRead(CONTROLLINO_A13) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_HSZ3ON] = (digitalRead(CONTROLLINO_A12) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_HSZ4ON] = (digitalRead(CONTROLLINO_A11) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_HSZ1Error] = (digitalRead(CONTROLLINO_A10) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_HSZ2Error] = (digitalRead(CONTROLLINO_A9) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_HSZ3Error] = (digitalRead(CONTROLLINO_A8) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_HSZ4Error] = (digitalRead(CONTROLLINO_A7) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_motor_ved1] = (digitalRead(CONTROLLINO_A6) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_motor_ved2] = (digitalRead(CONTROLLINO_A5) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_motor_ved3] = (digitalRead(CONTROLLINO_A4) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_Ajto_nyitva] = (digitalRead(CONTROLLINO_A3) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_Szivarg] = (digitalRead(CONTROLLINO_A2) == LOW) ? false : true;
  RMU1_Input_Data[RMU1_Tkor_nyomas_alacsony] = (digitalRead(CONTROLLINO_A1) == LOW) ? false : true;
  
  RMU1_Input_Data[RMU1_Nyomas] = analogRead(CONTROLLINO_A0); //0...1024
}

