//===========================================
int GetOutputIndexByName(String Name)
{
  for (int i=0; i<RMU1_Output_enum_num; ++i)
  {
    if (Name == RMU1_Output_Name[i])
    {
      return i;
    }
  }
  
  Serial.println("Error: cannot find output Name!!");
  return -1;
}

//===========================================
void DataReady(String Name, String Value)
{
  int iValue = Value.toInt();
  int Index = GetOutputIndexByName(Name);

  if (Index == -1)
  {
    return;
  }
  
  RMU1_Output_Data[Index] = ((iValue==0)? false : true);

  //Serial.print(Name);
  //Serial.print("=");
  //Serial.println(RMU1_Output_Data[Index]);
}

//===========================================
void ParseIncomingPacket(String Data)
{
  String Name;
  String Value;

  bool isName = false;
  bool isValue = false;
  
  for(int i=0; i<Data.length()+1; ++i)
  {
    if (Data[i] == '@') //name kovetkezik
    {
      isName = true;
      isValue = false;
      continue;
    }
    
    if (Data[i] == '=') //value kovetkezik
    {
      isName = false;
      isValue = true;
      continue;
    }

    if (Data[i] == ';') //vege van
    {
      isName = false;
      isValue = false;
      DataReady(Name, Value);
      Name="";
      Value="";
      continue;
    }

    if (isName)
    {
      Name += Data[i];
    }

    if (isValue)
    {
      Value += Data[i];
    }
  }
}

