#include <Controllino.h>  /* Usage of CONTROLLINO library allows you to use CONTROLLINO_xx aliases in your sketch. */
#include "ModbusRtu.h"

//Master device address
#define MasterModbusAdd  0
//Slave devices addresses
#define SlaveModbusAdd1  1
#define SlaveModbusAdd2  2
#define SlaveModbusAdd3  3

#define NUM_MODBUS_DEVICES 3

uint16_t ModbusSlaveRegisters[SlaveModbusAdd3+1][16];

//Serial port (UART) for RS485
#define RS485Serial     3

// The object ControllinoModbuMaster of the class Modbus is initialized with three parameters.
// The first parametr specifies the address of the Modbus master device.
// The second parameter specifies type of the interface used for communication between devices - in this sketch is used RS485.
// The third parameter can be any number. During the initialization of the object this parameter has no effect.
Modbus ControllinoModbusMaster(MasterModbusAdd, RS485Serial, 0);

modbus_t ModbusQuery[NUM_MODBUS_DEVICES];

uint8_t currentQuery = 0; // pointer to message query
int StateMach = 0;
long WaitingTime = 0;
bool StartWait = false;

//=================================================================================================================================
void StartModbusWait()
{
  WaitingTime = millis() + 300;
  StartWait = true;
}

//=================================================================================================================================
void ModBusSetup()
{
    ControllinoModbusMaster.begin( 19200 ); // baud-rate at 19200
    ControllinoModbusMaster.setTimeOut( 500 ); // if there is no answer in 500 ms, roll over

	  StartModbusWait();

  	// ModbusQuery 0: read temperature registers
  	ModbusQuery[0].u8id = SlaveModbusAdd1; // slave address
  	ModbusQuery[0].u8fct = 3; // function code (this one is registers read)
  	ModbusQuery[0].u16RegAdd = 6; // start address in slave
  	ModbusQuery[0].u16CoilsNo = 8; // number of elements (coils or registers) to read
  	ModbusQuery[0].au16reg = &ModbusSlaveRegisters[SlaveModbusAdd1-1][0]; // pointer to a memory array in the CONTROLLINO
  
  	// ModbusQuery 1: read temperature registers
  	ModbusQuery[1].u8id = SlaveModbusAdd2; // slave address
  	ModbusQuery[1].u8fct = 3; // function code (this one is registers read)
  	ModbusQuery[1].u16RegAdd = 6; // start address in slave
  	ModbusQuery[1].u16CoilsNo = 8; // number of elements (coils or registers) to read
  	ModbusQuery[1].au16reg = &ModbusSlaveRegisters[SlaveModbusAdd2-1][0]; // pointer to a memory array in the CONTROLLINO
  
  	// ModbusQuery 2: read temperature registers
  	ModbusQuery[2].u8id = SlaveModbusAdd3; // slave address
  	ModbusQuery[2].u8fct = 3; // function code (this one is registers read)
  	ModbusQuery[2].u16RegAdd = 6; // start address in slave
  	ModbusQuery[2].u16CoilsNo = 8; // number of elements (coils or registers) to read
  	ModbusQuery[2].au16reg = &ModbusSlaveRegisters[SlaveModbusAdd3-1][0]; // pointer to a memory array in the CONTROLLINO

    StartModbusWait();

    StateMach = 0;
    Serial.println("Modbus init done.");
}

//=================================================================================================================================
void ModBusLoop()
{
	if (millis() < WaitingTime)
  {
	  return;
  }

	switch(StateMach) //allapotgep
	{
		case 0: //send 1. query
		{
			ControllinoModbusMaster.query( ModbusQuery[currentQuery] ); // send query
      Serial.print("query sent: ");
      Serial.println(currentQuery);
			StateMach = 1;
			break;
		}
   
		case 1: //wait for the answer from 1. query
		{
			ControllinoModbusMaster.poll(); // check incoming messages
			if (ControllinoModbusMaster.getState() == COM_IDLE) 
			{
				// response from the slave was received
				StartModbusWait();
				StateMach = 0;

        UpdateModbusData(currentQuery);

        Serial.println(" ");
        Serial.print("Query: ");
        Serial.println(currentQuery);
        for (int u=0; u<8; u++)
        {
          Serial.print("Ez jött: ");
          Serial.println(ModbusSlaveRegisters[currentQuery][u], HEX);
        }
        Serial.println(" ");

        currentQuery++;
        if (currentQuery >= NUM_MODBUS_DEVICES)
        {
          currentQuery = 0;
          ModBusDataReady = true;
        }
			}
			break;
		}
   
		default:
			Serial.println("Error modbus state!");
			break;
	}
}

//=================================================================================================================================
float ConvertToFloat(int SenecaIndex, int Index)
{
  uint32_t Temperature1 = (((uint32_t)ModbusSlaveRegisters[SenecaIndex][Index*2]) << 16) + ModbusSlaveRegisters[SenecaIndex][Index*2+1];
  float& Teszt =  *(float*)&Temperature1;
  //Teszt = (Teszt+300.f)*10.f;
  return Teszt;
}

//=================================================================================================================================
void UpdateModbusData(int query)
{
    float Temp;

    if (query == (SlaveModbusAdd1-1))
    {
      Temp = ConvertToFloat(SlaveModbusAdd1-1, 0);
      RMU1_Temperature_Data[RMU1_Kulso] = Temp;
      Temp = ConvertToFloat(SlaveModbusAdd1-1, 1);
      RMU1_Temperature_Data[RMU1_HMV_konyha] = Temp;
      Temp = ConvertToFloat(SlaveModbusAdd1-1, 2);
      RMU1_Temperature_Data[RMU1_HMV_szoc] = Temp;
      Temp = ConvertToFloat(SlaveModbusAdd1-1, 3);
      RMU1_Temperature_Data[RMU1_Puffer] = Temp;
    }

    if (query == (SlaveModbusAdd2-1))
    {
      Temp = ConvertToFloat(SlaveModbusAdd2-1, 0);
      RMU1_Temperature_Data[RMU1_Hoforras_elor] = Temp;
      Temp = ConvertToFloat(SlaveModbusAdd2-1, 1);
      RMU1_Temperature_Data[RMU1_Hoforras_viss] = Temp;
      Temp = ConvertToFloat(SlaveModbusAdd2-1, 2);
      RMU1_Temperature_Data[RMU1_Zona1] = Temp;
    }

    if (query == (SlaveModbusAdd3-1))
    {
      Temp = ConvertToFloat(SlaveModbusAdd3-1, 0);
      RMU1_Temperature_Data[RMU1_Zona3] = Temp;
      Temp = ConvertToFloat(SlaveModbusAdd3-1, 1);
      RMU1_Temperature_Data[RMU1_Zona4] = Temp;
      //Temp = ConvertToFloat(SlaveModbusAdd3-1, 2);
      //RMU1_Temperature_Data[RMU1_Legionella_1] = Temp;
      //Temp = ConvertToFloat(SlaveModbusAdd3-1, 3);
      //RMU1_Temperature_Data[RMU1_Legionella_2] = Temp;
    }
}
