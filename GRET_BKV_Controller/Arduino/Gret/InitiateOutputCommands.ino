#include <Controllino.h>

void OutputCommandSetup()
{
  pinMode(CONTROLLINO_D0, OUTPUT); //RMU1_VezKI12
  pinMode(CONTROLLINO_D1, OUTPUT); //RMU1_VezKI34
  pinMode(CONTROLLINO_D2, OUTPUT); //RMU1_HMV_antileg_szoc
  pinMode(CONTROLLINO_D3, OUTPUT); //RMU1_HMV_antileg_konyha
  pinMode(CONTROLLINO_D4, OUTPUT); //RMU1_Szellozo_sziv_tiltas
}

void OutputCommandRun()
{
  if (RMU1_Output_Data[RMU1_VezKI12] == true)
    {digitalWrite(CONTROLLINO_D0, HIGH);}
  else
    {digitalWrite(CONTROLLINO_D0, LOW);}
    
  if (RMU1_Output_Data[RMU1_VezKI34] == true)
    {digitalWrite(CONTROLLINO_D1, HIGH);}
  else
    {digitalWrite(CONTROLLINO_D1, LOW);}

  if (RMU1_Output_Data[RMU1_HMV_antileg_szoc] == true)
    {digitalWrite(CONTROLLINO_D2, HIGH);}
  else
    {digitalWrite(CONTROLLINO_D2, LOW);}

  if (RMU1_Output_Data[RMU1_HMV_antileg_konyha] == true)
    {digitalWrite(CONTROLLINO_D3, HIGH);}
  else
    {digitalWrite(CONTROLLINO_D3, LOW);}

  if (RMU1_Output_Data[RMU1_Szellozo_sziv_tiltas] == true)
    {digitalWrite(CONTROLLINO_D4, HIGH);}
  else
    {digitalWrite(CONTROLLINO_D4, LOW);}
}

