#include <SPI.h>
#include <Ethernet.h>
#include "DataStore.h"
#include <String.h>

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network.
// gateway and subnet are optional:
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 201);
IPAddress myDns(192,168,1, 1);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

//Default port 5001
EthernetServer server(5001);

boolean alreadyConnected = false; // whether or not the client was connected previously
bool ModBusDataReady = false; //Modbus data is ready to send


//-------------------------------------------------------------
void setup() 
{
  // initialize the ethernet device
  Ethernet.begin(mac, ip, myDns, gateway, subnet);
  // start listening for clients
  server.begin();
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) 
  {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Serial.print("Arduino server address:");
  Serial.println(Ethernet.localIP());

  for (int i=0; i<RMU1_Input_enum_num; ++i)
  {
    RMU1_Input_Data[i]=0;
  }

  for (int i=0; i<RMU1_Output_enum_num; ++i)
  {
    RMU1_Output_Data[i]=false;
  }
  RMU1_Output_Data[RMU1_AntiLeg_Tiltas] = true; //Default value

  for (int i=0; i<RMU1_Temperature_enum_num; ++i)
  {
    RMU1_Temperature_Data[i]=0;
  }

  ModBusSetup();
  OutputCommandSetup();
  InputReadSetup();
}

//---------------------------------------------
//---------------------------------------------
void loop() 
{
  //Ha modbus lekerdezes van folyamatban, akkor oda megy a loop
  if (ModBusDataReady == false) 
  {
    ModBusLoop();
    return;
  }
  
  //Check for a new client:
  EthernetClient client = server.available();
  if (client) 
  {
    if (!alreadyConnected) 
    {
      // clear out the input buffer:
      client.flush();
      Serial.println("We have a new client");
      alreadyConnected = true;
    }

    if (client.available()) //olvassuk ki, mi jott!
    {
      String Data="";
      char Byte;
      while(client.available())
      {
        Byte = client.read();
        if (Byte == -1)
          break;
        Data+=Byte;
      }
      
      Serial.print("Incoming eth data:");
      Serial.println(Data);

      ParseIncomingPacket(Data);
      OutputCommandRun();
      
      InputReadRun();
      SendAnswerPacket();
    }
  }

  //Start modbus data request process
  ModBusDataReady = false;
        
  delay(10);
}



