#pragma once

//-------------------------------------------------------------
enum eRMU1_Output_enum
{
	 // Digital Output 12V
  RMU1_VezKI12,
  RMU1_VezKI34,
  RMU1_HMV_antileg_szoc,
  RMU1_HMV_antileg_konyha,
  RMU1_Szellozo_sziv_tiltas,

  RMU1_AntiLeg_Tiltas, //Theoretical data --> command for Mango. No output for this info.

  RMU1_Output_enum_num
};

const char* RMU1_Output_Name[RMU1_Output_enum_num] =
{
      //Digital Output 12V
  "VezKI12",
  "VezKI34",
  "HMV_antileg_szoc",
  "HMV_antileg_konyha",
  "Szellozo_sziv_tiltas",

  "AntiLeg_Tiltas"
};

bool RMU1_Output_Data[RMU1_Output_enum_num];

//-------------------------------------------------------------
enum eRMU1_Input_enum
{
      // Input - potencialmentes kontakt
  RMU1_EVU3FOK12,
  RMU1_EVU3FOK34,
  RMU1_HSZ1ON,
  RMU1_HSZ2ON,
  RMU1_HSZ3ON,
  RMU1_HSZ4ON,
  RMU1_HSZ1Error,
  RMU1_HSZ2Error,
  RMU1_HSZ3Error,
  RMU1_HSZ4Error,
  RMU1_motor_ved1,
  RMU1_motor_ved2,
  RMU1_motor_ved3,
  RMU1_Ajto_nyitva,
  RMU1_Szivarg,
  RMU1_Tkor_nyomas_alacsony,

      // Input analog 0-10V
  RMU1_Nyomas,
  
  RMU1_Input_enum_num
};

int RMU1_Input_Data[RMU1_Input_enum_num];

const char* RMU1_Input_Name[RMU1_Input_enum_num] =
{
      //Input - potencialmentes kontakt
  "EVU3FOK12",
  "EVU3FOK34",
  "HSZ1ON",
  "HSZ2ON",
  "HSZ3ON",
  "HSZ4ON",
  "HSZ1Error",
  "HSZ2Error",
  "HSZ3Error",
  "HSZ4Error",
  "Motor_ved1",
  "Motor_ved2",
  "Motor_ved3",
  "Ajto_nyitva",
  "Szivarg",
  "Tkor_nyomas_alacsony",
  
      //0-10V
  "Nyomas"
};

//-------------------------------------------------------------
enum eRMU1_Temperature_enum
{      
      // Input PT1000
	//Seneca-1
  RMU1_Kulso,
  RMU1_HMV_konyha,
  RMU1_HMV_szoc,
  RMU1_Puffer,
  
	//Seneca-2
  RMU1_Hoforras_elor,
  RMU1_Hoforras_viss,
  RMU1_Zona1,

  //Seneca-3
  RMU1_Zona3,
  RMU1_Zona4,
  
  RMU1_Temperature_enum_num
};

float RMU1_Temperature_Data[RMU1_Temperature_enum_num];

const char* RMU1_Temperature_Name[RMU1_Temperature_enum_num] =
{
        //Pt1000
  "Kulso",
  "HMV_konyha",
  "HMV_szoc",
  "Puffer",
  "Hoforras_elor",
  "Hoforras_viss",
  "Zona1",
  "Zona3",
  "Zona4",
};
