﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace ConsoleApplication1
{
    class Program
    {
        static string GetResponseString()
        {
            string i = "@EVU3FOK12=1;@EVU3FOK34=0;@HSZ1ON=0;@HSZ2ON=0;@HSZ3ON=0;@HSZ4ON=1;@HSZ1Error=0;@HSZ2Error=0;@HSZ3Error=0;@HSZ4Error=0;@Motor_ved1=1;@Motor_ved2=0;";
            i += "@Motor_ved3=0;@Ajto_nyitva=0;@Szivarg=0;@Tkor_nyomas_alacsony=1;@Nyomas=43;";
            i += "@Kulso=0;@HMV_konyha=0;@HMV_szoc=0;@Puffer=0;@Hoforras_elor=40;";
            i += "@Hoforras_viss=0;@Zona1=0;@Zona3=0;@Zona4=40;";

            return i;
        }

        static void Main(string[] args)
        {

            TcpListener serverSocket = new TcpListener(5001);
            int requestCount = 0;
            TcpClient clientSocket = default(TcpClient);
            serverSocket.Start();
            Console.WriteLine(" >> Server Started");
            requestCount = 0;

            while ( true /*requestCount<200*/ )
            {
                try
                {
                    clientSocket = serverSocket.AcceptTcpClient();
                    requestCount = requestCount + 1;
                    NetworkStream networkStream = clientSocket.GetStream();
                    Console.WriteLine(" >> Accept connection from client");

                    int ReadBufSize = (int)clientSocket.ReceiveBufferSize;
                    byte[] bytesFrom = new byte[ReadBufSize+2];
                    networkStream.Read(bytesFrom, 0, ReadBufSize);
                    string dataFromClient = System.Text.Encoding.UTF8.GetString(bytesFrom);

                    dataFromClient = dataFromClient.Substring(0, dataFromClient.LastIndexOf(';')+1);
                    Console.WriteLine(" >> Data from client - " + dataFromClient);

                    string serverResponse = GetResponseString();
                    Byte[] sendBytes = Encoding.ASCII.GetBytes(serverResponse);
                    networkStream.Write(sendBytes, 0, sendBytes.Length);
                    networkStream.Flush();
                    Console.WriteLine(" >> Data sent - " + serverResponse);

                    clientSocket.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }

            serverSocket.Stop();
            Console.WriteLine(" >> exit");
            Console.ReadLine();
        }

    }
}
